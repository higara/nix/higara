#!/usr/bin/env bash

targethost=$1
targetip=$2

usage() {
  echo "Installs nixos on a target machine using SSH."
  echo "SSH keys are source from pass"
  echo "  Usage: $1 targetname targetip"
  echo ""
  echo "  eg: $1 drummer 192.168.1.5"
}

if [ -z "$targethost" ]; then
  usage "$0"
  exit 1
fi

if [ -z "$targetip" ]; then
  usage "$0"
  exit 1
fi

# Create a temporary directory
temp=$(mktemp -d)

# Function to cleanup temporary directory on exit
cleanup() {
  rm -rf "$temp"
}
trap cleanup EXIT

# Create the directory where sshd expects to find the host keys
install -d -m755 "$temp/etc/ssh"

# Decrypt your private key from the password store and copy it to the temporary directory
pass machines/"${targethost}"/ssh/key >"$temp/etc/ssh/ssh_host_ed25519_key"

# Set the correct permissions so sshd will accept the key
chmod 600 "$temp/etc/ssh/ssh_host_ed25519_key"

# Install NixOS to the host system with our secrets
nix run \
  github:nix-community/nixos-anywhere \
  -- \
  --extra-files "$temp" \
  --flake ".#${targethost}" \
  --generate-hardware-config nixos-generate-config nixos/"${targethost}"/hardware-configuration.nix \
  $"root@${targetip}"
