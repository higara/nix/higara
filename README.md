# Higara Nix configuration

## Usage

### Bootstrapping new systems

1. You need to create SSH keys for the new machine. Store them in pass.
   ```bash
   mkdir ~/${NEWHOSTNAME}
   ssh-keygen -t ed25519 -C "root@${NEWHOSTNAME}" -f ~/${NEWHOSTNAME}/key
   pass insert --multiline machines/${NEWHOSTNAME}/ssh/key < ~/${NEWHOSTNAME}/key
   pass insert --multiline machines/${NEWHOSTNAME}/ssh/key.pub < ~/${NEWHOSTNAME}/key.pub
   rm -rf ~/${NEWHOSTNAME}
   ```
2. Then convert them to age keys using the `ssh-to-age` tool:
   ```bash
   pass machines/${NEWHOSTNAME}/ssh/key.pub | ssh-to-age -o nixos/secrets/keys/${NEWHOSTNAME}.asc
   ```
3. Add the age key to `.sops.yaml` using the same tool:
   ```bash
   pass machines/${NEWHOSTNAME}/ssh/key.pub | ssh-to-age
   ```
4. Update existing secrets file to include new key in secrets:
   ```bash
   sops updatekeys modules/formulas/secrets/stooj-workstation-user/secrets.yaml
   ```
5. Maybe update `modules/chassis/laptop-secrets.yaml`
6. Add new host to `nixos/configurations.nix`
7. Add new host to `nixos/deploy.nix`
8. Add new host disks, hardware, configuration to `nixos/${NEWHOSTNAME}/`
9. Create user passwords in pass.
   ```bash
   pass generate machines/${NEWHOSTNAME}/root
   pass generate machines/${NEWHOSTNAME}/stoo
   ```
10. Create hashes of the passwords
   ```bash
   mkpasswd $(pass machines/${NEWHOSTNAME}/root)
   mkpasswd $(pass machines/${NEWHOSTNAME}/stoo)
   ```
11. Add secrets to `nixos/${NEWHOSTNAME}/secrets.yaml`
12. Use `bin/install-nixos.sh` script to install nixos on target.

### Deploying

Simply run:

```bash
deploy .#NEWHOSTNAME
```

### Updating

To update the system, update the flakes:

```bash
nix flake update
```
