{
  mkShell,
  pkgs,
  sops-import-keys-hook,
  ssh-to-pgp,
  ssh-to-age,
  sops-init-gpg-key,
  sops,
  deploy-rs,
  nixpkgs-fmt
}:
mkShell {
  sopsPGPKeyDirs = [ "./nixos/secrets/keys" ];
  nativeBuildInputs = [
    pkgs.age
    ssh-to-age
    ssh-to-pgp
    sops-import-keys-hook
    sops-init-gpg-key
    sops
    deploy-rs
    nixpkgs-fmt
  ];
}
