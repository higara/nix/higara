{
  description = "Higara configuration";

  nixConfig = {
    # Add to the default substitutors
    extra-substitutors = [
      "https://nix-community.cachix.org"
      "https://stooj.cachix.org"
    ];
    extra-trusted-public-keys = [
      "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
      "stooj.cachix.org-1:3y3klRe64xC7ATkhBanE4yLNe7ZO1+MHUlTk3AV34PM="
    ];
  };
  inputs = {
    deploy.url = "github:serokell/deploy-rs";
    flake-utils.url = "github:numtide/flake-utils";
    home-manager = {
      inputs.nixpkgs.follows = "nixpkgs";
      url = "github:nix-community/home-manager/release-24.11";
    };
    nix.url = "github:NixOS/nix/2.17-maintenance";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";
    nixos-hardware.url = "github:nixos/nixos-hardware";
    nixvim = {
      url = "github:nix-community/nixvim/nixos-24.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    disko = {
      url = "github:nix-community/disko";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    sops-nix = {
      inputs.nixpkgs.follows = "nixpkgs";
      url = "github:Mic92/sops-nix";
    };
  };
  outputs = {...} @ args: import ./outputs.nix args;
}
