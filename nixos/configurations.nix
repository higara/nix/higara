{
  nixpkgs,
  sops-nix,
  inputs,
  disko,
  home-manager,
  nixos-hardware,
  ...
}: let
  nixosSystem = nixpkgs.lib.makeOverridable nixpkgs.lib.nixosSystem;
  ginstooDefaults = import ../modules/ginstoo-defaults.nix;
  laptopChassis = import ../modules/chassis/laptop.nix;
  serverChassis = import ../modules/chassis/server.nix;
  personalWorkstationRole = import ../modules/roles/personal-workstation.nix;
  workWorkstationRole = import ../modules/roles/work-workstation.nix;
  audiobookServerRole = import ../modules/roles/audiobook-server.nix;
  fireflyServerRole = import ../modules/roles/firefly-server.nix;
  mediaServerRole = import ../modules/roles/media-server.nix;
  wireguardServerRole = import ../modules/roles/wireguard-server.nix;
  baseModules = [
    {
      imports = [
        ({pkgs, ...}: {
          nix = {
            nixPath = [
              "nixpkgs=${pkgs.path}"
            ];
            gc = {
              automatic = true;
              dates = "weekly";
              options = "--delete-older-than 15d";
            };
            settings = {
              experimental-features = ["nix-command" "flakes"];
              auto-optimise-store = true;
            };
          };
          # documentation.info.enable = false;
        })
        sops-nix.nixosModules.sops
        disko.nixosModules.disko
        home-manager.nixosModules.home-manager
      ];
    }
  ];
  defaultModules = baseModules ++ ginstooDefaults;
in {
  ashford = nixosSystem {
    system = "x86_64-linux";
    modules =
      defaultModules
      ++ laptopChassis
      ++ workWorkstationRole
      ++ [
        ./ashford/configuration.nix
        # nixos-hardware.nixosModules.common-gpu-intel
        nixos-hardware.nixosModules.common-cpu-intel
        nixos-hardware.nixosModules.common-pc-laptop
        nixos-hardware.nixosModules.system76
      ];
    specialArgs = {inherit inputs;};
  };
  drummer = nixosSystem {
    system = "x86_64-linux";
    modules =
      defaultModules
      ++ laptopChassis
      ++ personalWorkstationRole
      ++ [
        ./drummer/configuration.nix
        # nixos-hardware.nixosModules.common-gpu-intel
        nixos-hardware.nixosModules.common-cpu-intel
        nixos-hardware.nixosModules.common-pc-laptop
        nixos-hardware.nixosModules.system76
      ];
    specialArgs = {inherit inputs;};
  };
  europa = nixosSystem {
    system = "x86_64-linux";
    modules =
      defaultModules
      ++ serverChassis
      ++ mediaServerRole
      ++ [
        ./europa/configuration.nix
      ];
    specialArgs = {inherit inputs;};
  };
  hygiea = nixosSystem {
    system = "x86_64-linux";
    modules =
      defaultModules
      ++ serverChassis
      ++ fireflyServerRole
      ++ [
        ./hygiea/configuration.nix
      ];
    specialArgs = {inherit inputs;};
  };
  kuiper = nixosSystem {
    system = "x86_64-linux";
    modules =
      defaultModules
      ++ serverChassis
      ++ audiobookServerRole
      ++ [
        ./kuiper/configuration.nix
      ];
    specialArgs = {inherit inputs;};
  };
  medina = nixosSystem {
    system = "x86_64-linux";
    modules =
      defaultModules
      ++ serverChassis
      ++ wireguardServerRole
      ++ [
        ./medina/configuration.nix
      ];
    specialArgs = {inherit inputs;};
  };
  rebecca = nixosSystem {
    system = "x86_64-linux";
    modules =
      defaultModules
      ++ laptopChassis
      ++ personalWorkstationRole
      ++ [
        ./rebecca/configuration.nix
      ];
    specialArgs = {inherit inputs;};
  };
}
