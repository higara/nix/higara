{config, ...}: {
  sops.defaultSopsFile = ./secrets.yaml;
  sops.secrets."wireguard-private-key" = {};
  sops.secrets.medina-ashford-psk = {
    sopsFile = ../../modules/formulas/secrets/wireguard-server/secrets.yaml;
  };
  networking.wireguard.interfaces.wg0 = {
    ips = ["10.20.40.21/24"];
    privateKeyFile =
      config.sops.secrets."wireguard-private-key".path;
    peers = [
      # For a client configuration, one peer entry for the server will suffice.

      {
        name = "medina";
        # Public key of the server (not a file path).
        publicKey = "Kz4bL3cPBCssa9v7CBpAryMUmYJ7+FnhDmn+y6P+WSU=";
        presharedKeyFile = config.sops.secrets.medina-ashford-psk.path;

        # Forward all the traffic via VPN.
        # allowedIPs = ["0.0.0.0/0"];
        # Or forward only particular subnets
        allowedIPs = ["10.20.40.0/24" "172.16.0.0/20" "172.16.48.0/24"];

        # Set this to the server IP and port.
        endpoint = "wgtemp.ginstoo.net:51820"; # ToDo: route to endpoint not automatically configured https://wiki.archlinux.org/index.php/WireGuard#Loop_routing https://discourse.nixos.org/t/solved-minimal-firewall-setup-for-wireguard-client/7577

        # Send keepalives every 25 seconds. Important to keep NAT tables alive.
        persistentKeepalive = 25;
      }
    ];
  };
}
