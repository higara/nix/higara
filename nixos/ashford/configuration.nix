{config, ...}: let
  machine = "ashford";
in {
  sops.defaultSopsFile = ./secrets.yaml;
  sops.secrets.rootPasswordHash.neededForUsers = true;
  sops.age.sshKeyPaths = ["/etc/ssh/ssh_host_ed25519_key"];
  sops.gnupg.sshKeyPaths = [];

  imports = [
    ./disk-config.nix
    ./hardware-configuration.nix
  ];

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.initrd.systemd.enable = true;
  boot.plymouth.enable = true;
  boot.kernelParams = ["quiet"];

  hardware.system76 = {
    enableAll = false;
    firmware-daemon.enable = true;
    kernel-modules.enable = true;
  };

  time.timeZone = "Europe/London";
  networking.hostName = machine;
  networking.enableIPv6 = false;

  users.users.root = {
    hashedPasswordFile = config.sops.secrets.rootPasswordHash.path;
  };
  system.stateVersion = "23.05";
}
