{
  config,
  ...
}:

let
  machine = "rebecca";
in {
  sops.defaultSopsFile = ./secrets.yaml;
  sops.secrets.rootPasswordHash.neededForUsers = true;
  sops.age.sshKeyPaths = ["/etc/ssh/ssh_host_ed25519_key" ];
  sops.gnupg.sshKeyPaths = [];

  imports = [
    ./disk-config.nix
    ./hardware-configuration.nix
  ];

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.plymouth.enable = true;

  time.timeZone = "Europe/London";
  networking.hostName = machine;

  users.users.root = {
    hashedPasswordFile = config.sops.secrets.rootPasswordHash.path;
  };
  # users.users.stooj.hashedPassword = config.sops.secrets.passwordHashes.stooj;
  system.stateVersion = "23.05";
}
