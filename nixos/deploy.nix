{
  self,
  deploy,
  ...
}: let
  mkNode = server: ip: fast: {
    hostname = "${ip}";
    fastConnection = false;
    profiles.system.path =
      deploy.lib.x86_64-linux.activate.nixos
      self.nixosConfigurations."${server}";
  };
in {
  user = "root";
  sshUser = "root";
  nodes = {
    ashford = mkNode "ashford" "192.168.1.109" true;
    europa = mkNode "europa" "159.69.181.210" true;
    hygiea = mkNode "hygiea" "138.199.155.142" true;
    kuiper = mkNode "kuiper" "188.245.231.172" true;
    medina = mkNode "medina" "49.13.9.128" true;
    rebecca = mkNode "rebecca" "172.16.3.5" true;
    drummer = mkNode "drummer" "192.168.1.101" true;
  };
}
