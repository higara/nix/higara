{config, ...}: let
  machine = "europa";
in {
  sops.defaultSopsFile = ./secrets.yaml;
  sops.secrets.rootPasswordHash.neededForUsers = true;
  sops.secrets.stoojPasswordHash.neededForUsers = true;
  sops.age.sshKeyPaths = ["/etc/ssh/ssh_host_ed25519_key"];
  sops.gnupg.sshKeyPaths = [];

  imports = [
    ./disk-config.nix
    ./hardware-configuration.nix
  ];

  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;

  time.timeZone = "Europe/London";
  networking.hostName = machine;

  users.users.root = {
    hashedPasswordFile = config.sops.secrets.rootPasswordHash.path;
  };

  users.users.stooj = {
    hashedPasswordFile = config.sops.secrets.stoojPasswordHash.path;
  };

  system.stateVersion = "24.11";
}
