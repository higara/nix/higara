{config, ...}: {
  sops.defaultSopsFile = ./secrets.yaml;
  sops.secrets."wireguard-private-key" = {};
  sops.secrets.medina-ashford-psk = {
    sopsFile = ../../modules/formulas/secrets/wireguard-server/secrets.yaml;
  };
  networking.wireguard.interfaces.wg0 = {
    # Determines the IP address and subnet of the server's end of the tunnel interface.
    ips = ["10.20.40.20/24"];
    privateKeyFile =
      config.sops.secrets."wireguard-private-key".path;
    peers = [
      # List of allowed peers.
      {
        name = "ashford";
        # Public key of the peer (not a file path).
        publicKey = "gLVFcZ/gbQkid6NPm4lFKfVP+/0shrG4BEtJR9aV/RU=";
        presharedKeyFile = config.sops.secrets.medina-ashford-psk.path;
        # List of IPs assigned to this peer within the tunnel subnet. Used to configure routing.
        allowedIPs = ["10.20.40.21/32"];
      }
    ];
  };
}
