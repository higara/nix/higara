{config, ...}: {
  sops.secrets.vortex-playa-psk = {
    sopsFile = ../chassis/laptop-secrets.yaml;
  };

  sops.templates."wireless-vortex-playa.env".content = ''
    VORTEX_PLAYA_WIFI_PASSWORD = "${config.sops.placeholder.vortex-playa-psk}"
  '';

  networking.networkmanager.ensureProfiles = {
    environmentFiles = [
      config.sops.templates."wireless-vortex-playa.env".path
    ];
    profiles = {
      vortex-playa = {
        connection = {
          id = "vortex-playa";
          type = "wifi";
        };
        wifi = {
          mode = "infrastructure";
          ssid = "Vortex Coworking";
        };
        wifi-security = {
          auth-alg = "open";
          key-mgmt = "wpa-psk";
          psk = "$VORTEX_PLAYA_WIFI_PASSWORD";
        };
        ipv4 = {
          method = "auto";
        };
        ipv6 = {
          addr-gen-mode = "default";
          method = "auto";
        };
        proxy = {
        };
      };
    };
  };
}
