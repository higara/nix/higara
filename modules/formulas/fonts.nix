{pkgs, ...}: {
  fonts.packages = with pkgs; [
    # Nerd fonts
    # Iconic font aggregator, collection, & patcher.
    (
      nerdfonts.override {
        fonts = [
          "FiraCode"
          "Meslo"
          "NerdFontsSymbolsOnly"
        ];
      }
    )
  ];
}
