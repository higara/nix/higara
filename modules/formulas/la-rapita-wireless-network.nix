{ config, ... }:
{
  sops.secrets.larapita-psk = {
    sopsFile = ../chassis/laptop-secrets.yaml;
  };

  sops.templates."wireless-larapita.env".content = ''
    LARAPITA_WIFI_PASSWORD = "${config.sops.placeholder.larapita-psk}"
  '';

  networking.networkmanager.ensureProfiles = {
    environmentFiles = [
      config.sops.templates."wireless-larapita.env".path
    ];
    profiles = {
      larapita = {
        connection = {
          id = "larapita";
          type = "wifi";
        };
        wifi = {
          mode = "infrastructure";
          ssid = "COWORKING LA RAPITA 5ghz";
        };
        wifi-security = {
          auth-alg = "open";
          key-mgmt = "wpa-psk";
          psk = "$LARAPITA_WIFI_PASSWORD";
        };
        ipv4 = {
          method = "auto";
        };
        ipv6 = {
          addr-gen-mode = "default";
          method = "auto";
        };
        proxy = {
        };
      };
    };
  };
}
