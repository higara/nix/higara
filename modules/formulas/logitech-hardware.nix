{config, ...}: {
  hardware.logitech.wireless = {
    enable = true;
    enableGraphical = true;
  };

  users.extraGroups.plugdev.members =
    builtins.filter (
      x: builtins.elem "wheel" config.users.users."${x}".extraGroups
    ) (
      builtins.attrNames config.users.users
    );
}
