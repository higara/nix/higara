{pkgs, ...}: let
  python-packages = ps:
    with ps; [
      dulwich
      notify-py
      pyperclip
      pyyaml
      pyxdg
      virtualenvwrapper
    ];
in {
  environment.systemPackages = with pkgs; [
    (python3.withPackages python-packages)
    pipenv
  ];
}
