{config, ...}: {
  sops.secrets.inngenio-psk = {
    sopsFile = ../chassis/laptop-secrets.yaml;
  };

  sops.templates."wireless-inngenio.env".content = ''
    INNGENIO_WIFI_PASSWORD = "${config.sops.placeholder.inngenio-psk}"
  '';

  networking.networkmanager.ensureProfiles = {
    environmentFiles = [
      config.sops.templates."wireless-inngenio.env".path
    ];
    profiles = {
      inngenio = {
        connection = {
          id = "inngenio";
          type = "wifi";
        };
        wifi = {
          mode = "infrastructure";
          ssid = "INNgenio_5G";
        };
        wifi-security = {
          auth-alg = "open";
          key-mgmt = "wpa-psk";
          psk = "$INNGENIO_WIFI_PASSWORD";
        };
        ipv4 = {
          method = "auto";
        };
        ipv6 = {
          addr-gen-mode = "default";
          method = "auto";
        };
        proxy = {
        };
      };
    };
  };
}
