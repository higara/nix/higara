{config, ...}: {
  sops.secrets.ceres-psk = {
    sopsFile = ../chassis/laptop-secrets.yaml;
  };

  sops.templates."wireless-ceres.env".content = ''
    CERES_WIFI_PASSWORD = "${config.sops.placeholder.ceres-psk}"
  '';

  networking.networkmanager.ensureProfiles = {
    environmentFiles = [
      config.sops.templates."wireless-ceres.env".path
    ];
    profiles = {
      ceres = {
        connection = {
          id = "ceres";
          type = "wifi";
        };
        wifi = {
          mode = "infrastructure";
          ssid = "ceres";
        };
        wifi-security = {
          auth-alg = "open";
          key-mgmt = "wpa-psk";
          psk = "$CERES_WIFI_PASSWORD";
        };
        ipv4 = {
          method = "auto";
        };
        ipv6 = {
          addr-gen-mode = "default";
          method = "auto";
        };
        proxy = {
        };
      };
    };
  };
}
