{ config, ... }:
{
  sops.secrets.empire-psk = {
    sopsFile = ../chassis/laptop-secrets.yaml;
  };

  sops.templates."wireless-empire.env".content = ''
    EMPIRE_WIFI_PASSWORD = "${config.sops.placeholder.empire-psk}"
  '';

  networking.networkmanager.ensureProfiles = {
    environmentFiles = [
      config.sops.templates."wireless-empire.env".path
    ];
    profiles = {
      empire = {
        connection = {
          id = "EMPIRE";
          type = "wifi";
        };
        wifi = {
          mode = "infrastructure";
          ssid = "EMPIRE";
        };
        wifi-security = {
          auth-alg = "open";
          key-mgmt = "wpa-psk";
          psk = "$EMPIRE_WIFI_PASSWORD";
        };
        ipv4 = {
          method = "auto";
        };
        ipv6 = {
          addr-gen-mode = "default";
          method = "auto";
        };
        proxy = {
        };
      };
    };
  };
}
