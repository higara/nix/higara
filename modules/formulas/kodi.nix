{
  pkgs,
  config,
  ...
}: {
  environment.systemPackages = [
    (pkgs.kodi.passthru.withPackages (kodiPkgs:
      with kodiPkgs; [
        jellyfin
      ]))
  ];
  services.xserver = {
    enable = true;
    desktopManager.kodi = {
      enable = true;
    };
  };
  sops.secrets.kodiPasswordHash = {
    sopsFile = ./secrets/kodi-user/secrets.yaml;
    neededForUsers = true;
  };
  users.extraUsers.kodi = {
    isNormalUser = true;
    hashedPasswordFile = config.sops.secrets.kodiPasswordHash.path;
  };
  networking.firewall = {
    allowedTCPPorts = [8080];
    allowedUDPPorts = [8080];
  };
}
