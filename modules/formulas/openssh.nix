{ ... }:
let
  shared = import ../shared.nix;
in
{
  users.users.root = {
    openssh.authorizedKeys.keys = shared.stooj.sshkeys;
  };

  services.openssh = {
    enable = true;
    settings = {
      PasswordAuthentication = false;
    };
  };
}
