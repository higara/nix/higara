{config, ...}: {
  sops.secrets.firefly-importer-access-token = {
    sopsFile = ./secrets/firefly-server/secrets.yaml;
  };

  services.firefly-iii-data-importer = {
    enable = true;
    enableNginx = true;
    settings = {
      APP_ENV = "production";
      LOG_CHANNEL = "syslog";
      FIREFLY_III_URL = "https://accounts.ginstoo.net";
      # This sucks. These can only be used once they've been generated in
      # the firefly UI 😭
      # FIREFLY_III_ACCESS_TOKEN = config.sops.secrets.firefly-importer-access-token.path;
      FIREFLY_III_CLIENT_ID = 2;
      TRUSTED_PROXIES = "**"; # TODO tighten this up
    };
  };

  # virtualHosts.${config.services.firefly-iii-data-importer.virtualHost} = {
  #   root = "${config.services.firefly-iii-data-importer.package}/public";
  #   enableACME = true;
  #   acmeRoot = null;
  #   forceSSL = true;
  #   locations = {
  #     "/" = {
  #       tryFiles = "$uri $uri/ /index.php?$query_string";
  #       index = "index.php";
  #       extraConfig = ''
  #         sendfile off;
  #       '';
  #     };
  #     "~ \\.php$" = {
  #       extraConfig = ''
  #         include ${config.services.nginx.package}/conf/fastcgi_params ;
  #         fastcgi_param SCRIPT_FILENAME $request_filename;
  #         fastcgi_param modHeadersAvailable true; #Avoid sending the security headers twice
  #         fastcgi_pass unix:${config.services.phpfpm.pools.firefly-iii-data-importer.socket};
  #       '';
  #     };
  #   };
  # };
}
