{config, ...}: {
  sops.secrets.vortex-centro-psk = {
    sopsFile = ../chassis/laptop-secrets.yaml;
  };

  sops.templates."wireless-vortex-centro.env".content = ''
    VORTEX_CENTRO_WIFI_PASSWORD = "${config.sops.placeholder.vortex-centro-psk}"
  '';

  networking.networkmanager.ensureProfiles = {
    environmentFiles = [
      config.sops.templates."wireless-vortex-centro.env".path
    ];
    profiles = {
      vortex-centro = {
        connection = {
          id = "vortex-centro";
          type = "wifi";
        };
        wifi = {
          mode = "infrastructure";
          ssid = "Vortex Coworking Centro";
        };
        wifi-security = {
          auth-alg = "open";
          key-mgmt = "wpa-psk";
          psk = "$VORTEX_CENTRO_WIFI_PASSWORD";
        };
        ipv4 = {
          method = "auto";
        };
        ipv6 = {
          addr-gen-mode = "default";
          method = "auto";
        };
        proxy = {
        };
      };
    };
  };
}
