{
  pkgs,
  config,
  ...
}: {
  security.polkit.enable = true;

  virtualisation = {
    libvirtd = {
      enable = true;
      qemu = {
        ovmf.enable = true;
        runAsRoot = false;
      };
      onBoot = "ignore";
      onShutdown = "shutdown";
    };
    spiceUSBRedirection.enable = true;
  };
  users.extraGroups.libvirtd.members =
    builtins.filter (
      x: builtins.elem "wheel" config.users.users."${x}".extraGroups
    ) (
      builtins.attrNames config.users.users
    );

  environment.systemPackages = with pkgs; [
    virt-manager
    quickemu
  ];
}
