{config, ...}: {
  sops.secrets.marxalenes-psk = {
    sopsFile = ../chassis/laptop-secrets.yaml;
  };

  sops.templates."wireless-marxalenes.env".content = ''
    MARXALENES_WIFI_PASSWORD = "${config.sops.placeholder.marxalenes-psk}"
  '';

  networking.networkmanager.ensureProfiles = {
    environmentFiles = [
      config.sops.templates."wireless-marxalenes.env".path
    ];
    profiles = {
      marxalenes = {
        connection = {
          id = "marxalenes";
          type = "wifi";
        };
        wifi = {
          mode = "infrastructure";
          ssid = "MIWIFI_rJdj";
        };
        wifi-security = {
          auth-alg = "open";
          key-mgmt = "wpa-psk";
          psk = "$MARXALENES_WIFI_PASSWORD";
        };
        ipv4 = {
          method = "auto";
        };
        ipv6 = {
          addr-gen-mode = "default";
          method = "auto";
        };
        proxy = {
        };
      };
    };
  };
}
