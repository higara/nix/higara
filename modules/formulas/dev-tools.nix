{pkgs, ...}: {
  environment.systemPackages = with pkgs; [
    gnumake
    jdk17 # Java development kit, most recent release
    pre-commit # A framework for managing and maintaining multi-language pre-commit hooks
    sops
    universal-ctags # A maintained ctags implementation
  ];
}
