{ config, ... }:
{
  virtualisation.docker.enable = true;
  users.extraGroups.docker.members = builtins.filter (
    x: builtins.elem "wheel" config.users.users."${x}".extraGroups
  ) (
    builtins.attrNames config.users.users
  );
}
