{
  pkgs,
  config,
  ...
}: {
  sops.secrets.storagebox-password1 = {
    sopsFile = ./secrets/media-server/secrets.yaml;
  };

  sops.secrets.storagebox-password2 = {
    sopsFile = ./secrets/media-server/secrets.yaml;
  };
  sops.secrets.ssh-password = {
    sopsFile = ./secrets/media-server/secrets.yaml;
  };

  sops.templates."rclone.conf" = {
    owner = "jellyfin";
    content = ''
      [ginstoo-storage-unencrypted]
      type = sftp
      host = u398328.your-storagebox.de
      user = u398328
      port = 23
      shell_type = unix
      md5sum_command = md5 -r
      sha1sum_command = sha1 -r
      pass = ${config.sops.placeholder.ssh-password}

      [ginstoo-storage]
      type = crypt
      remote = ginstoo-storage-unencrypted:datapool
      password = ${config.sops.placeholder.storagebox-password1}
      password2 = ${config.sops.placeholder.storagebox-password2}
    '';
  };

  # Allow non-root users to specify the allow_other or allow_root mount options, see mount.fuse3(8).
  programs.fuse.userAllowOther = true;
  systemd.tmpfiles.rules = [
    "d /srv/media 0755 jellyfin users"
  ];

  security.wrappers = {
    fusermount.source = "${pkgs.fuse}/bin/fusermount";
  };
  systemd.services.rclone-ginstoo-media = {
    # From http://web.archive.org/web/20230323204844/https://github.com/animosity22/homescripts/blob/master/systemd/rclone-tv.service
    description = "rclone service ginstoo media";
    wants = ["network-online.target"];
    after = ["network-online.target"];
    serviceConfig = {
      Type = "notify";
      RestartSec = 5;
      CacheDirectory = "media";
      LogsDirectory = "rclone";
      TimeoutStartSec = 180;
      Environment = [
        "RCLONE_CONFIG=${config.sops.templates."rclone.conf".path}"
        "PATH=/run/wrappers/bin:$PATH"
      ];
      ExecStart = ''        ${pkgs.rclone}/bin/rclone mount ginstoo-storage: /srv/media \
                                # This is for allowing users other than the rclone user to access
                                # the remote
                                --allow-other \
                                # Log file location
                                --log-file /var/log/rclone/rclone-ginstoo.log \
                                # Set the log level
                                --log-level WARNING \
                                --umask 002 \
                                # This sets up the remote control daemon so you can issue rc commands locally
                                --rc \
                                # This is the default port it runs on
                                --rc-addr 127.0.0.1:5575 \
                                # no-auth is used as no one else uses my server and it is not a shared seedbox
                                --rc-no-auth \
                                # The local disk used for caching
                                --cache-dir=/var/cache/media \
                                # This is used for caching files to local disk for streaming
                                --vfs-cache-mode full \
                                # Speed up the reading: Use fast (less accurate) fingerprints for change detection
                                --vfs-fast-fingerprint \
                                # Wait before uploading
                                --vfs-write-back 1h \
                                # This limits the age in the cache if the size is reached and it removes the oldest files first
                                --vfs-cache-max-age 1h0m0s \
                                # This limits the size of the cache
                                --vfs-cache-max-size 20G \
                                # Disable HTTP2
                                --disable-http2
      '';
      ExecStop = "/run/wrappers/bin/fusermount -uz /srv/media";
      ExecStartPost = "${pkgs.rclone}/bin/rclone rc vfs/refresh recursive=true --url 127.0.0.1:5575 _async=true";
      Restart = "on-failure";
      User = "jellyfin";
      Group = "users";
    };
    wantedBy = ["multi-user.target"];
  };
}
