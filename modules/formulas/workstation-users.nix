{config, ...}: {
  sops.secrets.stoojPasswordHash = {
    sopsFile = ./secrets/stooj-workstation-user/secrets.yaml;
    neededForUsers = true;
  };
  users.users.stooj = {
    # All users that should have permission to change network settings must
    # belong to the networkmanager group
    extraGroups = ["networkmanager"];
    hashedPasswordFile = config.sops.secrets.stoojPasswordHash.path;
  };
}
