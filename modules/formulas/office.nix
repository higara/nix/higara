{pkgs, ...}: {
  environment.systemPackages = with pkgs; [
    # calibre  # Comprehensive e-book software
    libreoffice-fresh # Comprehensive, professional-quality productivity suite
    zathura # A highly customizable and functional PDF viewer
  ];
}
