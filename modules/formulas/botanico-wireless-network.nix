{config, ...}: {
  sops.secrets.botanico-psk = {
    sopsFile = ../chassis/laptop-secrets.yaml;
  };

  sops.templates."wireless-botanico.env".content = ''
    BOTANICO_WIFI_PASSWORD = "${config.sops.placeholder.botanico-psk}"
  '';

  networking.networkmanager.ensureProfiles = {
    environmentFiles = [
      config.sops.templates."wireless-botanico.env".path
    ];
    profiles = {
      botanico = {
        connection = {
          id = "botanico";
          type = "wifi";
        };
        wifi = {
          mode = "infrastructure";
          ssid = "COWORKING_BOTANICO";
        };
        wifi-security = {
          auth-alg = "open";
          key-mgmt = "wpa-psk";
          psk = "$BOTANICO_WIFI_PASSWORD";
        };
        ipv4 = {
          method = "auto";
        };
        ipv6 = {
          addr-gen-mode = "default";
          method = "auto";
        };
        proxy = {
        };
      };
    };
  };
}
