{ config, ... }:
{
  sops.secrets.higara-psk = {
    sopsFile = ../chassis/laptop-secrets.yaml;
  };

  sops.templates."wireless-higara.env".content = ''
    HIGARA_WIFI_PASSWORD = "${config.sops.placeholder.higara-psk}"
  '';

  networking.networkmanager.ensureProfiles = {
    environmentFiles = [
      config.sops.templates."wireless-higara.env".path
    ];
    profiles = {
      higara = {
        connection = {
          id = "higara";
          type = "wifi";
        };
        wifi = {
          mode = "infrastructure";
          ssid = "Higara";
        };
        wifi-security = {
          auth-alg = "open";
          key-mgmt = "wpa-psk";
          psk = "$HIGARA_WIFI_PASSWORD";
        };
        ipv4 = {
          method = "auto";
        };
        ipv6 = {
          addr-gen-mode = "default";
          method = "auto";
        };
        proxy = {
        };
      };
    };
  };
}
