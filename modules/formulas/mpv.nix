{pkgs, ...}: {
  nixpkgs.overlays = [
    (self: super: {
      mpv = super.mpv.override {
        scripts = [self.mpvScripts.mpris];
      };
    })
  ];
  environment.systemPackages = with pkgs; [
    mpv
  ];
  hardware.graphics.enable = true;
  users.extraGroups.video.members = ["stooj"];
}
