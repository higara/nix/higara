{config, ...}: {
  sops.secrets.firefly-key = {
    sopsFile = ./secrets/firefly-server/secrets.yaml;
    owner = "firefly-iii";
  };

  services.postgresql = {
    enable = true;
    ensureDatabases = ["firefly-iii"];
    ensureUsers = [
      {
        name = config.services.firefly-iii.settings.DB_USERNAME;
        ensureDBOwnership = true;
      }
    ];
  };

  sops.secrets."do-auth-token" = {
    sopsFile = ./secrets/media-server/secrets.yaml;
  };

  security.acme = {
    acceptTerms = true;
    defaults = {
      email = "letsencrypt@stooj.org";
      dnsProvider = "digitalocean";
      credentialFiles = {
        "DO_AUTH_TOKEN_FILE" = config.sops.secrets."do-auth-token".path;
      };
    };
  };

  networking.firewall.allowedTCPPorts = [80 443];

  services.firefly-iii = {
    enable = true;
    group = "nginx"; # Have to set this manually, managing nginx myself
    virtualHost = "accounts.ginstoo.net";
    enableNginx = false; # Override this with a customer nginx config with ssl

    settings = {
      # Any option that isn't recognised here gets passed directly to the env
      APP_KEY_FILE = config.sops.secrets.firefly-key.path;
      DB_PORT = 5432; # Default
      DB_CONNECTION = "pgsql";
      DB_DATABASE = "firefly-iii"; # Undocumented option
      DB_USERNAME = "firefly-iii";
      APP_ENV = "production";
      TZ = "Europe/Madrid";
      TRUSTED_PROXIES = "**"; # TODO tighten this up
    };
  };

  services.nginx = {
    enable = true;
    # Don't use ipv6 for dns lookups
    resolver.ipv6 = false;
    recommendedGzipSettings = true;
    recommendedOptimisation = true;
    recommendedProxySettings = true;
    recommendedTlsSettings = true;
    clientMaxBodySize = "64M";
    virtualHosts.${config.services.firefly-iii.virtualHost} = {
      root = "${config.services.firefly-iii.package}/public";
      enableACME = true;
      acmeRoot = null;
      forceSSL = true;
      locations = {
        "/" = {
          tryFiles = "$uri $uri/ /index.php?$query_string";
          index = "index.php";
          extraConfig = ''
            sendfile off;
          '';
        };
        "~ \\.php$" = {
          extraConfig = ''
            include ${config.services.nginx.package}/conf/fastcgi_params ;
            fastcgi_param SCRIPT_FILENAME $request_filename;
            fastcgi_param modHeadersAvailable true; #Avoid sending the security headers twice
            fastcgi_pass unix:${config.services.phpfpm.pools.firefly-iii.socket};
          '';
        };
      };
    };
  };
}
