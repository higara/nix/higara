{config, ...}: {
  sops.secrets."kavita-keyfile" = {
    sopsFile = ./secrets/media-server/secrets.yaml;
  };
  services.kavita = {
    enable = true;
    user = "kavita";
    dataDir = "/var/lib/kavita";
    tokenKeyFile = config.sops.secrets."kavita-keyfile".path;
  };
  services.nginx.virtualHosts."books.ginstoo.net" = {
    enableACME = true;
    acmeRoot = null;
    forceSSL = true;
    locations."/" = {
      proxyPass = "http://localhost:5000";
      proxyWebsockets = true;
      extraConfig =
        # required when the target is also TLS server with multiple hosts
        "proxy_ssl_server_name on;";
    };
  };
}
