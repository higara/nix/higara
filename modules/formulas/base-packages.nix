{ pkgs, ... }:
{
  environment.systemPackages = with pkgs; [
    bind  # Domain name server (comes with lots of DNS tools)
    file  # A program that shows the type of files
    fzf  # A command-line fuzzy finder written in Go
    htop  # An interactive process viewer
    jq  # A lightweight and flexible command-line JSON processor
    kitty.terminfo  # Terminfo for kitty, a GPU-based terminal emulator
    man  # An implementation of the standard Unix documentation system accessed
         # using the man command
    man-pages  # Linux development manual pages
    ncdu  # Disk usage analyzer with an ncurses interface
    tree  # Command to produce a depth indented directory listing
    unzip  #  An extraction utility for archives compressed in .zip format
    vim  # The most popular clone of the VI editor
    wget  # Tool for retrieving files using HTTP, HTTPS, and FTP
  ];

  programs.git.enable = true;
  programs.mtr.enable = true;
}
