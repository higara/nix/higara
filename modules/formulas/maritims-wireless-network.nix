{config, ...}: {
  sops.secrets.maritims-psk = {
    sopsFile = ../chassis/laptop-secrets.yaml;
  };

  sops.templates."wireless-maritims.env".content = ''
    MARITIMS_WIFI_PASSWORD = "${config.sops.placeholder.maritims-psk}"
  '';

  networking.networkmanager.ensureProfiles = {
    environmentFiles = [
      config.sops.templates."wireless-maritims.env".path
    ];
    profiles = {
      maritims = {
        connection = {
          id = "maritims";
          type = "wifi";
        };
        wifi = {
          mode = "infrastructure";
          ssid = "MOVISTAR_PLUS_2749";
        };
        wifi-security = {
          auth-alg = "open";
          key-mgmt = "wpa-psk";
          psk = "$MARITIMS_WIFI_PASSWORD";
        };
        ipv4 = {
          method = "auto";
        };
        ipv6 = {
          addr-gen-mode = "default";
          method = "auto";
        };
        proxy = {
        };
      };
    };
  };
}
