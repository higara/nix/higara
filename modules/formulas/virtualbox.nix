{ config, ... }:
{
  virtualisation.virtualbox = {
    host.enable = true;
    guest = {
      enable = true;
      x11 = true;
    };
  };

  users.extraGroups.vboxusers.members = builtins.filter (
    x: builtins.elem "wheel" config.users.users."${x}".extraGroups
  ) (
    builtins.attrNames config.users.users
  );

}
