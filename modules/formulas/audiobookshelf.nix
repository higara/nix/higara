{config, ...}: {
  networking.firewall.allowedTCPPorts = [80 443];
  services.audiobookshelf = {
    enable = true;
    openFirewall = false;
  };

  sops.secrets."do-auth-token" = {
    sopsFile = ./secrets/media-server/secrets.yaml;
  };

  security.acme = {
    acceptTerms = true;
    defaults = {
      email = "letsencrypt@stooj.org";
      dnsProvider = "digitalocean";
      credentialFiles = {
        "DO_AUTH_TOKEN_FILE" = config.sops.secrets."do-auth-token".path;
      };
    };
  };

  services.nginx = {
    enable = true;
    recommendedProxySettings = true;
    virtualHosts."audiobooks.ginstoo.net" = {
      enableACME = true;
      acmeRoot = null;
      forceSSL = true; # Optional, but highly recommended
      locations."/" = {
        proxyPass = "http://127.0.0.1:${builtins.toString config.services.audiobookshelf.port}";
        proxyWebsockets = true;
        extraConfig = ''
          proxy_redirect http:// $scheme://;
        '';
      };
    };
  };
}
