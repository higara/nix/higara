{config, ...}: {
  networking.firewall.allowedTCPPorts = [80 443];
  services.jellyfin = {
    enable = true;
    user = "jellyfin";
    group = "users";
    cacheDir = "/var/cache/jellyfin";
    logDir = "/var/log/jellyfin";
    openFirewall = false;
    dataDir = "/var/lib/jellyfin";
    configDir = "${config.services.jellyfin.dataDir}/config";
  };

  sops.secrets."do-auth-token" = {
    sopsFile = ./secrets/media-server/secrets.yaml;
  };

  security.acme = {
    acceptTerms = true;
    defaults = {
      email = "letsencrypt@stooj.org";
      dnsProvider = "digitalocean";
      credentialFiles = {
        "DO_AUTH_TOKEN_FILE" = config.sops.secrets."do-auth-token".path;
      };
    };
  };

  services.nginx = {
    enable = true;
    # Don't use ipv6 for dns lookups
    resolver.ipv6 = false;
    recommendedGzipSettings = true;
    recommendedOptimisation = true;
    recommendedProxySettings = true;
    recommendedTlsSettings = true;
    ## The default `client_max_body_size` is 10M, this might not be enough for some posters, etc.
    clientMaxBodySize = "20M";
    virtualHosts = {
      "media.ginstoo.net" = {
        enableACME = true;
        acmeRoot = null;
        forceSSL = true;
        locations."/" = {
          proxyPass = "http://127.0.0.1:8096";
          proxyWebsockets = true;
          extraConfig = ''
            # required when the target is also TLS server with multiple hosts
            proxy_ssl_server_name on;

            # Disable buffering when the nginx proxy gets very resource heavy upon streaming
            proxy_buffering off;
          '';
        };
        locations."/socket" = {
          proxyPass = "http://127.0.0.1:8096";
          extraConfig = ''
            # Proxy Jellyfin Websockets traffic
            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "upgrade";
          '';
        };
      };
    };
  };
}
