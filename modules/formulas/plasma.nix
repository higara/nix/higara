{...}: {
  # Enable the X11 windowing system.
  services.xserver = {
    enable = true;
  };

  # Enable the Plasma 6 Desktop Environment.
  # services.displayManager.sddm.enable = true;
  services.desktopManager.plasma6.enable = true;
  programs.kdeconnect.enable = true;
  # Attempt to fix window decorations
  programs.dconf.enable = true;
  qt = {
    enable = true;
    platformTheme = "gnome";
    style = "adwaita-dark";
  };
}
