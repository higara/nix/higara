{pkgs, ...}: {
  environment.systemPackages = with pkgs; [
    polybarFull
  ];
  services.libinput.touchpad.disableWhileTyping = true;
  services.displayManager.defaultSession = "none+i3";
  services.xserver = {
    enable = true;
    enableTearFree = true;
    windowManager.i3 = {
      enable = true;
      extraPackages = with pkgs; [
        dunst
        i3lock
        rofi
        rofi-emoji
        rofi-pass
        unipicker
        xclip
        xss-lock
      ];
    };
  };
}
