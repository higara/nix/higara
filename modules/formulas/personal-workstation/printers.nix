{pkgs, ...}: {
  environment.systemPackages = with pkgs; [
    foomatic-db-ppds
  ];
  services.printing.enable = true;
  services.avahi = {
    enable = true;
    nssmdns4 = true; # Use settings from below
    openFirewall = true;
  };
}
