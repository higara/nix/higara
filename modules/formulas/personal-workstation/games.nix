{pkgs, ...}: {
  programs.steam = {
    enable = true;
    remotePlay.openFirewall = true;
  };
  hardware = {
    graphics = {
      enable = true;
      enable32Bit = pkgs.system == "x86_64-linux";
    };
  };
  environment.systemPackages = with pkgs; [
    prismlauncher # A free, open source launcher for Minecraft
    lutris # Open Source gaming platform for GNU/Linux
    uqm # Remake of Star Control II
    wineWowPackages.stable # An Open Source implementation of the Windows API on top of X, OpenGL, and Unix
    winetricks # A script to install DLLs needed to work around problems in Wine
    protontricks # A simple wrapper for running Winetricks commands for Proton-enabled games
  ];
}
