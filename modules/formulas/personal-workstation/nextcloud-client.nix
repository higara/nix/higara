{ pkgs, ... }:
{
  environment.systemPackages = with pkgs; [
    nextcloud-client
  ];
  fileSystems."/home/stooj/backups" = {
    device = "/home/stooj/nextcloud/ginstoo/backups";
    options = [ "bind" ];
  };
  fileSystems."/home/stooj/documents" = {
    device = "/home/stooj/nextcloud/ginstoo/documents";
    options = [ "bind" ];
  };
  fileSystems."/home/stooj/downloads" = {
    device = "/home/stooj/nextcloud/ginstoo/downloads";
    options = [ "bind" ];
  };
  fileSystems."/home/stooj/music" = {
    device = "/home/stooj/nextcloud/ginstoo/music";
    options = [ "bind" ];
  };
  fileSystems."/home/stooj/pictures" = {
    device = "/home/stooj/nextcloud/ginstoo/pictures";
    options = [ "bind" ];
  };
  fileSystems."/home/stooj/tmp" = {
    device = "/home/stooj/nextcloud/ginstoo/tmp";
    options = [ "bind" ];
  };
  fileSystems."/home/stooj/videos" = {
    device = "/home/stooj/nextcloud/ginstoo/videos";
    options = [ "bind" ];
  };
}
