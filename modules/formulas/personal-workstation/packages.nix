{pkgs, ...}: {
  environment.systemPackages = with pkgs; [
    element-desktop
    libation
    signal-desktop
    telegram-desktop
    whatsapp-for-linux
    yt-dlp
  ];
}
