{pkgs, ...}: {
  environment.systemPackages = with pkgs; [
    alacritty
    bitwarden
    bitwarden-cli
    brightnessctl
    buku
    bukubrow
    csvkit
    devbox
    distrobox
    dogdns
    feh
    libnotify
    mediaelch-qt6
    mpc-cli
    neovim-qt
    (wrapOBS {
      plugins = with obs-studio-plugins; [
        obs-pipewire-audio-capture
      ];
    })
    pass
    pavucontrol
    qpdf
    ripgrep
    sonixd
    terminator
    xorg.xhost
    yq
    zoom-us
  ];
  programs.chromium = {
    enable = true;
  };
}
