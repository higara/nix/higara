{pkgs, ...}: {
  environment.systemPackages = with pkgs; [
    hddtemp
    lm_sensors
  ];
}
