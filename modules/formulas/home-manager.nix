{
  inputs,
  outputs,
  ...
}: {
  home-manager = {
    useGlobalPkgs = true;
    useUserPackages = true;
    backupFileExtension = "hm~";
    extraSpecialArgs = {
      inherit inputs outputs;
    };
    sharedModules = [
      inputs.sops-nix.homeManagerModules.sops
      inputs.nixvim.homeManagerModules.nixvim
    ];
    users = {
      stooj = import ../users/home/stooj;
    };
  };
}
