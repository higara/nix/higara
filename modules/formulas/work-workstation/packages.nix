{pkgs, ...}: {
  environment.systemPackages = with pkgs; [
    _1password-cli
    _1password-gui
    dialog
    distrobox
    lynx
    notifymuch
    slack
    taskwarrior-tui
    timewarrior
    workrave
  ];
}
