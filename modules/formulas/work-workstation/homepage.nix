{...}: {
  services = {
    homepage-dashboard = {
      enable = true;
      # environmentFile = config.sops.secrets.homepage-env.path;
      # package = pkgs.unstable.homepage-dashboard;
      bookmarks = [
        {
          Development = [
            {
              GitHub = [
                {
                  abbr = "GH";
                  href = "https://github.com/pulumi";
                  icon = "github-light.png";
                }
              ];
            }
            {
              "Pulumi Registry" = [
                {
                  abbr = "PR";
                  href = "https://www.pulumi.com/registry/";
                  icon = "si-pulumi";
                }
              ];
            }
            {
              "Terraform Registry" = [
                {
                  abbr = "TR";
                  href = "https://registry.terraform.io/";
                  icon = "terraform.png";
                }
              ];
            }
          ];
        }
        {
          NixOS = [
            {
              "NixOS Discourse" = [
                {
                  abbr = "ND";
                  href = "https://discourse.nixos.org";
                  icon = "https://discourse.nixos.org/uploads/default/original/2X/c/cb4fe584627b37e7c1d5424e9cec0bb30fdb6c4d.png";
                }
              ];
            }
            {
              "Nixpkgs" = [
                {
                  abbr = "NP";
                  href = "https://github.com/NixOS/nixpkgs";
                  icon = "https://avatars.githubusercontent.com/u/487568?s=48&v=4";
                }
              ];
            }
            {
              "NixOS Search" = [
                {
                  abbr = "NS";
                  href = "https://search.nixos.org";
                  icon = "https://search.nixos.org/images/nix-logo.png";
                }
              ];
            }
            {
              "Home Manager" = [
                {
                  abbr = "HM";
                  href = "https://nix-community.github.io/home-manager/options.xhtml";
                  icon = "https://avatars.githubusercontent.com/u/33221035?s=200&v=4";
                }
              ];
            }
            {
              "NixOS Wiki" = [
                {
                  abbr = "NIXWIKI";
                  href = "https://wiki.nixos.org";
                  icon = "https://wiki.nixos.org/nixos.png";
                }
              ];
            }
            {
              FlakeHub = [
                {
                  abbr = "FH";
                  href = "https://flakehub.com";
                  icon = "https://flakehub.com/favicon.png";
                }
              ];
            }
          ];
        }
        {
          Social = [
            {
              Mastodon = [
                {
                  abbr = "MD";
                  href = "https://linuxrocks.online/home";
                  icon = "mastodon.png";
                }
              ];
            }
            {
              Bluesky = [
                {
                  abbr = "BS";
                  href = "https://bsky.app/notifications";
                  icon = "https://bsky.app/static/favicon-32x32.png";
                }
              ];
            }
            {
              LinkedIn = [
                {
                  abbr = "LI";
                  href = "https://www.linkedin.com/in/stooj/";
                  icon = "linkedin.png";
                }
              ];
            }
          ];
        }
        {
          Productivity = [
            {
              Calendar = [
                {
                  abbr = "GCAL";
                  href = "https://calendar.google.com";
                  icon = "https://ssl.gstatic.com/calendar/images/dynamiclogo_2020q4/calendar_31_2x.png";
                }
              ];
            }
            {
              Gmail = [
                {
                  abbr = "GMAIL";
                  href = "https://mail.google.com";
                  icon = "gmail.png";
                }
              ];
            }
          ];
        }
      ];
      settings = {
        background = {
          # image = "https://raw.githubusercontent.com/wimpysworld/nix-config/main/nixos/_mixins/configs/backgrounds/DeterminateColorway-2560x1440.png";
          blur = "sm"; # sm, md, xl... see https://tailwindcss.com/docs/backdrop-blur
          saturate = "75"; # 0, 50, 100... see https://tailwindcss.com/docs/backdrop-saturate
          brightness = "75"; # 0, 50, 75... see https://tailwindcss.com/docs/backdrop-brightness
          opacity = "100"; # 0-100
        };
        color = "zinc";
        # favicon = "https://wimpysworld.com/favicon.ico";
        headerStyle = "boxed";
        hideVersion = true;
        #layout = {
        #  Links = {
        #    style = "row";
        #    columns = 4;
        #  };
        #};
        showStats = true;
        title = "Homepage: ashford";
      };
      widgets = [
        # {
        #   logo = {
        #     icon = "https://wimpysworld.com/profile.webp";
        #   };
        # }
        #{
        #  datetime = {
        #    format = {
        #      dateStyle = "short";
        #      hourCycle = "h23";
        #      timeStyle = "short";
        #    };
        #  };
        #}
        #{
        #  greeting = {
        #    text_size = "xl";
        #    text = "Greeting Text";
        #  };
        #}
        #{
        #  quicklaunch = {
        #    provider = "custom";
        #    target = "_blank";
        #    url = "https://kagi.com/search?q=";
        #    searchDescriptions = true;
        #    hideInternetSearch = false;
        #    showSearchSuggestions = false;
        #    hideVisitURL = false;
        #  };
        #}
        {
          search = {
            provider = "custom";
            target = "_blank";
            url = "https://kagi.com/search?q=";
          };
        }
        {
          resources = {
            label = "ashford";
            cpu = true;
            cputemp = true;
            memory = false;
            refresh = 2000;
            uptime = true;
            units = "metric";
          };
        }
        {
          resources = {
            label = "/";
            disk = ["/"];
            diskUnits = "gigabytes";
            expanded = true;
          };
        }
        {
          resources = {
            label = "/home";
            disk = ["/home"];
            diskUnits = "gigabytes";
            expanded = true;
          };
        }
        {
          openmeteo = {
            label = "Weather";
            latitude = "39.47";
            longitude = "-0.376389";
            timezone = "Europe/Madrid";
            units = "metric";
          };
        }
      ];
    };
  };
}
