{...}: {
  programs.chromium = {
    extraOpts = {
      "BrowserNetworkTimeQueriesEnabled" = false;
      "DeviceMetricsReportingEnabled" = false;
      "DomainReliabilityAllowed" = false;
      "FeedbackSurveysEnabled" = false;
      "MetricsReportingEnabled" = false;
      "SpellCheckServiceEnabled" = false;
      # Misc; DNS
      "BuiltInDnsClientEnabled" = false;
      # Misc; Tabs
      "NTPCardsVisible" = false;
      "NTPCustomBackgroundEnabled" = false;
      "NTPMiddleSlotAnnouncementVisible" = false;
      # Misc; Downloads
      "DefaultDownloadDirectory" = "~/downloads/incoming";
      "DownloadDirectory" = "~/downloads/incoming";
      "PromptForDownloadLocation" = true;
      # Misc
      "AllowSystemNotifications" = true;
      "AutofillAddressEnabled" = false;
      "AutofillCreditCardEnabled" = false;
      "BackgroundModeEnabled" = false;
      "BookmarkBarEnabled" = false;
      "BrowserAddPersonEnabled" = true;
      "BrowserLabsEnabled" = false;
      "PromotionalTabsEnabled" = false;
      "ShoppingListEnabled" = false;
      "ShowFullUrlsInAddressBar" = true;
      "SpellcheckEnabled" = true;
      "SpellcheckLanguage" = [
        "en-GB"
        "en-US"
      ];
      # Cloud Reporting
      "CloudReportingEnabled" = false;
      "CloudProfileReportingEnabled" = false;
      # Content settings
      "DefaultGeolocationSetting" = 3;
      "DefaultImagesSetting" = 1;
      "DefaultPopupsSetting" = 1;
      "DefaultSearchProviderEnabled" = true;
      # Generative AI; these settings disable the AI features to prevent data collection
      "CreateThemesSettings" = 2;
      "DevToolsGenAiSettings" = 2;
      "GenAILocalFoundationalModelSettings" = 1;
      "HelpMeWriteSettings" = 2;
      "TabOrganizerSettings" = 2;
      # Network
      "ZstdContentEncodingEnabled" = true;
      # Password manager
      "PasswordDismissCompromisedAlertEnabled" = true;
      "PasswordLeakDetectionEnabled" = false;
      "PasswordManagerEnabled" = false;
      "PasswordSharingEnabled" = false;
      # Printing
      "PrintingPaperSizeDefault" = "iso_a4_210x297mm";
      # Related Website Sets
      "RelatedWebsiteSetsEnabled" = false;
      # Safe Browsing
      "SafeBrowsingExtendedReportingEnabled" = false;
      "SafeBrowsingProtectionLevel" = 1;
      "SafeBrowsingProxiedRealTimeChecksAllowed" = false;
      "SafeBrowsingSurveysEnabled" = false;
      # Startup, Home and New Tab Page
      "HomePageIsNewTabPage" = true;
      "RestoreOnStartup" = 1;
      "ShowHomeButton" = false;
      # Default search provider; Kagi
      "DefaultSearchProviderAlternateURLs" = [
        "https://kagi.com/search?q={searchTerms}"
      ];
      # - https://help.kagi.com/kagi/getting-started/setting-default.html
      "DefaultSearchProviderImageURL" = "https://assets.kagi.com/v2/apple-touch-icon.png";
      "DefaultSearchProviderKeyword" = "kagi";
      "DefaultSearchProviderName" = "Kagi";
      "DefaultSearchProviderSearchURL" = "https://kagi.com/search?q={searchTerms}";
      "DefaultSearchProviderSuggestURL" = "https://kagi.com/api/autosuggest?q={searchTerms}";
      "HomePageLocation" = "http://localhost:8082";
      "NewTabPageLocation" = "http://localhost:8082";
    };
    extensions = [
      "cjpalhdlnbpafiamejdnhcphjbkeiagm" # uBlock Origin
      "mdjildafknihdffpkfmmpnpoiajfjnjd" # Consent-O-Matic
      "mnjggcdmjocbbbhaepdhchncahnbgone" # SponsorBlock for YouTube
      "gebbhagfogifgggkldgodflihgfeippi" # Return YouTube Dislike
      "fdpohaocaechififmbbbbbknoalclacl" # GoFullPage
      "cdglnehniifkbagbbombnjghhcihifij" # Kagi
      "ihennfdbghdiflogeancnalflhgmanop" # Gruvbox theme
    ];
  };
}
