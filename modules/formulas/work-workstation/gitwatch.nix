{...}: {
  services.gitwatch.qutebrowser-sessions = {
    enable = true;
    path = "/home/stooj/.local/state/qutebrowser/default/sessions";
    remote = "git@github.com:stooj/pulumi-qutebrowser-sessions.git";
    user = "stooj";
  };
}
