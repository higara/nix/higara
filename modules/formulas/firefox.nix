{pkgs, ...}: {
  programs.firefox = {
    enable = true;
    package = pkgs.firefox;
    languagePacks = [
      "en-GB"
      "en-US"
    ];
  };
  services.gnome.gnome-browser-connector.enable = true;
}
