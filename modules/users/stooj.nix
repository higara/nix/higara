{ pkgs, ... }:
let
  shared = import ../shared.nix;
in
{
  nix.settings.trusted-users = [ "stooj" ];
  users.groups = {
    stooj = {
      gid = 1000;
    };
  };
  users.users.stooj = {
    description = "Stoo Johnston";
    isNormalUser = true;
    home = "/home/stooj";
    group = "stooj";
    extraGroups = [ "wheel" ];
    shell = pkgs.zsh;
    openssh.authorizedKeys.keys = shared.stooj.sshkeys;
  };
}
