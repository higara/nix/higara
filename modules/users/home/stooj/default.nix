{
  inputs,
  config,
  lib,
  options,
  specialArgs,
  extraSpecialArgs,
  ...
}: let
  inherit (specialArgs) isWorkstation isPersonalWorkstation isWorkWorkstation;
in {
  imports =
    [
      ./atuin.nix
      ./bash.nix
      ./bat.nix
      ./buku.nix
      ./direnv.nix
      ./envvars.nix
      ./eza.nix
      ./git.nix
      ./neovim
      ./powerlevel10k.nix
      ./shell.nix
      ./ssh.nix
      ./starship.nix
      ./tmux.nix
      ./xdg.nix
      ./zsh.nix
      ./zsh-antidote.nix
    ]
    ++ lib.optionals isWorkstation [
      ./workstation/alacritty.nix
      ./workstation/beets.nix
      ./workstation/bootstrap-homedir.nix
      ./workstation/cava.nix
      ./workstation/chromium.nix
      ./workstation/distrobox.nix
      ./workstation/dog.nix
      ./workstation/firefox.nix
      ./workstation/flameshot.nix
      ./workstation/git.nix
      ./workstation/gpg.nix
      # ./workstation/hyprland.nix
      ./workstation/i3wm.nix
      ./workstation/keybase.nix
      ./workstation/kitty.nix
      ./workstation/libvirt.nix
      ./workstation/mopidy.nix
      ./workstation/mpv.nix
      ./workstation/ncmpcpp.nix
      ./workstation/newsboat.nix
      ./workstation/pass.nix
      ./workstation/polybar.nix
      ./workstation/qutebrowser.nix
      ./workstation/rofi.nix
      # Disabled, does not build atm
      # ./workstation/virtualbox.nix
      ./workstation/virtualenvwrapper.nix
      ./workstation/zsh.nix
    ]
    ++ lib.optionals isPersonalWorkstation [
      ./personal-workstation/chromium.nix
      ./personal-workstation/git.nix
      ./personal-workstation/mr.nix
      ./personal-workstation/neovim.nix
      ./personal-workstation/nextcloud-client.nix
      ./personal-workstation/qutebrowser.nix
      ./personal-workstation/ssh.nix
    ]
    ++ lib.optionals isWorkWorkstation [
      ./work-workstation/chromium.nix
      ./work-workstation/envvars.nix
      ./work-workstation/git.nix
      ./work-workstation/home-manager.nix
      ./work-workstation/lieer.nix
      ./work-workstation/mail.nix
      ./work-workstation/mr.nix
      ./work-workstation/neovim.nix
      ./work-workstation/poetry.nix
      ./work-workstation/pulumi-scripts.nix
      ./work-workstation/qutebrowser.nix
      ./work-workstation/rclone.nix
      ./work-workstation/taskwarrior.nix
      ./work-workstation/timescripts.nix
      ./work-workstation/timewarrior.nix
      ./work-workstation/workrave.nix
    ];

  home = {
    username = "stooj";
    homeDirectory = "/home/stooj";
  };

  programs.home-manager.enable = true;

  systemd.user.startServices = "sd-switch";

  home.stateVersion = "23.11";
}
