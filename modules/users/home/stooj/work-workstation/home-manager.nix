{ config, ... }:
{
  home = {
    # This is required for user-level secrets.
    # If you add secrets to your personal workstation, it should be added there
    # too.
    activation.setupEtc = config.lib.dag.entryAfter [ "writeBoundary" ] ''
      /run/current-system/sw/bin/systemctl start --user sops-nix
    '';
  };
}
