{...}: {
  home.file = {
    ".local/bin/pproject" = {
      executable = true;
      source = ./files/pulumi-scripts/pproject;
    };
    ".local/bin/pbootproject" = {
      executable = true;
      source = ./files/pulumi-scripts/new-pulumi-project;
    };
    ".local/bin/psearch" = {
      executable = true;
      source = ./files/pulumi-scripts/psearch;
    };
    ".local/bin/ppeek" = {
      executable = true;
      source = ./files/pulumi-scripts/ppeek;
    };
    ".local/bin/taskadd" = {
      executable = true;
      source = ./files/pulumi-scripts/taskadd;
    };
    ".local/bin/tickettime" = {
      executable = true;
      source = ./files/pulumi-scripts/tickettime;
    };
    ".local/bin/twadmin" = {
      executable = true;
      source = ./files/pulumi-scripts/twadmin;
    };
  };
}
