{pkgs, ...}: {
  programs.nixvim = {
    plugins = {
      neorg = {
        modules = {
          "core.dirman" = {
            config = {
              workspaces = {
                about = "~/code/docs/neorg/about";
                ginstoo = "~/code/docs/neorg/ginstoo";
                pulumi = "~/documents/pulumi-neorg";
              };
              default_workspace = "pulumi";
            };
          };
        };
      };
    };
    extraPlugins = [
      pkgs.vimPlugins.taskwiki
    ];
    extraPython3Packages = p:
      with p; [
        packaging
        six
        tasklib
      ];
    globals = {
      taskwiki_taskrc_location = "~/.config/task/taskrc";
    };
    # plugins.neorg.modules = {
    #   "core.dirman".config.workspaces = {
    #     pulumi = "~/documents/pulumi-neorg";
    #   };
    # };
    extraConfigLua = ''
      vim.g.vimwiki_list = {
            {
                name = "Pulumi docs",
                path = "~/documents/pulumi-docs",
                path_html = '~/.cache/pulumi-docs',
                ext = '.md',
                syntax = 'markdown'
            },
            {
                name = 'Ginstoo Wiki',
                path = '~/code/docs/ginstoo',
                path_html = '~/.cache/ginstoo_wiki'
            }
        }
    '';
  };
  # home.file = {
  #   ".config/nvim/lua/v2/plugins/remote-containers.lua" = {
  #     source = ./files/neovim/lua/v2/plugins/remote-containers.lua;
  #   };
  # };
}
