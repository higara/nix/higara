{ pkgs, config, ... }:
{
  home.packages = [
    pkgs.mr
  ];

  sops = {
    defaultSopsFile = ./secrets.yaml;
    gnupg.sshKeyPaths = [];
    gnupg.home = "${config.home.homeDirectory}/.gnupg";

    secrets = {
      "mrconfig" = {
        mode = "0600";
        path = "${config.home.homeDirectory}/.mrconfig";
      };
    };
  };
}
