{config, ...}: {
  sops = {
    defaultSopsFile = ./secrets.yaml;
    gnupg.sshKeyPaths = [];
    gnupg.home = "${config.home.homeDirectory}/.gnupg";

    secrets = {
      "qutebrowser_quickmarks" = {
        mode = "0600";
        path = "${config.home.homeDirectory}/.config/qutebrowser/quickmarks";
      };
    };
  };
  programs.qutebrowser.extraConfig = ''
    with config.pattern('*://pulumi.zendesk.com') as p:
        p.content.javascript.clipboard = 'access-paste'

    with config.pattern('*://*.lightning.force.com') as p:
        p.content.notifications.enabled = True

    with config.pattern('*://metabase.corp.pulumi.com') as p:
        p.content.notifications.enabled = True

    with config.pattern('*://github.com') as p:
        p.content.javascript.clipboard = 'access-paste'

  '';
}
