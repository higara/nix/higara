{...}: {
  home.file = {
    "bin/sync-gdrive" = {
      executable = true;
      text = ''
        #!/usr/bin/env bash

        set -e

        usage() {
            echo "Usage:"
            echo "$0 [OPTIONS]"
            echo ""
            echo "Options:"
            echo "-h | --help       Show this message"
            echo "-d | --dry-run    Don't actually sync files, just do a dry run"
            echo "-p | --progress   Show progress of transfers"
        }

        DRYRUN=""
        PROGRESS=""
        while true; do
            case $1 in
            -h | -\? | --help)
                usage
                exit
                ;;
            -d | --dry-run)
                DRYRUN=--dry-run
                ;;
            -p | --progress)
                PROGRESS=--progress
                ;;
            --) # End of all options
                shift
                break
                ;;
            -?*)
                printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
                ;;
            *)
                break
                ;;
            esac
            shift
        done

        QUTEBROWSER_PROFILE=default
        QUTEBROWSER_SESSION_DIR=$XDG_STATE_HOME/qutebrowser/"$QUTEBROWSER_PROFILE"/sessions
        rclone sync $DRYRUN $PROGRESS "$HOME/documents" pulumi-gdrive:documents
        rclone sync $DRYRUN $PROGRESS --exclude "wallpapers/**" "$HOME/pictures" pulumi-gdrive:pictures
        rclone sync $DRYRUN $PROGRESS "$HOME/videos" pulumi-gdrive:videos
        rclone sync $DRYRUN $PROGRESS --exclude "hooks/**" "$HOME/.local/share/task" pulumi-gdrive:meta/task
        rclone sync $DRYRUN $PROGRESS "$HOME/.local/share/timewarrior" pulumi-gdrive:meta/timewarrior
        rclone sync $DRYRUN $PROGRESS "$HOME/.local/share/buku" pulumi-gdrive:meta/bookmarks
        if [ "$DRYRUN" == "" ]; then
            pushd "$QUTEBROWSER_SESSION_DIR"
            git push
            popd
        fi
      '';
    };
  };
}
