{pkgs, ...}: {
  programs.taskwarrior = {
    enable = true;
    package = pkgs.taskwarrior3;
    colorTheme = "solarized-dark-256";
    config = {
      report.tickets.description = "Zendesk tickets";
      report.tickets.columns = ["id" "description.count" "modified"];
      report.tickets.filter = "status:pending and +ticket and +zendesk";
      report.tickets.sort = "modified+";
    };
  };
  home.shellAliases = {
    # Show what I should be working on when I'm not working on tickets
    tnext = "task next -ticket limit:5";
  };
}
