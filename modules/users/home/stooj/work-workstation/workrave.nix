{ pkgs, ... }:
{
  home.file = {
    ".config/i3/autostart.d/workrave" = {
      executable = true;
      text = ''
        #!/usr/bin/env bash

        ######################################################################
        #  File managed by Home Manager
        #  Your changes will be overwritten.
        ######################################################################
        "${pkgs.workrave}/bin/workrave" &
      '';
    };
    "bin/meeting" = {
      executable = true;
      text = ''
        #!/usr/bin/env bash

        ######################################################################
        #  File managed by Home Manager
        #  Your changes will be overwritten.
        ######################################################################

        start="start"
        stop="stop"

        if [ $# -eq 0 ]; then
            action=$start
        elif [ "$1" == $start ]; then
            action=$start
        elif [ "$1" == $stop ]; then
            action=$stop
        else
            echo "Didn't recognise action" >&2
            exit 1
        fi

        # From https://github.com/rcaelers/workrave/blob/main/contrib/suspend-workrave-on-screensaver-activation/suspend-workrave-on-screensaver-activation
        setWorkraveOperationMode () {
            dbus-send --session --dest=org.workrave.Workrave --type=method_call \
            /org/workrave/Workrave/Core org.workrave.CoreInterface.SetOperationMode \
            string:"$1"
        }

        if [ $action == $start ]; then
            notify-send "Notifications and screen lock paused"
            sleep 5
            dunstctl close
            dunstctl set-paused true
            xset s off
            xset -dpms
            setWorkraveOperationMode "suspended"
        elif [ $action == $stop ]; then
            setWorkraveOperationMode "normal"
            xset +dpms
            xset s on
            dunstctl set-paused false
            sleep 5
            notify-send "Notifications and screen lock enabled"
        else
            :
        fi
      '';
    };
  };
}
