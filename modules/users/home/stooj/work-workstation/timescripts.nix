{...}: {
  home.file = {
    "bin/timefor" = {
      executable = true;
      source = ./files/timescripts/timefor;
    };
    "bin/timein" = {
      executable = true;
      source = ./files/timescripts/timein;
    };
  };
}
