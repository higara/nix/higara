{ ... }:
{
  home.file = {
    ".local/share/task/hooks/on-modify.timewarrior" = {
      executable = true;
      source = ./files/timewarrior/on-modify.timewarrior;
    };
    ".config/timewarrior/timewarrior.cfg" = {
      text = ''
      '';
    };
  };
}
