{pkgs, ...}: {
  home = {
    file = {
      ".config/neomutt/gruvbox/colors-gruvbox-shuber-extended.muttrc".source =
        ./files/neomutt/colors-gruvbox-shuber-extended.muttrc;
      ".config/neomutt/gruvbox/colors-gruvbox-shuber.muttrc".source =
        ./files/neomutt/colors-gruvbox-shuber.muttrc;
      ".config/neomutt/config/common.muttrc".source =
        ./files/neomutt/common.muttrc;
      ".config/neomutt/config/mailcap".source =
        ./files/neomutt/mailcap;
      ".config/notifymuch/notifymuch.cfg".source =
        ./files/notifymuch/notifymuch.cfg;
    };
    shellAliases = {
      mutt = "neomutt";
    };
  };
  programs.neomutt = {
    enable = true;
    vimKeys = true;
    sort = "threads";
    extraConfig = ''
      source ~/.config/neomutt/config/common.muttrc
      source ~/.config/neomutt/gruvbox/colors-gruvbox-shuber.muttrc
      source ~/.config/neomutt/gruvbox/colors-gruvbox-shuber-extended.muttrc
      set mailcap_path = ~/.config/neomutt/config/mailcap
      auto_view text/html
    '';
  };
  programs.lieer.enable = true;
  services.lieer.enable = true;

  accounts.email.maildirBasePath = ".local/share/mail";
  accounts.email.accounts.pulumi = {
    primary = true;
    flavor = "gmail.com";
    folders.inbox = "mail";
    realName = "Stoo Johnston";
    address = "stoo@pulumi.com";
    userName = "stoo@pulumi.com";

    neomutt = {
      enable = true;
      sendMailCommand = "msmtpq --read-envelope-from --read-recipients";
    };
    notmuch = {
      enable = true;
      neomutt.enable = true;
    };

    smtp.tls.enable = true;

    lieer = {
      enable = true;
      sync = {
        enable = true;
        frequency = "*:0/2";
      };
    };
  };

  programs.afew = {
    enable = true;

    # https://afew.readthedocs.io/en/latest/configuration.html
    extraConfig = ''
      [KillThreadsFilter]
      [ArchiveSentMailsFilter]
      [SpamFilter]
    '';
    # [ListMailsFilter]
    # [InboxFilter]
  };
  programs.notmuch = {
    enable = true;
    new.tags = ["new"];
    search.excludeTags = ["trash" "spam"];
    hooks.postNew = ''
      ${pkgs.notifymuch}/bin/notifymuch'';
  };
  programs.msmtp.enable = true;
  systemd.user = {
    services = {
      notmuch-new = {
        Unit = {
          Description = "Run notmuch new as user";
        };
        Service = {
          ExecStart = "${pkgs.notmuch}/bin/notmuch new";
          Environment = [
            "DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus"
          ];
        };
        Install = {
          WantedBy = ["default.target"];
        };
      };
    };
    timers = {
      notmuch-new = {
        Unit = {
          Description = "Run notmuch new every minute.";
        };
        Timer = {
          OnCalendar = "minutely";
          RandomizedDelaySec = 30;
        };
        Install = {WantedBy = ["timers.target"];};
      };
    };
  };
}
