{pkgs, ...}: {
  home.file.".config/distrobox/distrobox.conf" = {
    text = ''
      container_additional_volumes="/nix/store:/nix/store:ro /etc/profiles/per-user:/etc/profiles/per-user:ro /etc/static/profiles/per-user:/etc/static/profiles/per-user:ro"
      ${pkgs.xorg.xhost}/bin/xhost +si:localuser:$USER
    '';
  };
}
