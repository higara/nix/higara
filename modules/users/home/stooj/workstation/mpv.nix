{ ... }:
{
  programs.mpv = {
    enable = true;

    config = {
      # Read https://mpv.io/manual/stable/#options-hwdec before enabling.
      # Also see https://nixos.wiki/wiki/Accelerated_Video_Playback
      hwdec = "auto-safe";
      vo= "gpu";
      profile = "gpu-hq";

      # Prevent harmless warnings/errors when using hardware decoding
      msg-level = "vo=fatal";

      # https://github.com/mpv-player/mpv/issues/4241
      # Pick the best format it can find (up to but not above 720p), chose
      # 60fps or lower, and lastly, ignore the VP9 codec.
      ytdl-format =
        "bestvideo[height<=?720][fps<=?60][vcodec!=?vp9]+bestaudio/best";
    };
  };
}
