#!/bin/sh --
########################################################################
# File managed by Home Manager
# Your changes will be overwritten.
########################################################################

# From https://raw.githubusercontent.com/ayekat/localdir/092d410f6717994d7b663346457377dc291102ce/bin/qutebrowser

# Wrapper around qutebrowser that allows multiple profiles.
#
# We do so by adding a new option -p/--profile  and using the argument to set up
# the following directory structure and symbolic links:
#
# $XDG_RUNTIME_DIR/qutebrowser/$profile/cache → $XDG_CACHE_HOME/qutebrowser/$profile
# $XDG_RUNTIME_DIR/qutebrowser/$profile/data → $XDG_STATE_HOME/qutebrowser/$profile
# $XDG_RUNTIME_DIR/qutebrowser/$profile/data/userscripts → $XDG_DATA_HOME/qutebrowser/userscripts
# $XDG_RUNTIME_DIR/qutebrowser/$profile/config → $XDG_CONFIG_HOME/qutebrowser
# $XDG_RUNTIME_DIR/qutebrowser/$profile/runtime (no symlink, regular directory)
#
# (note the use of the non-standard XDG_STATE_HOME)
#
# We then specify $XDG_RUNTIME_DIR/qutebrowser/$profile as a --basedir, and the
# files will end up in their intended locations (notice how the config directory
# is the same for all profiles, as there is no point in keeping it separate).
#
# DISCLAIMER: The author of this script manages all his configuration files
# manually, so this wrapper script has not been tested for the use case where
# qutebrowser itself writes to these files (and more importantly, if multiple
# such "profiles" simultaneously write to the same configuration file).
#
# YOU HAVE BEEN WARNED.
#
# Written by ayekat in an burst of nostalgy, on a mildly cold wednesday night in
# February 2017.

set -eu

# Constants:
FALSE=0
TRUE=1

# Set default values for the variables as defined in the XDG base directory spec
# (https://specifications.freedesktop.org/basedir-spec/latest/):
XDG_RUNTIME_DIR="${XDG_RUNTIME_DIR:-/run/user/$(id -u)}"
XDG_CONFIG_HOME="${XDG_CONFIG_HOME:-$HOME/.config}"
XDG_CACHE_HOME="${XDG_CACHE_HOME:-$HOME/.cache}"
XDG_DATA_HOME="${XDG_DATA_HOME:-$HOME/.local/share}"
XDG_STATE_HOME="${XDG_STATE_HOME:-$HOME/.var/state}"

# Translate options: remove occurrences of -r/--restore from the list of
# command line arguments and save the session name for later; ignore -R (TODO):
profile='default'
basedir_specified=$FALSE
opts_read=0
while [ $opts_read -lt $# ]; do
	opt="$1" && shift
	case "$opt" in
		(--basedir) basedir_specified=$TRUE ;;
		(-p|--profile) test $# -gt 0 && profile="$1" && shift && continue ;;
		(-[!-]*r) test $# -gt 0 && profile="$1" && shift && opt=${opt%r} ;;
		(-R) continue ;; # TODO
	esac
	set -- "$@" "$opt"
	opts_read=$((opts_read + 1))
done

# Set up profile base directory, unless --basedir has been specified by the
# user:
if [ $basedir_specified -eq $FALSE ]; then
	basedir="$XDG_RUNTIME_DIR/qutebrowser/$profile"
	set -- --basedir "$basedir" "$@"
	mkdir -p \
		"$basedir" \
		"$XDG_CONFIG_HOME/qutebrowser" \
		"$XDG_CACHE_HOME/qutebrowser/$profile" \
		"$XDG_STATE_HOME/qutebrowser/$profile" \
		"$basedir/runtime"
	ln -fsT "$XDG_CONFIG_HOME/qutebrowser" "$basedir/config"
	ln -fsT "$XDG_CACHE_HOME/qutebrowser/$profile" "$basedir/cache"
	ln -fsT "$XDG_STATE_HOME/qutebrowser/$profile" "$basedir/data"
	if [ -d "$XDG_DATA_HOME/qutebrowser/userscripts" ]; then
		ln -fsT "$XDG_DATA_HOME/qutebrowser/userscripts" \
		        "$basedir/data/userscripts"
	fi
fi

# Search "real" qutebrowser executable:
spath="$(readlink -f "$0")"
IFS=:
for p in $PATH; do
	epath="$p"/qutebrowser
	if [ -x "$epath" ] && [ "$(readlink -f "$epath")" != "$spath" ]; then
		exec "$epath" "$@"
	fi
done

# ¯\_(ツ)_/¯
echo 'command not found: qutebrowser' >&2
exit 127
