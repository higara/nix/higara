#!/bin/env python

import argparse
import os
from pathlib import Path
import pprint
import time

from dulwich import porcelain
from dulwich.errors import NotGitRepository
from dulwich.repo import Repo
from notifypy import Notify
from xdg.BaseDirectory import xdg_config_home
import yaml


CONFIGDIR = Path(os.path.join(xdg_config_home, 'bootstrap-homedir'))
USERDIRS = Path(os.path.join(CONFIGDIR, 'directories.yaml'))
USERREPOS = Path(os.path.join(CONFIGDIR, 'repos.yaml'))
USERSYMS = Path(os.path.join(CONFIGDIR, 'symlinks.yaml'))


def main():
    """Main entrypoint for standalone invocation"""
    args = __parse_args()
    no_act = args.no_act

    with open(USERDIRS, 'r') as f:
        directories = yaml.safe_load(f)
    create_directories(directories, no_act)

    with open(USERSYMS, 'r') as f:
        symlinks = yaml.safe_load(f)
    create_symlinks(symlinks, no_act)

    with open(USERREPOS, 'r') as f:
        repos = yaml.safe_load(f)
    clone_repos(repos, no_act)


def __parse_args():
    parser = argparse.ArgumentParser(
        prog='bootstrap-homedir',
        description='''
            Ensures all home directories exist and git repos are cloned
        ''',
    )
    parser.add_argument('-n', '--no-act', action='store_true')

    return parser.parse_args()


def alert(message):
    notification = Notify(
        default_notification_title="Run bootstrap-homedir",
    )
    notification.message = message
    notification.send(block=False)


def create_directories(directories, no_act):
    need_to_create_dirs = False
    for dir in directories:
        p = Path(os.path.join('~', dir)).expanduser()
        if not os.path.isdir(p):
            if no_act:
                need_to_create_dirs = True
            else:
                p.mkdir(parents=True, exist_ok=True)

    if need_to_create_dirs and no_act:
        alert("Directories need to be created")


def create_symlinks(symlinks, no_act):
    need_to_create_symlinks = False
    for sym, data in symlinks.items():
        source = data.get('source', sym)
        source_dir = Path(os.path.join('~', source)).expanduser()
        target = data.get('target', None)
        if target.startswith('/'):
            target_dir = Path(target)
        else:
            target_dir = Path(os.path.join('~', target)).expanduser()
        if target is not None:
            # Only create the symlink if the source exists and the target does
            # not.
            if not target_dir.exists() and source_dir.exists():
                if no_act:
                    need_to_create_symlinks = True
                else:
                    target_dir.symlink_to(source_dir)
    if need_to_create_symlinks and no_act:
        alert("Symlinks need to be created")


def clone_repos(repos, no_act):
    need_to_clone_repos = False
    need_to_create_dirs = False
    code_dir = Path(os.path.join('~', 'code')).expanduser()
    for dir, data in repos.items():
        abs_dir = Path(os.path.join(code_dir, dir))
        if no_act:
            need_to_create_dirs = True
        else:
            abs_dir.mkdir(parents=True, exist_ok=True)
        for repo, remotes in data.items():
            repo_path = Path(os.path.join(abs_dir, repo))
            origin = remotes.pop('origin')
            print(f"Checking {repo_path}")
            try:
                r = Repo(repo_path)
            except NotGitRepository:
                if no_act:
                    need_to_clone_repos = True
                else:
                    print(f"Cloning {origin}")
                    r = porcelain.clone(origin, repo_path)
                    time.sleep(2)
            if not no_act:
                for remote, url in remotes.items():
                    print(f"Adding {url} as {remote} remote")
                    try:
                        porcelain.remote_add(r, remote, url)
                    except porcelain.RemoteExists:
                        print(f"Remote {remote} already exists")

    if need_to_create_dirs and no_act:
        alert("Code directories need to be created")
    if need_to_clone_repos and no_act:
        alert("Repos need to be cloned")


if __name__ == "__main__":
    main()
