import yaml

with open('repos.yaml', 'r') as file:
    repos = yaml.safe_load(file)

for dir, repo in repos.items():
    for target, remotes in repo.items():
        print(f'      "code/{dir}/{target}" = {{')
        origin = remotes.get('origin')
        if len(remotes) > 1:
            checkout = "        checkout = ''\n"
            checkout += f'          git clone \'{origin}\' \'code/{dir}/{target}\'\n'
            checkout += f"            cd {dir}/{target}\n"
            for remote, url in remotes.items():
                if remote == 'origin':
                    continue
                else:
                    checkout += f"            git remote add {remote} {url}\n"
            checkout += "        '';"
        else:
            checkout = f'        checkout = "git clone \'{origin}\' \'code/{dir}/{target}\'";'
        print(checkout)
        print("      };")
