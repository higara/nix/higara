{ pkgs, ... }:
{
  programs.rofi = {
    enable = true;
    extraConfig = {
      show-icons = false;
      icon-theme = "papirus";
      # TODO This doesn't work correctly. It should be a rasi literal string,
      # but not end with a ';'. The built-in rasi mkLiteral appends a 
      # ';' to the end of the line, and this current approach wraps everything
      # in quotes
      # timeout = "{action: \"kb-cancel\"; delay = 0; }";
      # filebrowser = "{directories-first = true; sorting-method = \"name\"; }";
    };
    # TODO Should be configurable based on scaling factor.
    # font = "fira code medium 10";
    location = "center";
    pass = {
      enable = true;
      # See https://github.com/carnager/rofi-pass/blob/master/config.example for
      # more
      extraConfig = ''
        EDITOR='nvim-qt'

        # workaround as per https://github.com/carnager/rofi-pass/issues/226
        help_color="#4872FF"
      '';
    };
    plugins = [
      pkgs.rofi-emoji
    ];
    theme = "gruvbox-dark";
  };

  home.file = {
    ".config/rofi/emoji.rasi".source = ./files/rofi/emoji.rasi;
  };
}
