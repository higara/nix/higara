{ pkgs, ... }:
{
  services.polybar = {
    enable = true;
    config = ./files/polybar/config.ini;
    package = pkgs.polybarFull;
    script = "echo";
  };
  home.file = {
    ".config/polybar/powerline-for-polybar.config".source = ./files/polybar/powerline-for-polybar.config;
    ".config/polybar/colors.config".source = ./files/polybar/colors.config;
    ".config/i3/autostart.d/polybar" = {
      executable = true;
      text = ''
        #!/usr/bin/env bash

        ######################################################################
        #  File managed by Home Manager
        #  Your changes will be overwritten.
        ######################################################################

        # Terminate already running bar instances
                killall -q polybar
        # If all your bars have ipc enabled, you can also use 
        # polybar-msg cmd quit
        #
        cachedir=$HOME/.cache/polybar

        if [ ! -d "$cachedir" ]; then
          mkdir -p "$cachedir"
        fi

        # Launch bar1
        echo "---" | tee -a "$cachedir"/polybar1.log
        polybar main 2>&1 | tee -a "$cachedir"/polybar1.log & disown

        echo "Bars launched..."
      '';
    };
  };
}
