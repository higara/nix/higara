{config, ...}: {
  services.picom = {
    enable = true;
    activeOpacity = 1.0;
    backend = "xrender";
    extraArgs = [];
    fade = false;
    fadeDelta = 10;
    fadeExclude = [
      "window_type *= 'menu'"
    ];
    fadeSteps = [
      0.028
      0.03
    ];
    inactiveOpacity = 0.7;
    menuOpacity = 1.0;
    opacityRules = [
      "100:fullscreen"
      "100:name = 'Zoom Meeting'"
      "100:name = 'zoom_linux_float_video_window'"
      "100:class_g = 'Rofi'"
      "100:class_g = 'mpv'"
    ];
    settings = {
      focus-exclude = [
        "name = 'i3lock'"
        "name = 'Picture-in-Picture'"
        "class_g = 'zoom'"
      ];
    };
    shadow = false;
    shadowExclude = [];
    shadowOffsets = [
      (-15)
      (-15)
    ];
    shadowOpacity = 0.75;
    vSync = true;
    wintypes = {
      popup_menu = {
        opacity = config.services.picom.menuOpacity;
      };
      dropdown_menu = {
        opacity = config.services.picom.menuOpacity;
      };
      utility = {
        opacity = config.services.picom.menuOpacity;
      };
    };
  };

  xsession = {
    enable = true;
    windowManager.i3 = {
      # enable = true;
      # config = rec {
      #   modifier = "Mod4";
      #   floating = {
      #     modifier = config.modifier;
      #   };
      #   focus = {
      #     followMouse = false;
      #   };
      #   keybindings = import ./i3-keybindings.nix config.modifier;
      #   startup = [
      #     { command = "exec --no-startup-id xss-lock -- i3lock --nofork --color=000000 --image=/home/stooj/pictures/lock-screen.png"; }
      #     # TODO Replace picom start with proper picom config
      #     { command = "exec --no-startup-id picom --daemon"; }
      #     # TODO Replace wallpaper services with proper configuration
      #     { command = "exec --no-startup-id systemctl --user start change_i3_background.service"; }
      #     { command = "exec --no-startup-id systemctl --user start change_i3_background.timer"; }
      #     # Adding this here because GDM disables CPP when merging.
      #     # https://bbs.archlinux.org/viewtopic.php?id=169112
      #     { command = "exec --no-startup-id xrdg -merge /home/stooj/.Xresources"; }
      #     # Launch notification daemon
      #     { command = "exec --no-startup-id /usr/bin/dunst"; }
      #   ];
      #   modes = {
      #     resize = {
      #       # These bindings trigger as soon as you enter the resize mode
      #       # Pressing left will shrink the window’s width.
      #       # Pressing right will grow the window’s width.
      #       # Pressing up will shrink the window’s height.
      #       # Pressing down will grow the window’s height.
      #       h = "resize shrink width 10 px or 10 ppt";
      #       j = "resize grow height 10 px or 10 ppt";
      #       k = "resize shrink height 10 px or 10 ppt";
      #       l = "resize grow width 10 px or 10 ppt";

      #       # same bindings, but for the arrow keys
      #       Left = "resize shrink width 10 px or 10 ppt";
      #       Down = "resize grow height 10 px or 10 ppt";
      #       Up = "resize shrink height 10 px or 10 ppt";
      #       Right = "resize grow width 10 px or 10 ppt";

      #       # back to normal: Enter or Escape or $mod+r
      #       Return = "mode 'default'";
      #       Escape = "mode 'default'";
      #       "${config.modifier}+r" = "mode 'default'";
      #     };
      #   };
      # };
      # extraConfig = ''
      #   # Setting border style to pixel eliminates title bars. The border style normal
      #   # allows you to adjust edge border width while keeping your title bar.
      #   default_border pixel
      # '';
    };
  };
  home.file = {
    ".config/i3/config".source = ./files/i3/config;
    ".config/i3/autostart" = {
      source = ./files/i3/autostart;
      executable = true;
    };
    ".config/i3/scripts/exit_menu" = {
      source = ./files/i3/scripts/exit_menu;
      executable = true;
    };
    ".config/systemd/user/change_i3_background.service" = {
      source = ./files/i3/systemd/change_i3_background.service;
    };
    ".config/systemd/user/change_i3_background.timer" = {
      source = ./files/i3/systemd/change_i3_background.timer;
    };
    ".local/bin/change_wallpaper" = {
      source = ./files/i3/change_wallpaper;
      executable = true;
    };
  };

  # This doesn't work
  # Systemd needs the filename of the mount to match the WHERE path
  # (but escaped, use `systemd-escape` to see how it should be escaped
  # The `-` is converted to `\x2d` and nix doesn't like that.
  # So I'm going to do the symlink creation in my bootstrap script.
  # systemd.user.mounts = {
  #   home-stooj-pictures-wallpapers-eve\x2donline = {
  #     Unit = {
  #       Description = "Stoo's wallpapers";
  #       ConditionPathExists="%h/pictures/wallpapers/eve-online";
  #       After = "graphical-session-pre.target";
  #     };
  #     Mount = {
  #       What = "%h/.config/desktop-backgrounds/";
  #       Where = "%h/pictures/wallpapers/eve-online";
  #       Type = "none";
  #       Options = "bind";
  #     };
  #     Install = {
  #       WantedBy = [
  #         "graphical-session.target"
  #       ];
  #     };
  #   };
  # };
}
