{...}: {
  # Configuration inspiration:
  # - https://gist.github.com/anonymous/42d2f5956e7bc8ee1ebc
  # - https://moparx.com/configs/newsbeuter/
  # - https://forums.freebsd.org/threads/newsboat-rss-reader-enable-vim-key-bindings.69448/
  programs.newsboat = {
    enable = true;
    autoReload = true;
    extraConfig = ''
      bind-key j next
      bind-key k prev
      bind-key J next-feed
      bind-key K prev-feed
      bind-key j down article
      bind-key k up article
      bind-key J next article
      bind-key K prev article
      # Colors from "gruvbox" colorscheme https://github.com/morhetz/gruvbox
      color article                              color223 color236
      color background                           color100 color236
      color info                                 color142 color235
      color listfocus                            color214 color239
      color listfocus_unread                     color214 color96
      color listnormal                           color246 color237
      color listnormal_unread                    color175 color237
      highlight article "^Feed:.*"               color175 color237
      highlight article "^Title:.*"              color214 color237 bold
      highlight article "^Author:.*"             color167 color237
      highlight article "^Link:.*"               color109 color237
      highlight article "^Date:.*"               color142 color237
      highlight article "\\[[0-9]\\+\\]"         color208 color237 bold
      highlight article "\\[[^0-9].*[0-9]\\+\\]" color167 color237 bold
    '';
    urls = [
      {
        tags = ["work" "pulumi"];
        url = "https://www.pulumi.com/blog/rss.xml";
      }
      {
        tags = ["work" "k8s" "blog"];
        url = "https://paulbutler.org/posts/index.xml";
      }
      {
        tags = ["work" "ai" "blog" "k8s"];
        url = "https://xeiaso.net/blog.rss";
      }
      {
        tags = ["work" "devops" "blog" "k8s" "hiring"];
        url = "https://matduggan.com/rss/";
      }
      {
        tags = ["blog" "documentation" "neorg" "neovim"];
        url = "https://amartin.codeberg.page/posts/index.xml";
      }
    ];
  };
}
