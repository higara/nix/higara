{ ... }:
{
  home.file = {
    ".config/bootstrap-homedir/directories.yaml".source = ./files/bootstrap-homedir/directories.yaml;
    ".config/bootstrap-homedir/symlinks.yaml".source = ./files/bootstrap-homedir/symlinks.yaml;
    ".config/bootstrap-homedir/repos.yaml".source = ./files/bootstrap-homedir/repos.yaml;
    ".config/i3/autostart.d/bootstrap-homedir" = {
      executable = true;
      text = ''
        #!/bin/sh

        if command -v python >/dev/null 2>&1; then
            /run/current-system/sw/bin/python $HOME/.local/lib/bootstrap-homedir/bootstrap-homedir.py --no-act
        fi
      '';
    };
    ".local/lib/bootstrap-homedir/bootstrap-homedir.py".source =./files/bootstrap-homedir/bootstrap-homedir.py;
  };
}
