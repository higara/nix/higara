{config, ...}: {
  services.flameshot = {
    enable = true;
    settings = {
      General = {
        contrastOpacity = 188;
        drawThickness = 3;
        filenamePattern = "%F_%H-%M-%S";
        fontFamily = "FiraMono Nerd Font";
        saveAsFileExtension = "png";
        savePath = "${config.home.homeDirectory}/pictures/screenshots";
        savePathFixed = true;
        showHelp = false;
        startupLaunch = false;
        userColors = "picker, #cc241d, #98971a, #d79921, #458558, #b16286, #689d6a, #a89984, #fb4934, #b8bb26";
      };
    };
  };
  home.file = {
    ".config/i3/autostart.d/flameshot" = {
      executable = true;
      text = ''
        #!/usr/bin/env bash

        ######################################################################
        #  File managed by Home Manager
        #  Your changes will be overwritten.
        ######################################################################

        flameshot &
      '';
    };
    ".local/bin/delayed-screenshot-selection" = {
      executable = true;
      text = ''
        #!/usr/bin/env bash

        ######################################################################
        #  File managed by Home Manager
        #  Your changes will be overwritten.
        ######################################################################

        for i in $(seq 5 -1 1); do
            notify-send "Taking screenshot in $i"
            sleep 1
            dunstctl close
        done
        flameshot gui
      '';
    };
  };
}
