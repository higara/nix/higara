{ pkgs, ... }:
{
  programs.chromium = {
    enable = true;
    dictionaries = [
      pkgs.hunspellDictsChromium.en_GB
    ];
    extensions = [
      {
        id = "ghniladkapjacfajiooekgkfopkjblpn";  # bukubrow
      }
    ];
  };
  home.file = {
    ".local/bin/default-chromium".source = ./files/chromium/default-chromium;
  };
}
