{...}: {
  programs.gpg = {
    enable = true;
    publicKeys = [
      {
        source = ../../../../../nixos/secrets/keys/stooj.asc;
        trust = 5;
      }
    ];
    scdaemonSettings = {
      # disable-ccid = true;
      # debug-all = true;
      debug-ccid-driver = true;
    };
  };
}
