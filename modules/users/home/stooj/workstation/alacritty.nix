{
  pkgs,
  config,
  ...
}: {
  home.sessionVariables = {
    TERMINAL = pkgs.alacritty;
  };
  systemd.user.sessionVariables = {
    TERMINAL = "alacritty";
  };
  programs.alacritty = {
    enable = true;
    settings = {
      env = {
        TERM = "xterm-256color";
      };
      font = {
        normal = {
          family = "MesloLGS Nerd Font";
          style = "Regular";
        };
        bold = {
          family = "MesloLGS Nerd Font";
          style = "Bold";
        };
        italic = {
          family = "MesloLGS Nerd Font";
          style = "Italic";
        };
        bold_italic = {
          family = "MesloLGS Nerd Font";
          style = "Bold Italic";
        };
        size = 9;
      };
      general.import = [
        "${config.home.homeDirectory}/.config/alacritty/gruvbox-dark.toml"
      ];
    };
  };
  home.file = {
    ".config/alacritty/gruvbox-dark.toml".source = ./files/alacritty/gruvbox-dark.toml;
  };
}
