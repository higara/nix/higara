{...}: {
  home.file = {
    ".config/zsh/env_includes/homedirs.zsh" = {
      executable = true;
      text = ''
        directories=(
            downloads/incoming
            pictures/screenshots
        )

        for dir ($directories) {
            mkdir --parents "$HOME/$dir"
        }
      '';
    };
  };
}
