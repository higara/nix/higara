{...}: {
  programs.git = {
    extraConfig = {
      includeIf = {
        "gitdir:~/code/pulumi/" = {
          path = "~/code/pulumi/.gitconfig";
        };
      };
    };
  };

  # Workaround for cloning go modules from private repos 😭
  # From: https://stackoverflow.com/questions/27500861/whats-the-proper-way-to-go-get-a-private-repository
  # > the question tries to replace "github.com" with "ssh://git@github.com/",
  # > this could work, but using the ssh:// format does not automatically find
  # > the .git file that go get needs. However, using simply git@github.com to
  # > replace works, since it automatically returns the .git file that go get
  # > expects
  home.file = {
    "code/pulumi/.gitconfig".text = ''
      [url "git@github.com:"]
          insteadOf = "https://github.com"
    '';
  };
}
