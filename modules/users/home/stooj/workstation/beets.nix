{
  pkgs,
  config,
  ...
}: {
  programs.beets = {
    enable = true;
    settings = {
      plugins = "fetchart lyrics missing embedart convert scrub replaygain lastgenre chroma web";
      directory = "${config.home.homeDirectory}/downloads/incoming/music/processed";
      art_filename = "albumart";
      threaded = true;
      original_date = false;
      max_filename_length = 60;
      per_disk_numbering = false;
      convert = {
        auto = false;
        ffmpeg = "${pkgs.ffmpeg}/bin/ffmpeg";
        opts = "-ab 320k -ac 2 -ar 48000";
        max_bitrate = 320;
        threads = 1;
      };
      paths = {
        default = "$albumartist/$album%aunique{}/$track - $title";
        singleton = "Non-Album/$artist - $title";
        comp = "Compilations/$album%aunique{}/$track - $title";
        albumtype_soundtrack = "Soundtracks/$album/$track $title";
      };

      import = {
        write = true;
        copy = false;
        move = true;
        resume = "ask";
        incremental = true;
        quiet_fallback = "skip";
        timid = false;
        # TODO Make this directory
        log = "~/.local/state/beets/beet.log";
      };

      lastgenre = {
        auto = true;
        source = "album";
      };

      embedart = {
        auto = true;
      };

      fetchart = {
        auto = true;
      };

      replaygain = {
        auto = false;
      };

      scrub = {
        auto = true;
      };

      web = {
        host = "0.0.0.0";
        port = 8337;
      };

      match = {
        preferred = {
          countries = [
            "GB|UK"
            "US"
            "AU"
          ];
          media = ["CD"];
        };
        ignored_media = [
          "Data CD"
          "DVD"
          "DVD-Video"
          "Blu-ray"
          "HD-DVD"
          "VCD"
          "SVCD"
          "UMD"
          "VHS"
          "12\" Vinyl"
          "2xVinyl"
        ];
      };
    };
  };
}
