{ pkgs, inputs, ... }:
{
  wayland.windowManager.hyprland = {
    enable = true;
    xwayland.enable = true;
    plugins = [
      inputs.hy3.packages.${pkgs.system}.hy3
      # hy3.packages.${pkgs.system}.hy3

    ];
    settings = {
      # TODO, this should be per-machine
      # See https://wiki.hyprland.org/Configuring/Monitors/
      monitor = "eDP-1,preferred,auto,1";

      # Execute your favorite apps at launch
      exec-once = with pkgs; [
        "${dunst}/bin/dunst"
        "${waybar}/bin/waybar"
        "${libsForQt5.polkit-kde-agent}/libexec/polkit-kde-authentication-agent-1"
      ];

      # Some default env vars.
      env = "XCURSOR_SIZE,24";
      general = {
        # See https://wiki.hyprland.org/Configuring/Variables/ for more
        gaps_in = 3;
        gaps_out = 5;
        border_size = 2;
        "col.active_border" = "rgba(33ccffee) rgba(00ff99ee) 45deg";
        "col.inactive_border" = "rgba(595959aa)";
        layout = "hy3";
      };

      decoration = {
        # See https://wiki.hyprland.org/Configuring/Variables/ for more
        rounding = 5;
        blur = {
          enabled = true;
          size = 7;
          passes = 4;
        };

        drop_shadow = "yes";
        shadow_range = 4;
        shadow_render_power = 3;
        "col.shadow" = "rgba(1a1a1aee)";
      };
      animations = {
        enabled = "yes";

        bezier = "myBezier, 0.05, 0.9, 0.1, 1.05";

        # Some default animations, see https://wiki.hyprland.org/Configuring/Animations/ for more
        animation = [

          "windows, 1, 7, myBezier"
          "windowsOut, 1, 7, default, popin 80%"
          "border, 1, 10, default"
          "borderangle, 1, 8, default"
          "fade, 1, 7, default"
          "workspaces, 1, 6, default"
        ];
      };
      input = {
        # For all categories, see https://wiki.hyprland.org/Configuring/Variables/
        kb_layout = "us";
        follow_mouse = 2;
        touchpad = {
          natural_scroll = "no";
        };
        sensitivity = 0;
      };
      dwindle = {
        # See https://wiki.hyprland.org/Configuring/Dwindle-Layout/ for more
        # master switch for pseudotiling. Enabling is bound to mainMod + P in the keybinds section below
        pseudotile = "yes";
        # you probably want this
        preserve_split = "yes";
      };
      master = {
        # See https://wiki.hyprland.org/Configuring/Master-Layout/ for more
        new_is_master = true;
      };
      gestures = {
        # See https://wiki.hyprland.org/Configuring/Variables/ for more
        workspace_swipe = "off";
      };

      # Example per-device config
      # See https://wiki.hyprland.org/Configuring/Keywords/#executing for more
      "device:epic-mouse-v1" =  {
          sensitivity = -0.5;
      };
      
      layerrule = "blur, wofi";
      "$mainMod" = "SUPER";
      bind = [
        # Start a terminal
        "$mainMod, Return, exec, kitty"

        # Kill the focused window
        "$mainMod SHIFT, Q, hy3:killactive"

        # Control wofi
        "$mainMod, d, exec, wofi --show run"
        "$mainMod SHIFT, D, exec, wofi --show drun"
        # $mainMod, C, exec, wofi --show ssh   - I would love to show ssh but Idk what it is
        "$mainMod, p, exec, wofi-pass --type"
        "$mainMod SHIFT, S, exec, wofi-emoji --config ~/.config/rofi/emoji.rasi"
        
        # Manage notifications
        "CONTROL, SPACE, exec, dunstctl close"
        "CONTROL SHIFT, SPACE, exec, dunstctl close-all"
        "CONTROL, GRAVE, exec, dunstctl history-pop"
        
        # Change focus
        "$mainMod, h, hy3:movefocus, l"
        "$mainMod, j, hy3:movefocus, d"
        "$mainMod, k, hy3:movefocus, u"
        "$mainMod, l, hy3:movefocus, r"
        
        # Move windows
        "$mainMod SHIFT, H, hy3:movewindow, l"
        "$mainMod SHIFT, J, hy3:movewindow, d"
        "$mainMod SHIFT, K, hy3:movewindow, u"
        "$mainMod SHIFT, L, hy3:movewindow, r"
        
        "$mainMod, o, hy3:makegroup, h"
        "$mainMod, v, hy3:makegroup, v"
        
        
        # Enter fullscreen mode for the focused window
        "$mainMod, f, fullscreen"
        
        # TODO How to do tabbed windows?
        
        # Toggle floating window
        "$mainMod SHIFT, SPACE, togglefloating "
        
        # TODO Do you need floating toggle?
        
        # {,un}pin a floating window
        "$mainMod SHIFT, P, pin"
        
        # $mainMod, Q, exec, kitty
        # $mainMod, C, killactive, 
        # $mainMod, M, exit, 
        # $mainMod, E, exec, dolphin
        # $mainMod, V, togglefloating, 
        # $mainMod, R, exec, wofi --show drun
        # $mainMod, P, pseudo, # dwindle
        # $mainMod, J, togglesplit, # dwindle
        
        # Scroll through existing workspaces with mainMod + scroll
        "$mainMod, mouse_down, workspace, e+1"
        "$mainMod, mouse_up, workspace, e-1"
        # Exit hyprland
        "$mainMod SHIFT, E, exit"
        # bind = $mainMod, M, exit, 
        # Power management
        "bind = $mainMod CONTROL, l, exec, swaylock --color 000000"
      ];

      bindm = [
        # Move/resize windows with mainMod + LMB/RMB and dragging
        "$mainMod, mouse:272, movewindow"
        "$mainMod, mouse:273, resizewindow"
      ];
      bindl = [
        ",switch:on:Lid Switch,exec,hyprctl keyword monitor \"eDP-1,preferred,auto,1\""
        ",switch:off:Lid Switch,exec,hyprctl keyword monitor \"eDP-1, disable\""
      ];
    };
    extraConfig = ''
      plugin {
        hy3 {
          tabs {
            height = 5
            padding = 8
            render_text = false
          }

          autotile {
            enable = true
            trigger_width = 800
            trigger_height = 500
          }
        }
      }

      # workspaces
      ${builtins.concatStringsSep "\n" (builtins.genList (
        x: let
          ws = let
            c = (x + 1) / 10;
          in
            builtins.toString (x + 1 - (c * 10));
        in ''
          bind = $mod, ${ws}, workspace, ${toString (x + 1)}
          bind = $mod SHIFT, ${ws}, movetoworkspace, ${toString (x + 1)}
        ''
      )
      10)}
      # Resize windows
      # See https://wiki.hyprland.org/Configuring/Binds/#submaps
      bind = $mainMod, r, submap, resize

      submap = resize

      binde =, l, resizeactive, 10 0
      binde =, h, resizeactive, -10 0
      binde =, k, resizeactive, 0 -10
      binde =, j, resizeactive, 0 10

      # Back to normal mode with escape, enter, or the resize mapping again
      bind = ,ESCAPE, submap, reset
      bind = ,RETURN, submap, reset
      bind = $mainMod, r, submap, reset
      submap = reset
    '';
  };
}
