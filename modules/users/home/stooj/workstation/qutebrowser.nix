{...}: {
  programs.qutebrowser = {
    enable = true;
    # TODO bookmarks somehow? They probably can't be read-only.
    # TODO keys.conf file. Can be read-only probably.
    enableDefaultBindings = true;
    keyBindings = {
      normal = {
        ",i" = "hint images yank";
        "yr" = "yank inline `{title} <{url}`_";
        "yv" = "yank inline [[{url}|{title}]]";
        ",e" = "fake-key <Escape>";
        ",v" = "spawn mpv {url}";
        ",V" = "hint links spawn mpv {hint-url}";
        # "zul" = "spawn --userscript qute-pass --username-only";
        # "zpl" = "spawn --userscript qute-pass --password-only";
        # "zol" = "spawn --userscript qute-pass --otp-only";
      };
    };
    searchEngines = {
      DEFAULT = "https://duckduckgo.com/?q={}";
      archforums = "https://bbs.archlinux.org/search.php?action=search&keywords={}";
      archwiki = "https://wiki.archlinux.org/index.php?search={}";
      aur = "https://aur.archlinux.org/packages/?O=0&SeB=nd&K={}&outdated=&SB=n&SO=a&PP=50&do_Search=Go";
      bashfaq = "http://mywiki.wooledge.org/BashGuide?action=fullsearch&titlesearch=0&value={}&context=180";
      define = "https://en.wiktionary.org/wiki/index.php?search={}";
      djangopkg = "https://djangopackages.org/search/?q={}";
      dockerhub = "https://hub.docker.com/search/?isAutomated=0&isOfficial=0&page=1&pullCount=0&q={}&starCount=0";
      dpkg = "https://packages.debian.org/search?keywords={}&searchon=names&suite=stretch&section=all";
      duck = "https://duckduckgo.com/?q={}";
      gmap = "http://maps.google.com/?q={}";
      homemanager = "https://home-manager-options.extranix.com/?query={}&release=master";
      map = "https://nominatim.openstreetmap.org/search.php?q={}&polygon_geojson=1&viewbox=";
      man = "https://man.archlinux.org/search?q={}&go=Go";
      nixoption = "https://search.nixos.org/options?channel=unstable&from=0&size=50&sort=relevance&type=packages&query={}";
      nixpkg = "https://search.nixos.org/packages?channel=unstable&from=0&size=50&sort=relevance&type=packages&query={}";
      nixwiki = "https://nixos.wiki/index.php?search={}&go=Go";
      pacman = "https://www.archlinux.org/packages/?q={}";
      pip = "https://pypi.org/search/?q={}";
      python = "https://docs.python.org/3/search.html?q={}&check_keywords=yes&area=default";
      snap = "https://snapcraft.io/search?category=&q={}";
      ubpkg = "https://packages.ubuntu.com/search?keywords={}&searchon=names&suite=bionic&section=all";
      vimext = "https://vimawesome.com/?q={}";
      wiki = "https://en.wikipedia.org/w/index.php?search={}";
      youtube = "https://www.youtube.com/results?search_query={}";
    };
    settings = let
      base00 = "#32302f";
      base01 = "#3c3836";
      base02 = "#504945";
      base03 = "#665c54";
      base04 = "#bdae93";
      base05 = "#d5c4a1";
      base06 = "#ebdbb2";
      base07 = "#fbf1c7";
      base08 = "#fb4934";
      base09 = "#fe8019";
      base0A = "#fabd2f";
      base0B = "#b8bb26";
      base0C = "#8ec07c";
      base0D = "#83a598";
      base0E = "#d3869b";
      base0F = "#d65d0e";
    in {
      colors = {
        completion = {
          category = {
            bg = "${base00}";
            border = {
              bottom = "${base00}";
              top = "${base00}";
            };
            fg = "${base0A}";
          };
          even = {
            bg = "${base00}";
          };
          fg = "${base05}";
          item = {
            selected = {
              bg = "${base02}";
              border = {
                bottom = "${base02}";
                top = "${base02}";
              };
              fg = "${base05}";
              match.fg = "${base0B}";
            };
          };
          match = {
            fg = "${base0B}";
          };
          odd = {
            bg = "${base01}";
          };
          scrollbar = {
            bg = "${base00}";
            fg = "${base05}";
          };
        };
        contextmenu = {
          disabled = {
            bg = "${base01}";
            fg = "${base04}";
          };
          menu = {
            bg = "${base00}";
            fg = base05;
          };
          selected = {
            bg = "${base02}";
            fg = "${base05}";
          };
        };
        downloads = {
          bar = {
            bg = "${base00}";
          };
          error = {
            # bg = 'red';
            fg = "${base08}";
          };
          start = {
            bg = "${base0D}";
            fg = "${base00}";
          };
          stop = {
            bg = "${base0C}";
            fg = "${base00}";
          };
          # system = {
          # bg = 'rgb';
          # fg = 'rgb';
          # };
        };
        hints = {
          bg = "${base0A}";
          fg = "${base00}";
          match = {
            fg = "${base05}";
          };
        };
        keyhint = {
          bg = "${base00}";
          fg = "${base05}";
          suffix = {
            fg = "${base05}";
          };
        };
        messages = {
          error = {
            bg = "${base08}";
            border = "${base08}";
            fg = "${base00}";
          };
          info = {
            bg = "${base00}";
            border = "${base00}";
            fg = "${base05}";
          };
          warning = {
            bg = "${base0E}";
            border = "${base0E}";
            fg = "${base00}";
          };
        };
        prompts = {
          bg = "${base00}";
          border = "${base00}";
          fg = "${base05}";
          selected = {
            bg = "${base02}";
          };
        };
        statusbar = {
          caret = {
            bg = "${base0E}";
            fg = "${base00}";
            selection = {
              bg = "${base0D}";
              fg = "${base00}";
            };
          };
          command = {
            bg = "${base00}";
            fg = "${base05}";
            private = {
              bg = "${base00}";
              fg = "${base05}";
            };
          };
          insert = {
            bg = "${base0D}";
            fg = "${base00}";
          };
          normal = {
            bg = "${base00}";
            fg = "${base0B}";
          };
          passthrough = {
            bg = "${base0C}";
            fg = "${base00}";
          };
          private = {
            bg = "${base01}";
            fg = "${base00}";
          };
          progress = {
            bg = "${base0D}";
          };
          url = {
            error = {
              fg = "${base08}";
            };
            fg = "${base05}";
            hover = {
              fg = "${base05}";
            };
            success = {
              http = {
                fg = "${base0C}";
              };
              https = {
                fg = "${base0B}";
              };
            };
            warn = {
              fg = "${base0E}";
            };
          };
        };
        tabs = {
          bar = {
            bg = "${base00}";
          };
          even = {
            bg = "${base00}";
            fg = "${base05}";
          };
          indicator = {
            error = "${base08}";
            start = "${base0D}";
            stop = "${base0C}";
            # system = 'rgb';
          };
          odd = {
            bg = "${base01}";
            fg = "${base05}";
          };
          pinned = {
            even = {
              bg = "${base0C}";
              fg = "${base07}";
            };
            odd = {
              bg = "${base0B}";
              fg = "${base07}";
            };
            selected = {
              even = {
                bg = "${base02}";
                fg = "${base05}";
              };
              odd = {
                bg = "${base02}";
                fg = "${base05}";
              };
            };
          };
          selected = {
            even = {
              bg = "${base02}";
              fg = "${base05}";
            };
            odd = {
              bg = "${base02}";
              fg = "${base05}";
            };
          };
        };
        # webpage = {
        # bg = "${base00}";
        # darkmode = {
        # algorithm = 'lightness-cielab';
        # contrast = 0.0;
        # enabled = false;
        # grayscale = {
        # all = false;
        # images = 0.0;
        # };
        # policy = {
        # images = 'never';
        # page = 'smart';
        # };
        # threshold = {
        # background = 0;
        # text = 256;
        # };
        # };
        # prefers_color_scheme_dark = false;
        # };
      };
      downloads = {
        location = {
          # TODO create directory if needed after nextcloud mounts are done
          directory = "/home/stooj/downloads/incoming";
        };
      };
      editor = {
        command = [
          "nvim-qt"
          "--nofork"
          "{}"
        ];
        encoding = "utf-8";
      };
      fonts = {
        completion = {
          category = "10pt 'fira code medium'";
          entry = "10pt 'fira code medium'";
        };
        debug_console = "10pt 'fira code medium'";
        downloads = "10pt 'fira code medium'";
        hints = "10pt 'fira code medium'";
        keyhint = "10pt 'fira code medium'";
        messages = {
          error = "10pt 'fira code medium'";
          info = "10pt 'fira code medium'";
          warning = "10pt 'fira code medium'";
        };
        prompts = "10pt 'fira code medium'";
        statusbar = "10pt 'fira code medium'";
        tabs = {
          selected = "10pt 'fira code medium'";
        };
      };
      tabs = {
        position = "bottom";
      };
    };
  };
  home.file = {
    ".local/bin/default-qutebrowser".source = ./files/qutebrowser/default-qutebrowser;
    ".local/bin/qutebrowser".source = ./files/qutebrowser/wrapper-script;
  };
}
