{ ... }:
{
  home.file.".config/zsh/rc_includes/virtualbox.zsh" = {
    enable = true;
    text = ''
      #!/usr/bin/env bash

      ######################################################################
      #  File managed by Home Manager
      #  Your changes will be overwritten.
      ######################################################################

      if [[ -e "$HOME/.config/VirtualBox/VirtualBox.xml" ]]; then
        if grep --silent "SystemProperties" ~/.config/VirtualBox/VirtualBox.xml | grep defaultMachineFolder | grep .local/share/virtualbox_vms; then
          echo
        else
          VBoxManage setproperty machinefolder "$HOME"/.local/share/virtualbox_vms
        fi
      else
        VBoxManage setproperty machinefolder "$HOME"/.local/share/virtualbox_vms
      fi
    '';
  };
}
