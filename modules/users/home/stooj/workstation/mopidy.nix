{
  pkgs,
  config,
  ...
}: {
  services.mopidy = {
    enable = true;
    extensionPackages = with pkgs; [mopidy-jellyfin mopidy-mpd];
    settings = {
      mpd = {
        hostname = "::";
      };
    };
    extraConfigFiles = [
      "${config.home.homeDirectory}/.config/mopidy/jellyfin.conf"
    ];
  };
}
