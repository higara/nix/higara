{ pkgs, ... }:
{
  programs.firefox = {
    package = pkgs.firefox.override {
      nativeMessagingHosts = [
        pkgs.bukubrow
      ];
    };
    enable = true;
    profiles.stooj = {
      name = "default";
      id = 0;
      isDefault = true;
    };
    profiles.pindy = {
      name = "pindy";
      id = 1;
    };
    profiles.lee = {
      name = "lee";
      id = 2;
    };
    profiles.stock = {
      name = "stock";
      id = 3;
    };
  };
  home.file = {
    ".local/bin/default-firefox".source = ./files/firefox/default-firefox;
    ".local/bin/pindy-firefox".source = ./files/firefox/pindy-firefox;
    ".local/bin/lee-firefox".source = ./files/firefox/lee-firefox;
    ".local/bin/stock-firefox".source = ./files/firefox/stock-firefox;
  };
}
