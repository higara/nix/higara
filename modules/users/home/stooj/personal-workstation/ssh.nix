{ ... }:
{
  programs.ssh = {
    matchBlocks = {
      "kitchen" = {
        host = "kitchen";
        hostname = "kitchen";
        user = "root";
      };
      "medina" = {
        host = "wg.ginstoo.net medina";
        hostname = "wg.ginstoo.net";
        serverAliveInterval = 240;
        user = "root";
      };
      "steamdeck" = {
        host = "steamdeck.ginstoo.net steamdeck";
        user = "deck";
        hostname = "steamdeck.ginstoo.net";
      };
      "homeassistant" = {
        host = "homeassistant homeassistant.ginstoo.net yellow yellow.ginstoo.net";
        user = "hassio";
        hostname = "homeassistant.ginstoo.net";
      };
      "storagebox" = {
        user = "u398328";
        host = "u398328.your-storagebox.de storagebox";
        hostname = "u398328.your-storagebox.de";
        port = 23;
      };
    };
  };
}
