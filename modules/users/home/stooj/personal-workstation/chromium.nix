{ ... }:
{
  home.file = {
    ".local/bin/pindy-chromium".source = ./files/chromium/pindy-chromium;
    ".local/bin/stock-chromium".source = ./files/chromium/stock-chromium;
  };
}
