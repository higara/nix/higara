{...}: {
  programs.nixvim = {
    plugins = {
      neorg = {
        enable = true;
        modules = {
          "core.dirman" = {
            config = {
              workspaces = {
                about = "~/code/docs/neorg/about";
                ginstoo = "~/code/docs/neorg/ginstoo";
              };
              default_workspace = "ginstoo";
            };
          };
        };
      };
    };
    extraConfigLua = ''
      vim.g.vimwiki_list = {
            {
                name = 'Ginstoo Wiki',
                path = '~/code/docs/ginstoo',
                path_html = '~/.cache/ginstoo_wiki'
            },
            {
                name = 'B-Eight',
                path = '~/documents/beight'
            },
            {
                name = 'Goodteith Wiki',
                path = '~/code/docs/goodteith'
            },
            {
                name = 'Nextcloud Notes',
                path = '~/nextcloud/ginstoo/notes',
                path_html = '~/.cache/nextcloud_notes',
                ext = '.md',
                syntax = 'markdown'
            },
            {
                name = 'Ginstoo recipes',
                path = '~/code/docs/recipes'
            },
            {
                name = 'Higara',
                path = '~/code/docs/higara',
                ext = '.md',
                syntax = 'markdown'
            },
        }
    '';
  };
}
