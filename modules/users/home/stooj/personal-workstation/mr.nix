{config, ...}: {
  programs.mr = {
    enable = true;
    settings = {
      ".config" = {
        order = 1;
        update = ":";
        checkout = "mkdir --parents ${config.home.homeDirectory}/.config";
        status = ":";
      };
      ".local/share" = {
        order = 1;
        update = ":";
        checkout = "mkdir --parents ${config.home.homeDirectory}/.local/share";
        status = ":";
      };
      ".local/share/buku" = {
        order = 1;
        update = ":";
        checkout = "mkdir --parents ${config.home.homeDirectory}/.local/share/buku";
        status = ":";
      };
      "nextcloud/ginstoo" = {
        order = 1;
        update = ":";
        checkout = "mkdir --parents ${config.home.homeDirectory}/nextcloud/ginstoo";
        status = ":";
      };
      "nextcloud/ginstoo/syncs/bookmarks.db" = {
        order = 5;
        update = ":";
        checkout = "[[ -d ${config.home.homeDirectory}/nextcloud/ginstoo/syncs/bookmarks.db ]] && ln --symbolic ${config.home.homeDirectory}/nextcloud/ginstoo/syncs/bookmarks.db ${config.home.homeDirectory}/.local/share/buku/bookmarks.db";
        status = ":";
      };
      "pictures/wallpapers/eve-online" = {
        order = 5;
        update = ":";
        checkout = "[[ -d ${config.home.homeDirectory}/pictures/wallpapers/eve-online/ ]] && ln --symbolic ${config.home.homeDirectory}/pictures/wallpapers/eve-online/ ${config.home.homeDirectory}/.config/desktop-backgrounds";
        status = ":";
      };
      "code/docs" = {
        order = 5;
        update = ":";
        checkout = "mkdir --parents ${config.home.homeDirectory}/code/docs";
        status = ":";
      };
      "code/misc" = {
        order = 5;
        update = ":";
        checkout = "mkdir --parents ${config.home.homeDirectory}/code/misc";
        status = ":";
      };
      "code/nix" = {
        order = 5;
        update = ":";
        checkout = "mkdir --parents ${config.home.homeDirectory}/code/nix";
        status = ":";
      };
      "code/nix/higara" = {
        checkout = "git clone 'git@gitlab.com:higara/nix/higara.git' '${config.home.homeDirectory}/code/nix/higara'";
      };
      "code/nix/nixpkgs" = {
        checkout = "git clone 'git@github.com:stooj/nixpkgs.git' '${config.home.homeDirectory}/code/nix/nixpkgs'";
      };
      "code/nix/nur-packages" = {
        checkout = "git clone 'git@github.com:stooj/nur-packages.git' '${config.home.homeDirectory}/code/nix/nur-packages'";
      };
      "code/pulumi" = {
        order = 5;
        update = ":";
        checkout = "mkdir --parents ${config.home.homeDirectory}/code/pulumi";
        status = ":";
      };
      "code/pulumi/higara" = {
        checkout = "git clone 'git@gitlab.com:higara/pulumi/higara.git' '${config.home.homeDirectory}/code/pulumi/higara'";
      };
      "code/pulumi/homework" = {
        checkout = "git clone 'git@github.com:stooj/pulumi-homework' '${config.home.homeDirectory}/code/pulumi/homework'";
      };
      "code/research/nix" = {
        order = 5;
        update = ":";
        checkout = "mkdir --parents ${config.home.homeDirectory}/code/research/nix";
        status = ":";
      };
      "code/research/nix-anywhere" = {
        order = 5;
        update = ":";
        checkout = "mkdir --parents ${config.home.homeDirectory}/code/research/nix-anywhere";
        status = ":";
      };
      "code/research/nvim" = {
        order = 5;
        update = ":";
        checkout = "mkdir --parents ${config.home.homeDirectory}/code/research/nvim";
        status = ":";
      };
      "code/research/other-dots" = {
        order = 5;
        update = ":";
        checkout = "mkdir --parents ${config.home.homeDirectory}/code/research/other-dots";
        status = ":";
      };
      "code/salt/external-formulas" = {
        order = 5;
        update = ":";
        checkout = "mkdir --parents ${config.home.homeDirectory}/code/salt/external-formulas";
        status = ":";
      };
      "code/salt/old-formulas" = {
        order = 5;
        update = ":";
        checkout = "mkdir --parents ${config.home.homeDirectory}/code/salt/old-formulas";
        status = ":";
      };
      "code/salt/personal-formulas" = {
        order = 5;
        update = ":";
        checkout = "mkdir --parents ${config.home.homeDirectory}/code/salt/personal-formulas";
        status = ":";
      };
      "code/terraform" = {
        order = 5;
        update = ":";
        checkout = "mkdir --parents ${config.home.homeDirectory}/code/terraform";
        status = ":";
      };
      ".local/share/password-store" = {
        checkout = "git clone 'git@code.ginstoo.net:stooj/pass-store.git' '${config.home.homeDirectory}/.local/share/password-store'";
      };
      ".local/share/pindy-password-store" = {
        checkout = "git clone 'git@code.ginstoo.net:pindy/pass-store.git' '${config.home.homeDirectory}/.local/share/pindy-password-store'";
      };
      "code/docs/about" = {
        checkout = "git clone 'git@code.ginstoo.net:stooj/about.git' '${config.home.homeDirectory}/code/docs/about'";
      };
      "code/docs/calendar" = {
        checkout = "git clone 'git@code.ginstoo.net:stooj/calendar.git' '${config.home.homeDirectory}/code/docs/calendar'";
      };
      "code/docs/cv" = {
        checkout = "git clone 'git@code.ginstoo.net:stooj/cv.git' '${config.home.homeDirectory}/code/docs/cv'";
      };
      "code/docs/fishers-glen" = {
        checkout = "git clone 'git@code.ginstoo.net:team-ginstoo/fishers-glen.git' '${config.home.homeDirectory}/code/docs/fishers-glen'";
      };
      "code/docs/ginstoo" = {
        checkout = "git clone 'git@code.ginstoo.net:team-ginstoo/ginstoo.git' '${config.home.homeDirectory}/code/docs/ginstoo'";
      };
      "code/docs/ginstoo.wiki" = {
        checkout = "git clone 'git@code.ginstoo.net:team-ginstoo/gin-stoo.wiki.git' '${config.home.homeDirectory}/code/docs/ginstoo.wiki'";
      };
      "code/docs/goodteith" = {
        checkout = "git clone 'git@code.ginstoo.net:goodteith/goodteith.git' '${config.home.homeDirectory}/code/docs/goodteith'";
      };
      "code/docs/goodteith.wiki" = {
        checkout = "git clone 'git@code.ginstoo.net:goodteith/goodteith-wiki.wiki.git' '${config.home.homeDirectory}/code/docs/goodteith.wiki'";
      };
      "code/docs/higara" = {
        checkout = "git clone 'git@code.ginstoo.net:team-ginstoo/higara-log.git' '${config.home.homeDirectory}/code/docs/higara'";
      };
      "code/docs/itw.wiki" = {
        checkout = "git clone 'git@code.ginstoo.net:invent-the-world/itw-wiki.wiki.git' '${config.home.homeDirectory}/code/docs/itw.wiki'";
      };
      "code/docs/recipes" = {
        checkout = "git clone 'git@code.ginstoo.net:team-ginstoo/recipes.git' '${config.home.homeDirectory}/code/docs/recipes'";
      };
      "code/docs/the_book_of_ginstoo" = {
        checkout = "git clone 'git@code.ginstoo.net:team-ginstoo/the_book_of_ginstoo.git' '${config.home.homeDirectory}/code/docs/the_book_of_ginstoo'";
      };
      "code/misc/accounts" = {
        checkout = "git clone 'git@code.ginstoo.net:team-ginstoo/accounts.git' '${config.home.homeDirectory}/code/misc/accounts'";
      };
      "code/misc/input-maps" = {
        checkout = "git clone 'git@gitlab.com:stooj/input-maps.git' '${config.home.homeDirectory}/code/misc/input-maps'";
      };
      "code/salt/salt-templater" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/salt-templater.git' '${config.home.homeDirectory}/code/salt/salt-templater'";
      };
      "code/salt/salt-testing-env" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/salt-testing-env.git' '${config.home.homeDirectory}/code/salt/salt-testing-env'";
      };
      "code/salt/old-formulas/beets-formula" = {
        checkout = "git clone 'git@gitlab.com:cremona-salt/beets-formula.git' '${config.home.homeDirectory}/code/salt/old-formulas/beets-formula'";
      };
      "code/salt/old-formulas/cremona-salt-pillar" = {
        checkout = "git clone 'git@code.ginstoo.net:configs-stoo/cremona-salt-pillar.git' '${config.home.homeDirectory}/code/salt/old-formulas/cremona-salt-pillar'";
      };
      "code/salt/old-formulas/cremona-salt-states" = {
        checkout = "git clone 'git@code.ginstoo.net:team-ginstoo/cremona-salt/cremona-salt-states.git' '${config.home.homeDirectory}/code/salt/old-formulas/cremona-salt-states'";
      };
      "code/salt/old-formulas/emby-formula" = {
        checkout = "git clone 'git@gitlab.com:cremona-salt/emby-formula.git' '${config.home.homeDirectory}/code/salt/old-formulas/emby-formula'";
      };
      "code/salt/old-formulas/gnome-formula" = {
        checkout = "git clone 'git@gitlab.com:cremona-salt/gnome-formula.git' '${config.home.homeDirectory}/code/salt/old-formulas/gnome-formula'";
      };
      "code/salt/old-formulas/grafana-docker-formula" = {
        checkout = "git clone 'git@gitlab.com:cremona-salt/grafana-docker-formula.git' '${config.home.homeDirectory}/code/salt/old-formulas/grafana-docker-formula'";
      };
      "code/salt/old-formulas/grocy-formula" = {
        checkout = "git clone 'git@gitlab.com:cremona-salt/grocy-formula.git' '${config.home.homeDirectory}/code/salt/old-formulas/grocy-formula'";
      };
      "code/salt/old-formulas/home-salt-pillar" = {
        checkout = "git clone 'git@code.ginstoo.net:configs-stoo/home-salt-pillar.git' '${config.home.homeDirectory}/code/salt/old-formulas/home-salt-pillar'";
      };
      "code/salt/old-formulas/home-salt-states" = {
        checkout = "git clone 'git@code.ginstoo.net:team-ginstoo/home-salt-states.git' '${config.home.homeDirectory}/code/salt/old-formulas/home-salt-states'";
      };
      "code/salt/old-formulas/jellyfin-formula" = {
        checkout = "git clone 'git@gitlab.com:cremona-salt/jellyfin-formula.git' '${config.home.homeDirectory}/code/salt/old-formulas/jellyfin-formula'";
      };
      "code/salt/old-formulas/matrix-formula" = {
        checkout = "git clone 'git@gitlab.com:cremona-salt/matrix-formula.git' '${config.home.homeDirectory}/code/salt/old-formulas/matrix-formula'";
      };
      "code/salt/old-formulas/modoboa-formula" = {
        checkout = "git clone 'git@gitlab.com:cremona-salt/modoboa-formula.git' '${config.home.homeDirectory}/code/salt/old-formulas/modoboa-formula'";
      };
      "code/salt/old-formulas/mounts-formula" = {
        checkout = "git clone 'git@gitlab.com:cremona-salt/mounts-formula.git' '${config.home.homeDirectory}/code/salt/old-formulas/mounts-formula'";
      };
      "code/salt/old-formulas/nextcloud-formula" = {
        checkout = "git clone 'git@gitlab.com:cremona-salt/nextcloud-formula.git' '${config.home.homeDirectory}/code/salt/old-formulas/nextcloud-formula'";
      };
      "code/salt/old-formulas/pindy-dotfiles-workstation-common-formula" = {
        checkout = "git clone 'git@gitlab.com:cremona-salt/pindy-dotfiles-workstation-common-formula.git' '${config.home.homeDirectory}/code/salt/old-formulas/pindy-dotfiles-workstation-common-formula'";
      };
      "code/salt/old-formulas/plasma-formula" = {
        checkout = "git clone 'git@gitlab.com:cremona-salt/plasma-formula.git' '${config.home.homeDirectory}/code/salt/old-formulas/plasma-formula'";
      };
      "code/salt/old-formulas/stooj-dotfiles-common-formula" = {
        checkout = "git clone 'git@gitlab.com:cremona-salt/stooj-dotfiles-common-formula.git' '${config.home.homeDirectory}/code/salt/old-formulas/stooj-dotfiles-common-formula'";
      };
      "code/salt/old-formulas/stooj-dotfiles-common" = {
        checkout = "git clone 'git@code.ginstoo.net:team-ginstoo/stooj-dotfiles-formula.git' '${config.home.homeDirectory}/code/salt/old-formulas/stooj-dotfiles-common'";
      };
      "code/salt/old-formulas/stooj-dotfiles-personal-workstation-formula" = {
        checkout = "git clone 'git@gitlab.com:cremona-salt/stooj-dotfiles-personal-workstation-formula.git' '${config.home.homeDirectory}/code/salt/old-formulas/stooj-dotfiles-personal-workstation-formula'";
      };
      "code/salt/old-formulas/stooj-dotfiles-server-formula" = {
        checkout = "git clone 'git@gitlab.com:cremona-salt/stooj-dotfiles-server-formula.git' '${config.home.homeDirectory}/code/salt/old-formulas/stooj-dotfiles-server-formula'";
      };
      "code/salt/old-formulas/stooj-dotfiles-workstation-common-formula" = {
        checkout = "git clone 'git@gitlab.com:cremona-salt/stooj-dotfiles-workstation-common-formula.git' '${config.home.homeDirectory}/code/salt/old-formulas/stooj-dotfiles-workstation-common-formula'";
      };
      "code/salt/old-formulas/stooj-dotfiles-work-workstation-formula" = {
        checkout = "git clone 'git@gitlab.com:cremona-salt/stooj-dotfiles-work-workstation-formula.git' '${config.home.homeDirectory}/code/salt/old-formulas/stooj-dotfiles-work-workstation-formula'";
      };
      "code/salt/old-formulas/traefik-formula" = {
        checkout = "git clone 'git@gitlab.com:cremona-salt/traefik-formula.git' '${config.home.homeDirectory}/code/salt/old-formulas/traefik-formula'";
      };
      "code/salt/personal-formulas/abcde-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/abcde-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/abcde-formula'";
      };
      "code/salt/personal-formulas/alacritty-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/alacritty-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/alacritty-formula'";
      };
      "code/salt/personal-formulas/asdf-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/asdf-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/asdf-formula'";
      };
      "code/salt/personal-formulas/base-packages-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/base-packages-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/base-packages-formula'";
      };
      "code/salt/personal-formulas/beets-docker-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/beets-docker-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/beets-docker-formula'";
      };
      "code/salt/personal-formulas/bonded-ethernet-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/bonded-ethernet-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/bonded-ethernet-formula'";
      };
      "code/salt/personal-formulas/calibre-docker-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/calibre-docker-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/calibre-docker-formula'";
      };
      "code/salt/personal-formulas/chromium-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/chromium-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/chromium-formula'";
      };
      "code/salt/personal-formulas/cig-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/cig-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/cig-formula'";
      };
      "code/salt/personal-formulas/direnv-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/direnv-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/direnv-formula'";
      };
      "code/salt/personal-formulas/dunst-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/dunst-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/dunst-formula'";
      };
      "code/salt/personal-formulas/firefox-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/firefox-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/firefox-formula'";
      };
      "code/salt/personal-formulas/fishers-salt-pillar" = {
        checkout = "git clone 'git@code.ginstoo.net:team-ginstoo/fishers-salt/fishers-salt-pillar.git' '${config.home.homeDirectory}/code/salt/personal-formulas/fishers-salt-pillar'";
      };
      "code/salt/personal-formulas/fishers-salt-states" = {
        checkout = "git clone 'git@code.ginstoo.net:team-ginstoo/fishers-salt/fishers-salt-states.git' '${config.home.homeDirectory}/code/salt/personal-formulas/fishers-salt-states'";
      };
      "code/salt/personal-formulas/gdm-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/gdm-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/gdm-formula'";
      };
      "code/salt/personal-formulas/geany-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/geany-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/geany-formula'";
      };
      "code/salt/personal-formulas/git-ng-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/git-ng-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/git-ng-formula'";
      };
      "code/salt/personal-formulas/homeassistant-docker-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/homeassistant-docker-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/homeassistant-docker-formula'";
      };
      "code/salt/personal-formulas/i3wm-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/i3wm-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/i3wm-formula'";
      };
      "code/salt/personal-formulas/jellyfin-docker-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/jellyfin-docker-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/jellyfin-docker-formula'";
      };
      "code/salt/personal-formulas/linkding-docker-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/linkding-docker-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/linkding-docker-formula'";
      };
      "code/salt/personal-formulas/mounts-ng-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/mounts-ng-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/mounts-ng-formula'";
      };
      "code/salt/personal-formulas/neovim-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/neovim-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/neovim-formula'";
      };
      "code/salt/personal-formulas/nerd-fonts-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/nerd-fonts-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/nerd-fonts-formula'";
      };
      "code/salt/personal-formulas/network-manager-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/network-manager-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/network-manager-formula'";
      };
      "code/salt/personal-formulas/nextcloud-client-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/nextcloud-client-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/nextcloud-client-formula'";
      };
      "code/salt/personal-formulas/osupdates-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/osupdates-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/osupdates-formula'";
      };
      "code/salt/personal-formulas/pass-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/pass-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/pass-formula'";
      };
      "code/salt/personal-formulas/paperless-ngx-docker-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/paperless-ngx-docker-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/paperless-ngx-docker-formula'";
      };
      "code/salt/personal-formulas/picom-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/picom-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/picom-formula'";
      };
      "code/salt/personal-formulas/pi-hole-unbound-docker-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/pi-hole-unbound-docker-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/pi-hole-unbound-docker-formula'";
      };
      "code/salt/personal-formulas/polybar-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/polybar-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/polybar-formula'";
      };
      "code/salt/personal-formulas/powerlevel10k-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/powerlevel10k-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/powerlevel10k-formula'";
      };
      "code/salt/personal-formulas/printer-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/printer-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/printer-formula'";
      };
      "code/salt/personal-formulas/qutebrowser-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/qutebrowser-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/qutebrowser-formula'";
      };
      "code/salt/personal-formulas/rofi-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/rofi-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/rofi-formula'";
      };
      "code/salt/personal-formulas/rofi-pass-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/rofi-pass-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/rofi-pass-formula'";
      };
      "code/salt/personal-formulas/saltplusplus-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/saltplusplus-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/saltplusplus-formula'";
      };
      "code/salt/personal-formulas/steam-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/steam-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/steam-formula'";
      };
      "code/salt/personal-formulas/timesyncd-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/timesyncd-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/timesyncd-formula'";
      };
      "code/salt/personal-formulas/tmux-ng-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/tmux-ng-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/tmux-ng-formula'";
      };
      "code/salt/personal-formulas/traefik-docker-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/traefik-docker-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/traefik-docker-formula'";
      };
      "code/salt/personal-formulas/user-repos-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/user-repos-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/user-repos-formula'";
      };
      "code/salt/personal-formulas/vim-ng-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/vim-ng-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/vim-ng-formula'";
      };
      "code/salt/personal-formulas/virtualbox-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/virtualbox-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/virtualbox-formula'";
      };
      "code/salt/personal-formulas/workstation-packages-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/workstation-packages-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/workstation-packages-formula'";
      };
      "code/salt/personal-formulas/xdg-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/xdg-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/xdg-formula'";
      };
      "code/salt/personal-formulas/yubikey-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/yubikey-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/yubikey-formula'";
      };
      "code/salt/personal-formulas/zfs-ng-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/zfs-ng-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/zfs-ng-formula'";
      };
      "code/salt/personal-formulas/zsh-formula" = {
        checkout = "git clone 'git@gitlab.com:fishers-salt/personal-formulas/zsh-formula.git' '${config.home.homeDirectory}/code/salt/personal-formulas/zsh-formula'";
      };
      "code/salt/external-formulas/docker-formula" = {
        checkout = ''
          git clone 'git@gitlab.com:fishers-salt/external-formulas/docker-formula.git' '${config.home.homeDirectory}/code/salt/external-formulas/docker-formula'
            cd salt/external-formulas/docker-formula
            git remote add upstream https://github.com/saltstack-formulas/docker-formula.git
        '';
      };
      "code/salt/external-formulas/libvirt-formula" = {
        checkout = ''
          git clone 'git@gitlab.com:fishers-salt/external-formulas/libvirt-formula.git' '${config.home.homeDirectory}/code/salt/external-formulas/libvirt-formula'
            cd salt/external-formulas/libvirt-formula
            git remote add upstream https://github.com/saltstack-formulas/libvirt-formula.git
        '';
      };
      "code/salt/external-formulas/nfs-formula" = {
        checkout = ''
          git clone 'git@gitlab.com:fishers-salt/external-formulas/nfs-formula.git' '${config.home.homeDirectory}/code/salt/external-formulas/nfs-formula'
            cd salt/external-formulas/nfs-formula
            git remote add upstream https://github.com/saltstack-formulas/nfs-formula.git
        '';
      };
      "code/salt/external-formulas/openssh-formula" = {
        checkout = ''
          git clone 'git@gitlab.com:fishers-salt/external-formulas/openssh-formula.git' '${config.home.homeDirectory}/code/salt/external-formulas/openssh-formula'
            cd salt/external-formulas/openssh-formula
            git remote add upstream https://github.com/saltstack-formulas/openssh-formula.git
        '';
      };
      "code/salt/external-formulas/salt-formula" = {
        checkout = ''
          git clone 'git@gitlab.com:fishers-salt/external-formulas/salt-formula.git' '${config.home.homeDirectory}/code/salt/external-formulas/salt-formula'
            cd salt/external-formulas/salt-formula
            git remote add upstream https://gitlab.com/saltstack-formulas/salt-formula.git
        '';
      };
      "code/salt/external-formulas/ssf-formula" = {
        checkout = ''
          git clone 'git@gitlab.com:fishers-salt/external-formulas/ssf-formula.git' '${config.home.homeDirectory}/code/salt/external-formulas/ssf-formula'
            cd salt/external-formulas/ssf-formula
            git remote add upstream https://gitlab.com/myii/ssf-formula.git
        '';
      };
      "code/salt/external-formulas/template-formula" = {
        checkout = "git clone 'https://gitlab.com/saltstack-formulas/template-formula.git' '${config.home.homeDirectory}/code/salt/external-formulas/template-formula'";
      };
      "code/salt/external-formulas/timezone-formula" = {
        checkout = ''
          git clone 'git@gitlab.com:fishers-salt/external-formulas/timezone-formula.git' '${config.home.homeDirectory}/code/salt/external-formulas/timezone-formula'
            cd salt/external-formulas/timezone-formula
            git remote add upstream https://github.com/saltstack-formulas/timezone-formula.git
        '';
      };
      "code/salt/external-formulas/users-formula" = {
        checkout = ''
          git clone 'git@gitlab.com:fishers-salt/external-formulas/users-formula.git' '${config.home.homeDirectory}/code/salt/external-formulas/users-formula'
            cd salt/external-formulas/users-formula
            git remote add upstream https://github.com/saltstack-formulas/users-formula.git
        '';
      };
      "code/salt/external-formulas/vim-formula" = {
        checkout = ''
          git clone 'git@gitlab.com:fishers-salt/external-formulas/vim-formula.git' '${config.home.homeDirectory}/code/salt/external-formulas/vim-formula'
            cd salt/external-formulas/vim-formula
            git remote add upstream https://github.com/saltstack-formulas/vim-formula.git
        '';
      };
      "code/salt/external-formulas/wireguard-formula" = {
        checkout = ''
          git clone 'git@gitlab.com:fishers-salt/external-formulas/wireguard-formula.git' '${config.home.homeDirectory}/code/salt/external-formulas/wireguard-formula'
            cd salt/external-formulas/wireguard-formula
            git remote add upstream https://github.com/saltstack-formulas/wireguard-formula.git
        '';
      };
      "code/research/nix/aaronjanse-dotfiles" = {
        checkout = "git clone 'https://github.com/aaronjanse/dotfiles.git' '${config.home.homeDirectory}/code/research/nix/aaronjanse-dotfiles'";
      };
      "code/research/nix/andrew-kvalheim" = {
        checkout = "git clone 'https://codeberg.org/AndrewKvalheim/configuration.git' '${config.home.homeDirectory}/code/research/nix/andrew-kvalheim'";
      };
      "code/research/nix/areina-nixos-config" = {
        checkout = "git clone 'https://github.com/areina/nixos-config.git' '${config.home.homeDirectory}/code/research/nix/areina-nixos-config'";
      };
      "code/research/nix/axelf4-nixos-config" = {
        checkout = "git clone 'https://github.com/axelf4/nixos-config.git' '${config.home.homeDirectory}/code/research/nix/axelf4-nixos-config'";
      };
      "code/research/nix/badele-nix-homelab" = {
        checkout = "git clone 'https://github.com/badele/nix-homelab.git' '${config.home.homeDirectory}/code/research/nix/badele-nix-homelab'";
      };
      "code/research/nix/baitinq-nixos-config" = {
        checkout = "git clone 'https://github.com/Baitinq/nixos-config.git' '${config.home.homeDirectory}/code/research/nix/baitinq-nixos-config'";
      };
      "code/research/nix/balsoft-nixos-config" = {
        checkout = "git clone 'https://github.com/balsoft/nixos-config.git' '${config.home.homeDirectory}/code/research/nix/balsoft-nixos-config'";
      };
      "code/research/nix/barrucadu-nixfiles" = {
        checkout = "git clone 'https://github.com/barrucadu/nixfiles.git' '${config.home.homeDirectory}/code/research/nix/barrucadu-nixfiles'";
      };
      "code/research/nix/baughn-machine-config" = {
        checkout = "git clone 'https://github.com/Baughn/machine-config.git' '${config.home.homeDirectory}/code/research/nix/baughn-machine-config'";
      };
      "code/research/nix/bbigras-nix-config" = {
        checkout = "git clone 'https://github.com/bbigras/nix-config.git' '${config.home.homeDirectory}/code/research/nix/bbigras-nix-config'";
      };
      "code/research/nix/dissassembler-network" = {
        checkout = "git clone 'https://github.com/disassembler/network.git' '${config.home.homeDirectory}/code/research/nix/dissassembler-network'";
      };
      "code/research/nix/jonringer-nixpkgs-config" = {
        checkout = "git clone 'https://github.com/jonringer/nixpkgs-config.git' '${config.home.homeDirectory}/code/research/nix/jonringer-nixpkgs-config'";
      };
      "code/research/nix/khanelinix" = {
        checkout = "git clone 'https://github.com/khaneliman/khanelinix.git' '${config.home.homeDirectory}/code/research/nix/khanelinix'";
      };
      "code/research/nix/khanelivim" = {
        checkout = "git clone 'https://github.com/khaneliman/khanelivim.git' '${config.home.homeDirectory}/code/research/nix/khanelivim'";
      };
      "code/research/nix/longerhv-nixos-configuration" = {
        checkout = "git clone 'https://github.com/LongerHV/nixos-configuration.git' '${config.home.homeDirectory}/code/research/nix/longerhv-nixos-configuration'";
      };
      "code/research/nix/mangoiv-dotfiles" = {
        checkout = "git clone 'https://git.mangoiv.com/mangoiv/dotfiles.git' '${config.home.homeDirectory}/code/research/nix/mangoiv-dotfiles'";
      };
      "code/research/nix/matthew-croughan-nixcfg" = {
        checkout = "git clone 'https://github.com/MatthewCroughan/nixcfg.git' '${config.home.homeDirectory}/code/research/nix/matthew-croughan-nixcfg'";
      };
      "code/research/nix/mic92-dotfiles" = {
        checkout = "git clone 'https://github.com/Mic92/dotfiles.git' '${config.home.homeDirectory}/code/research/nix/mic92-dotfiles'";
      };
      "code/research/nix/misterio77-nix-config" = {
        checkout = "git clone 'https://github.com/Misterio77/nix-config.git' '${config.home.homeDirectory}/code/research/nix/misterio77-nix-config'";
      };
      "code/research/nix/nix-starter-configs" = {
        checkout = "git clone 'https://github.com/Misterio77/nix-starter-configs.git' '${config.home.homeDirectory}/code/research/nix/nix-starter-configs'";
      };
      "code/research/nix/nixy" = {
        checkout = "git clone 'https://github.com/anotherhadi/nixy.git' 'nixy'";
      };
      "code/research/nix/stellarhoof-nix-home" = {
        checkout = "git clone 'https://github.com/stellarhoof/nix-home.git' '${config.home.homeDirectory}/code/research/nix/stellarhoof-nix-home'";
      };
      "code/research/nix/stellarhoof-nixos-hosts" = {
        checkout = "git clone 'https://github.com/stellarhoof/nixos-hosts.git' '${config.home.homeDirectory}/code/research/nix/stellarhoof-nixos-hosts'";
      };
      "code/research/nix/wiedzmin-nixos-config" = {
        checkout = "git clone 'https://github.com/wiedzmin/nixos-config.git' '${config.home.homeDirectory}/code/research/nix/wiedzmin-nixos-config'";
      };
      "code/research/nix/will-t-dotfiles" = {
        checkout = "git clone 'https://github.com/wiltaylor/dotfiles.git' '${config.home.homeDirectory}/code/research/nix/will-t-dotfiles'";
      };
      "code/research/nix/xddxdd-nixos-config" = {
        checkout = "git clone 'https://github.com/xddxdd/nixos-config.git' '${config.home.homeDirectory}/code/research/nix/xddxdd-nixos-config'";
      };
      "code/research/nix/xddxdd-nur-packages" = {
        checkout = "git clone 'https://github.com/xddxdd/nur-packages.git' '${config.home.homeDirectory}/code/research/nix/xddxdd-nur-packages'";
      };
      "code/research/nix-anywhere/djacu-nixos-config" = {
        checkout = "git clone 'https://github.com/djacu/nixos-config.git' '${config.home.homeDirectory}/code/research/nix-anywhere/djacu-nixos-config'";
      };
      "code/research/nix-anywhere/nixos-anywhere-examples" = {
        checkout = "git clone 'https://github.com/numtide/nixos-anywhere-examples.git' '${config.home.homeDirectory}/code/research/nix-anywhere/nixos-anywhere-examples'";
      };
      "code/research/nvim/jdhao-nvim-config" = {
        checkout = "git clone 'https://github.com/jdhao/nvim-config.git' '${config.home.homeDirectory}/code/research/nvim/jdhao-nvim-config'";
      };
      "code/research/nvim/kickstart.nvim" = {
        checkout = "git clone 'https://github.com/nvim-lua/kickstart.nvim.git' '${config.home.homeDirectory}/code/research/nvim/kickstart.nvim'";
      };
      "code/research/nvim/launch.nvim" = {
        checkout = "git clone 'https://github.com/LunarVim/Launch.nvim.git' '${config.home.homeDirectory}/code/research/nvim/launch.nvim'";
      };
      "code/research/nvim/nvim-basic-ide" = {
        checkout = "git clone 'https://github.com/elyps/nvim-basic-ide.git' '${config.home.homeDirectory}/code/research/nvim/nvim-basic-ide'";
      };
      "code/research/other-dots/vonheikemen-dotfiles" = {
        checkout = "git clone 'https://github.com/VonHeikemen/dotfiles.git' '${config.home.homeDirectory}/code/research/other-dots/vonheikemen-dotfiles'";
      };
      "code/research/other-dots/bibjaw99-workstation" = {
        checkout = "git clone 'https://github.com/bibjaw99/workstation.git' '${config.home.homeDirectory}/code/research/other-dots/bibjaw99-workstation'";
      };
      "code/research/other-dots/enrico223-config" = {
        checkout = "git clone 'https://github.com/enrico223/.config.git' '${config.home.homeDirectory}/code/research/other-dots/enrico223-config'";
      };
      "code/terraform/cremona" = {
        checkout = "git clone 'git@gitlab.com:cremona-terraform/cremona.git' '${config.home.homeDirectory}/code/terraform/cremona'";
      };
      "code/terraform/fishers" = {
        checkout = "git clone 'git@code.ginstoo.net:team-ginstoo/fishers-salt/fishers-terraform.git' '${config.home.homeDirectory}/code/terraform/fishers'";
      };
    };
  };
}
