{ ... }:
{
  services.nextcloud-client = {
    enable = true;
    startInBackground = true;
  };
  home.file = {
    ".config/Nextcloud/sync-exclude.lst".source = ./files/nextcloud/sync-exclude.lst;
  };
}
