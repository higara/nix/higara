{ ... }:
{
  programs.qutebrowser = {
    quickmarks = {
      jinjaparser = "https://j2live.ttl255.com";
      pi-hole = "http://pihole.ginstoo.net/admin";
    };
    searchEngines = {
      about = "file:///home/stooj/code/docs/about/build/html/search.html?q={}&check_keywords=yes&area=default";
      botw = "https://uk.ign.com/wikis/the-legend-of-zelda-breath-of-the-wild/search?term={}";
      elite = "https://elite-dangerous.wikia.com/wiki/Special:Search?query={}";
      imdb = "https://www.imdb.com/find?ref_=nv_sr_fn&q={}&s=all";
      lutris = "https://lutris.net/games/?q={}";
      w40k = "https://warhammer40k.wikia.com/wiki/Special:Search?query={}";
      windward = "https://windward.gamepedia.com/index.php?search={}&title=Special%3ASearch&profile=default&fulltext=1";
    };
  };
}
