{config, ...}: {
  home.file = {
    ".config/zsh/zenv_functions".source = ./files/zsh/zshenv_functions;
    ".config/zsh/rc_includes/aliases-common.zsh" = {
      source = ./files/zsh/rc_includes/aliases-common.zsh;
    };
    ".config/zsh/rc_includes/comments.zsh" = {
      source = ./files/zsh/rc_includes/comments.zsh;
    };
    ".config/zsh/rc_includes/history.zsh" = {
      source = ./files/zsh/rc_includes/history.zsh;
    };
    ".config/zsh/rc_includes/vimmode.zsh" = {
      source = ./files/zsh/rc_includes/vimmode.zsh;
    };
  };
  programs.zsh = {
    enable = true;
    enableCompletion = true;
    enableVteIntegration = true;
    zprof.enable = false;
    completionInit = ''
      autoload -Uz compinit
      for dump in ~/.zcompdump(N.mh+24); do
        compinit
      done
      compinit -C
    '';
    dotDir = ".config/zsh";
    envExtra = ''
      source "$HOME/.config/zsh/zenv_functions"
      # Path settings
      pathprepend ~/.local/bin PATH;
      pathprepend ~/bin PATH;

      # The current directory should never be in $PATH
      pathremove . PATH
      pathremove "" PATH

      if [ -d /home/stooj/.config/zsh/env_includes ]; then
          for file in /home/stooj/.config/zsh/env_includes/*; do
              if [ -f "$file" ]; then
                  source "$file"
              fi
          done
      fi
    '';
    history = {
      extended = true;
      ignoreAllDups = true;
      ignoreSpace = true;
      path = "${config.xdg.dataHome}/zsh/history";
      save = 10000;
      share = true;
      size = 10000;
    };
    initExtra = ''
      for file in /home/stooj/.config/zsh/init.d/*(Nn); do
          source "$file"
      done

      for file in /home/stooj/.config/zsh/rc_includes/*(Nn); do
          source "$file"
      done

      for file in /home/stooj/.config/zsh/after.d/*(Nn); do
          source "$file"
      done
    '';
    initExtraFirst = ''
      # Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
      # Initialization code that may require console input (password prompts, [y/n]
      # confirmations, etc.) must go above this block; everything else may go below.
      if [[ -r "$\{XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-$\{(%):-%n}.zsh" ]]; then
        source "$\{XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-$\{(%):-%n}.zsh"
      fi
    '';
    logoutExtra = ''
      # when leaving the console, clear the screen to increase privacy
      if [ "$SHLVL" = 1 ]; then
          if which clear_console &> /dev/null; then
              clear_console -q
          fi
      fi
    '';
  };
}
