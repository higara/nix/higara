########################################################################
# File managed by Home Manager
# Your changes will be overwritten.
########################################################################

# Aliases
# Use coloured diffs (remember to install colordiff)
if command -v colordiff > /dev/null; then
	alias diff='colordiff'
fi

# If nvim is installed, launch it when vim is called
if command -v nvim > /dev/null; then
	alias vim='nvim'
	alias sudovimdiff="SUDO_EDITOR='nvim -d' sudoedit"
else
	alias sudovimdiff="SUDO_EDITOR='vim -d' sudoedit"
fi
