{ ... }: {
  home.file = {
    ".config/powerlevel10k/powerlevel10k.zsh" = {
      source = ./files/powerlevel10k/powerlevel10k.zsh;
    };
    ".config/zsh/after.d/99-powerlevel10k.zsh" = {
      source = ./files/powerlevel10k/after.d/99-powerlevel10k.zsh;
    };
  };
}
