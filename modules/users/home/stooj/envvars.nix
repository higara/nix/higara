{ config, ... }: {
  home.sessionPath = [
    "${config.home.homeDirectory}/.local/bin"
    "${config.home.homeDirectory}/.local/go/bin"
  ];
  home.sessionVariables = {
    GOPATH = "${config.home.homeDirectory}/.local/go";
    PULUMI_HOME = "${config.home.homeDirectory}/.local/pulumi";
  };
}
