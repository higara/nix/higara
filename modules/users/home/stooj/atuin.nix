{...}: {
  programs.atuin = {
    enable = true;
    enableBashIntegration = true;
    enableZshIntegration = true;
    settings = {
      filter_mode_shell_up_key_binding = "session";
      keymap_mode = "vim-normal";
      secrets_filter = true;
      store_failed = true;
    };
  };
}
