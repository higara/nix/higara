{pkgs, ...}: {
  programs.git = {
    enable = true;
    diff-so-fancy.enable = true;
    ignores = [
      # Anything with a tilda at the end is a temporary file or something.
      "*~"
      # Project files
      ".envrc"
      ".direnv/"
      "tags"
      "tags.lock"
      "tags.temp"
      # nvim files
      "lazy-lock.json"
      # asdf files
      ".tool-versions"
      # Python
      ".mypy_cache/"
      # Java
      "*.class"
      # Go
      ".go/"
      # Pulumi packages
      ".pulumi/"
    ];
    extraConfig = {
      init = {
        defaultBranch = "main";
      };
      merge = {
        tool = "vimdiff";
        conflictstyle = "diff3";
      };
      mergetool = {
        path = "${pkgs.neovim}/bin/nvim";
        "vimdiff" = {
          path = "${pkgs.neovim}/bin/nvim";
        };
        "vimdiff3" = {
          path = "${pkgs.neovim}/bin/nvim";
        };
      };
      # From https://rakhesh.com/coding/git-push-default/
      pull = {
        rebase = "false";
      };
      push = {
        default = "current";
      };
    };
  };
}
