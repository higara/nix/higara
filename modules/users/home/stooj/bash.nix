{ ... }:
{
  programs.bash = {
    enable = true;
  };
  home.file = {
    ".inputrc".text = "set editing-mode vi";
  };
}
