{pkgs, ...}: {
  programs.tmux = {
    enable = true;
    keyMode = "vi";
    plugins = with pkgs; [
      tmuxPlugins.vim-tmux-navigator
      tmuxPlugins.gruvbox
    ];
    prefix = "C-s";
    escapeTime = 250;
    historyLimit = 8000;
    sensibleOnTop = true;
    terminal = "tmux-256color";
    extraConfig = ''
      # Split windows with better keys, keeping cwd
      bind-key - split-window -v -c '#{pane_current_path}'
      bind-key '\' split-window -h -c '#{pane_current_path}'

      # Fine adjustment (1 or 2 cursor cells per bump)
      bind -n S-Left resize-pane -L 2
      bind -n S-Right resize-pane -R 2
      bind -n S-Down resize-pane -D 1
      bind -n S-Up resize-pane -U 1

      # Coarse adjustment (5 or 10 cursor cells per bump)
      bind -n C-Left resize-pane -L 10
      bind -n C-Right resize-pane -R 10
      bind -n C-Down resize-pane -D 5
      bind -n C-Up resize-pane -U 5

      # Smart pane switching with awareness of Vim splits.
      # See: https://github.com/christoomey/vim-tmux-navigator
      is_vim="ps -o state= -o comm= -t '#{pane_tty}' \
          | grep -iqE '^[^TXZ ]+ +(\\S+\\/)?g?(view|n?vim?x?)(diff)?$'"
      bind-key -n C-h if-shell "$is_vim" "send-keys C-h"  "select-pane -L"
      bind-key -n C-j if-shell "$is_vim" "send-keys C-j"  "select-pane -D"
      bind-key -n C-k if-shell "$is_vim" "send-keys C-k"  "select-pane -U"
      bind-key -n C-l if-shell "$is_vim" "send-keys C-l"  "select-pane -R"
      bind-key -n 'C-\' if-shell "$is_vim" "send-keys C-\\" "select-pane -l"
      bind-key -T copy-mode-vi C-h select-pane -L
      bind-key -T copy-mode-vi C-j select-pane -D
      bind-key -T copy-mode-vi C-k select-pane -U
      bind-key -T copy-mode-vi C-l select-pane -R
      bind-key -T copy-mode-vi 'C-\' select-pane -l

      set -ag terminal-overrides ",xterm-256color:RGB"

      # Renumber windows when we create/destroy them
      set-option -g renumber-windows on

      # Break pane into background window
      bind-key b break-pane -d

      # Join pane into current window
      bind-key j command-prompt -p "join pane from: " "join-pane -h -s '%%'"

      # Synchronize input of all panes
      bind-key y set-window-option synchronize-panes

      # Vi copypaste mode
      bind-key -T copy-mode-vi 'v' send-keys -X begin-selection
      bind-key -T copy-mode-vi 'y' send-keys -X copy-selection-and-cancel

      bind-key -T copy-mode-vi g switch-client -T copy-mode-vi-g
      bind-key -T copy-mode-vi-g _ send -X end-of-line \; send -X cursor-left

      # Clear the old name when renaming a window
      bind , rename-window "" \; command-prompt "rename-window '%%'"

      # Prevent zsh from renaming tmuxinator panes
      set-window-option -g allow-rename off
      set-window-option -g automatic-rename off

      # Since (automatic)-rename is off, we make an exception for shell windows:
      bind-key c new-window -c '#{pane_current_path}' \; set-window-option -q allow-rename on \; set-window-option  -q automatic-rename on

      # Change the colours of panes to highlight active and inactive ones
      set-option -g window-style 'bg=colour236'
      set-option -g window-active-style 'bg=black'

    '';
  };
}
