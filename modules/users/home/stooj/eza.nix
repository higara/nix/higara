{...}: {
  programs.eza = {
    enable = true;
    enableBashIntegration = true;
    enableZshIntegration = true;
    git = true;
    icons = "auto";
  };
  home.shellAliases = {
    ls = "eza";
    ll = "eza --all --long --classify=automatic";
    la = "eza --almost-all";
    l = "eza --classify=automatic";
  };
}
