{ ... }: {
  programs.zsh.antidote = {
    enable = true;
    useFriendlyNames = true;
    plugins = [
      "zsh-users/zsh-completions"
      "zsh-users/zsh-autosuggestions"
      "ohmyzsh/ohmyzsh path:plugins/tmux"
      "mafredri/zsh-async"
      "zdharma/fast-syntax-highlighting"
      "romkatv/powerlevel10k"
    ];
  };
}
