{...}: {
  programs.nixvim = {
    # `telescope.nvim` is a highly extendable fuzzy finder over lists.
    # Built on the latest awesome features from neovim core. Telescope
    # is centered around modularity, allowing for easy customization.
    # See https://github.com/nvim-telescope/telescope.nvim
    plugins.telescope = {
      enable = true;
      keymaps = {
        "<leader>?" = {
          action = "oldfiles";
          options.desc = "[?] Find recently opened files";
        };
        "<leader>ff" = {
          action = "find_files";
          options.desc = "[f]ind [f]iles";
        };
        "<leader>/" = {
          action = "current_buffer_fuzzy_find";
          options.desc = "[/] Fuzzily search in current buffer";
        };
        "<leader>fw" = {
          action = "grep_string";
          options.desc = "[f]ind current [w]ord";
        };
        "<leader>fd" = {
          action = "diagnostics";
          options.desc = "[f]ind [d]iagnostics";
        };
        "<leader>fg" = {
          action = "live_grep";
          options.desc = "[f]ind by [g]rep";
        };
        "<leader>fb" = {
          action = "buffers";
          options.desc = "[f]ind buffers";
        };
        "<leader>fh" = {
          action = "help_tags";
          options.desc = "[f]ind [h]elp";
        };
        "<leader>fk" = {
          action = "keymaps";
          options.desc = "[f]ind [k]eymaps";
        };
      };
      extensions.media-files = {
        enable = true;
        dependencies = {
          epub-thumbnailer.enable = true;
          pdftoppm.enable = true;
        };
      };
      settings = {
        defaults = {
          file_ignore_patterns = [
            "^.git/"
            "^__pycache__/"
            "^venv/"
            "yarn%.lock"
            "package%-lock%.json"
            "pnpm%-lock%.yaml"
            "node_modules/.*"
            "deno%.lock"
            "%.svg"
            "%.png"
            "%.jpeg"
            "%.jpg"
            "%.ico"
            "%.webp"
            "%.avif"
            "%.heic"
            "%.mp3"
            "%.mp4"
            "%.mkv"
            "%.mov"
            "%.wav"
            "%.flv"
            "%.avi"
            "%.webm"
            "%.env.*"
            "%.db"
            "%.zip"
          ];
        };
      };
    };
  };
}
