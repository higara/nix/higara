{...}: {
  programs.nixvim = {
    keymaps = [
      {
        key = "<leader>cc";
        mode = "n";
        action = ":set cursorcolumn!<CR>";
        options = {
          silent = true;
          noremap = true;
          desc = "Toggle vertical column";
        };
      }
      {
        mode = "v";
        key = "J";
        action = ":m '>+1<CR>gv=gv";
        options.desc = "Use the move command to move visual blocks down";
      }
      {
        mode = "v";
        key = "K";
        action = ":m '<-2<CR>gv=gv";
        options.desc = "Use the move command to move visual blocks up";
      }
      # Quickfix navigation
      {
        key = "<C-k>";
        action = "<cmd>cnext<CR>";
      }
      {
        key = "<C-j>";
        action = "<cmd>cprev<CR>";
      }
      {
        key = "<leader>k";
        action = "<cmd>lnext<CR>";
      }
      {
        key = "<leader>j";
        action = "<cmd>lprev<CR>";
      }
      # Search and replace the word under the cursor.
      {
        key = "<leader>s";
        action = "[[:%s/\<<C-r><C-w>\>/<C-r><C-w>/I<Left><Left><Left>]]";
      }
      # Set the executable bit on for the current file
      {
        key = "<leader>X";
        action = "<cmd>!chmod +x %<CR>";
        options = {
          silent = true;
        };
      }
      # Sometimes you are working on a set of windows but you temporarily
      # want to zoom in on a particular one without losing your layout.
      # This remap command sets the current window width and height to
      # maximum
      {
        key = "<leader>+";
        action = ":wincmd _<cr> :wincmd |<cr>";
      }
      # Once you've had a good look at this window, it's time to make
      # everything fair and square again. Rebalance all the windows with
      # this command.
      {
        key = "<leader>=";
        action = ":wincmd =<cr>";
      }
      # I occasionally like to see non-printing characters but not all
      # that often, so I've got a keybind for it.
      {
        key = "<leader>l";
        action = ":set list!<cr>";
      }
      # Trouble keybinds
      {
        key = "<leader>xx";
        action = "<cmd>Trouble diagnostics toggle<cr>";
        options.desc = "Diagnostics (Trouble)";
      }
      {
        key = "<leader>xX";
        action = "<cmd>Trouble diagnostics toggle filter.buf=0<cr>";
        options.desc = "Buffer Diagnostics (Trouble)";
      }
      {
        key = "<leader>cs";
        action = "<cmd>Trouble symbols toggle focus=false<cr>";
        options.desc = "Symbols (Trouble)";
      }
      {
        key = "<leader>cl";
        action = "<cmd>Trouble lsp toggle focus=false win.position=right<cr>";
        options.desc = "LSP Definitions / references / ... (Trouble)";
      }
      {
        key = "<leader>xL";
        action = "<cmd>Trouble loclist toggle<cr>";
        options.desc = "Location List (Trouble)";
      }
      {
        key = "<leader>xQ";
        action = "<cmd>Trouble qflist toggle<cr>";
        options.desc = "Quickfix List (Trouble)";
      }
      # Luasnip bindings
      {
        action.__raw = ''
          function()
            local luasnip = require('luasnip')
            if luasnip.choice_active() then
              luasnip.change_choice(1)
            end
          end
        '';
        key = "<C-f>";
        mode = ["i" "s"];
        options = {
          silent = true;
        };
      }
      {
        key = "<leader>u";
        action.__raw = "vim.cmd.UndotreeToggle";
        mode = "n";
      }
      {
        action.__raw = "vim.lsp.buf.signature_help";
        key = "<C-h>";
        mode = "i";
      }
      # All my search options mean that I have a lot of highlighted stuff
      # on the screen and it's nice to be able to get rid of it quickly.
      # I used to use the keybind below, but the actual ex command is only
      # four characters, so I've disabled my shortcut and am going to try
      # and remember the actual command. It's given as the long version
      # here, but the short version is: `:noh`
      # {
      #   action = ":nohlsearch<cr>";
      #   key = "<leader><space>";
      #   options = {
      #     silent = true;
      #   };
      # }
      # {
      #   action = ''
      #     function()
      #       local luasnip = require('luasnip')
      #       luasnip.expand()
      #     end
      #   '';
      #   lua = true;
      #   key = "<Tab>";
      #   mode = "i";
      #   options = {
      #     silent = true;
      #   };
      # }
      # {
      #   action = ''
      #     function()
      #       local luasnip = require('luasnip')
      #       luasnip.jump( 1)
      #     end
      #   '';
      #   lua = true;
      #   key = "<Tab>";
      #   mode = ["i" "s"];
      #   options = {
      #     silent = true;
      #   };
      # }
      # {
      #   action = ''
      #     function()
      #       local luasnip = require('luasnip')
      #       luasnip.jump(-1)
      #     end
      #   '';
      #   lua = true;
      #   key = "<S-Tab>";
      #   mode = ["i" "s"];
      #   options = {
      #     silent = true;
      #   };
      # }
      ## Neorg global mappings
      {
        key = "<leader><leader>ww";
        mode = "n";
        action = ":Neorg index<CR>";
        options.desc = "Open the index file for the default norg workspace";
      }
      {
        key = "<leader><leader>ws";
        mode = "n";
        action = ":Neorg workspace ";
        options.desc = "Change workspace";
      }
      {
        key = "<leader><leader>w<leader>w";
        mode = "n";
        action = ":Neorg journal today<CR>";
        options.desc = "Open diary file for today.";
      }
      {
        key = "<leader><leader>w<leader>y";
        mode = "n";
        action = ":Neorg journal yesterday<CR>";
        options.desc = "Open diary file for yesterday.";
      }
      {
        key = "<leader><leader>w<leader>m";
        mode = "n";
        action = ":Neorg journal tomorrow<CR>";
        options.desc = "Open diary file for tomorrow.";
      }
    ];
  };
}
