{pkgs, ...}: {
  programs.nixvim = {
    plugins = {
      conform-nvim = {
        enable = true;
        settings = {
          default_format_opts = {
            lsp_format = "first";
            quiet = true;
          };
          formatters_by_ft = {
            asm = ["asmfmt"];
            c = ["astyle"];
            cpp = ["astyle"];
            css = ["prettierd" "prettier"];
            cmake = ["cmake_format"];
            go = ["goimports" "gofumpt" "golines"];
            html = ["prettierd" "prettier"];
            javascript = ["prettierd" "prettier"];
            javascriptreact = ["prettier"];
            json = ["prettier"];
            lua = ["stylua"];
            markdown = ["prettier"];
            nix = ["alejandra"];
            python = ["isort" "black"];
            rust = ["rustfmt"];
            sh = ["shfmt"];
            typescript = ["prettierd" "prettier"];
            typescriptreact = ["prettier"];
            yaml = ["prettierd" "prettier"];
          };
          formatters = {
            asmfmt = {
              command = "asmfmt";
              stdin = true;
            };
          };
          format_on_save = {
            lsp_format = "first";
            timeout_ms = 2000;
          };
          notify_no_formatters = true;
          notify_on_error = true;
        };
      };
      # nvim-autopairs
      # https://github.com/windwp/nvim-autopairs/
      # A super powerful autopair plugin for Neovim that supports multiple
      # characters.
      nvim-autopairs.enable = true;

      # Another tpope goldun'
      vim-surround.enable = true;
    };

    extraPlugins = with pkgs.vimPlugins; [
      # i3wm config syntax highlighting
      i3config-vim
      # Salt syntax files. Less useful for me these days.
      salt-vim
      # Jinja syntax files.
      jinja-vim
      # Detect and configure tabstop and shiftwidth automatically.
      # Thanks again tpope.
      vim-sleuth
      # And another one from tpope. :exploding_head:
      # https://github.com/tpope/vim-abolish
      # Can use it to cooerce snake_case to MixedCase, camelCase, and back.
      vim-abolish
    ];
    extraConfigLuaPre = ''
      -- Formatting function for conform
      _G.format_with_conform = function()
      	local conform = require("conform")
      	conform.format({
      		lsp_fallback = true,
      		async = false,
      		timeout_ms = 2000,
      	})
      end
    '';
  };
}
