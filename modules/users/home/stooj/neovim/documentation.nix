{pkgs, ...}: {
  programs.nixvim = {
    files = {
      "ftplugin/markdown.lua" = {
        localOpts = {
          textwidth = 0;
          wrap = true;
          breakindent = true;
          breakindentopt = "shift:2";
          linebreak = true;
        };
      };
      "ftplugin/vimwiki.lua" = {
        localOpts = {
          textwidth = 0;
          wrap = true;
          breakindent = true;
          breakindentopt = "shift:2";
          linebreak = true;
        };
      };
      "ftplugin/norg.lua" = {
        localOpts = {
          textwidth = 0;
          wrap = true;
          breakindent = true;
          breakindentopt = "shift:2";
          linebreak = true;
        };
      };
    };
    plugins = {
      neorg = {
        enable = true;
        modules = {
          "core.defaults" = {
            __empty = null;
          };
          "core.completion" = {
            config = {
              engine = "nvim-cmp";
              name = "[Norg]";
            };
          };
          "core.concealer" = {
            config = {
              icon_preset = "diamond";
            };
          };
          "core.esupports.metagen" = {
            config = {
              type = "auto";
              update_date = true;
            };
          };
          "core.export" = {
            __empty = null;
          };
          "core.export.markdown" = {
            config = {
              extensions = "all";
            };
          };
          "core.journal" = {
            config = {
              journal_folder = "diary";
              strategy = "nested";
            };
          };
          "core.keybinds" = {
            config = {
              default_keybinds = true;
              neorg_leader = "<Leader><Leader>";
            };
          };
          "core.looking-glass" = {
            __empty = null;
          };
          "core.presenter" = {
            config = {
              zen_mode = "zen-mode";
            };
          };
          "core.qol.toc" = {
            __empty = null;
          };
          "core.qol.todo_items" = {
            __empty = null;
          };
          "core.summary" = {
            __empty = null;
          };
          "core.tangle" = {
            config = {
              report_on_empty = false;
            };
          };
          # Not supported in neovim < 10.0.0
          # "core.ui.calendar" = {
          #   __empty = null;
          # };
        };
      };
      treesitter = {
        # From https://haseebmajid.dev/posts/2024-04-21-til-how-to-fix-neorg-metadata-treesitter-issues-with-nixvim/
        # options.programs.bash.promptInit.default +
        grammarPackages = with pkgs.tree-sitter-grammars; [
          tree-sitter-norg
          tree-sitter-norg-meta
        ];
      };
    };
    #### OR (if using overlay) ####
    # extraConfigLua = ''
    #   require("neorg").setup({
    #     load = {
    #       ["core.defaults"] = {},
    #       ["core.completion"] = {
    #         config = {
    #           engine = "nvim-cmp",
    #         },
    #       },
    #       ["core.concealer"] = {},
    #       ["core.dirman"] = {
    #         config = {
    #           workspaces = {
    #             about = "~/code/docs/neorg/about",
    #             ginstoo = "~/code/docs/neorg/ginstoo",
    #             higara = "~/code/docs/neorg/higara",
    #           },
    #           default_workspace = "ginstoo",
    #         },
    #       },
    #     },
    #   })
    # '';
    extraPlugins = [
      # Vimwiki is my knowledgebase of choice at the moment. I've looked at
      # Obsidian, Logseq, I've tried using plain markdown files or a sphinx
      # project, but vimwiki is the closest so far and has definition lists.
      # Definition lists are important and it _really_ hurts Markdown not to have
      # them.
      # That being said, there are some issues. I love the HTML output of vimwiki
      # if you are using wiki syntax, but if you switch to markdown then HTML
      # output is not supported. I'd have to script something.
      # Gitlab doesn't recognise the syntax either, so the files aren't rendered
      # very well in my VCS.
      # And Gin will never use it 😭.
      # But the diary is great and the shortcuts are useful and I would like to
      # spend more time getting used to actually typing my thoughts and storing
      # them well.
      pkgs.vimPlugins.vimwiki
      # Workaround for neorg
      (pkgs.vimUtils.buildVimPlugin {
        inherit (pkgs.luaPackages.lua-utils-nvim) pname version src;
      })
      (pkgs.vimUtils.buildVimPlugin {
        inherit (pkgs.luaPackages.pathlib-nvim) pname version src;
      })
      (pkgs.vimUtils.buildVimPlugin {
        inherit (pkgs.luaPackages.nvim-nio) pname version src;
      })
    ];
  };
}
