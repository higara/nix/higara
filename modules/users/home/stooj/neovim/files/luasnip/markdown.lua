local ls = require("luasnip")
local s = ls.snippet
local sn = ls.snippet_node
local isn = ls.indent_snippet_node
local t = ls.text_node
local i = ls.insert_node
local f = ls.function_node
local c = ls.choice_node
local d = ls.dynamic_node
local r = ls.restore_node
local events = require("luasnip.util.events")
local ai = require("luasnip.nodes.absolute_indexer")
local extras = require("luasnip.extras")
local l = extras.lambda
local rep = extras.rep
local p = extras.partial
local m = extras.match
local n = extras.nonempty
local dl = extras.dynamic_lambda
local fmt = require("luasnip.extras.fmt").fmt
local fmta = require("luasnip.extras.fmt").fmta
local conds = require("luasnip.extras.expand_conditions")
local postfix = require("luasnip.extras.postfix").postfix
local types = require("luasnip.util.types")
local parse = require("luasnip.util.parser").parse_snippet
local ms = ls.multi_snippet
local k = require("luasnip.nodes.key_indexer").new_key

return {
	s({ trig = "<!", name = "HTML comment snippet" }, {
		t("<!-- "),
		i(1),
		t(" -->"),
	}),
	s({ trig = "TODO", name = "TODO note as an HTML comment" }, {
		t("<!-- TODO "),
		i(1),
		t(" -->"),
	}),
	s(
		{ trig = "dl", dscr = "definition list in HTML" },
		fmt(
			[[
            <dl>
                <dt>{}</dt>
                <dd>{}</dd>
            </dl>
        ]],
			{ i(1, "definition name"), i(2, "definition description") }
		)
	),
	s(
		{ trig = "dt", name = "definition item in HTML" },
		fmt(
			[[
                <dt>{}</dt>
                <dd>{}</dd>
            ]],
			{ i(1, "definition name"), i(2, "definition description") }
		)
	),
}
