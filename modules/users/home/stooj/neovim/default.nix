{pkgs, ...}: {
  imports = [
    ./keymaps.nix
    ./style.nix
    ./documentation.nix
    ./telescope.nix
    ./treesitter.nix
    ./folds.nix
    ./lsp.nix
    ./completion.nix
    ./format.nix
    ./lint.nix
    ./debug.nix
  ];

  programs.nixvim = {
    defaultEditor = true;
    enable = true;
    enableMan = true;
    highlight = {
      # Highlight groups for active and inactive windows
      ActiveWindow.bg = "#282828";
      InactiveWindow.bg = "#32302f";
    };
    globals = {
      # Set the leader key to be something easy to type. The default is
      # `\` and maybe I should try to relearn using that rather than my
      # preferred option which is `,`.
      #
      # Nope, `\` is horrible and I hate it. But `,` is a really useful
      # bind and I should use it more, so I want to find something that's
      # not used for anything useful but also easy to hit.
      # Woah, <space>. Space does *nothing*. Well, it does the same as
      # `l`. So no loss there, because `l` is a great key to press and I
      # use it all the time.
      mapleader = " ";
      # But I need a localleader too! Currently using `,`, but I should
      # change it to something that doesn't mask something useful.
      # TODO: Change me to something else.
      maplocalleader = ",";
      # Netrw file browser configurations
      # See https://vonheikemen.github.io/devlog/tools/using-netrw-vim-builtin-file-explorer/
      # Work around file moving errors
      netrw_keepdir = 0;
      # Hide the top banner (press `I` to show it)
      netrw_banner = 0;
      # Default to recursive file copying
      netrw_localcopydircmd = "cp --recursive";
      # Don't let csv plugin throw an error if it can't figure out a delimiter
      csv_default_delim = ",";
    };

    opts = {
      # I've always made a lot of use of modelines, even though everyone
      # on the internet says it's a bad idea. I also ran my own email
      # server for years, so 😛 to common wisdom.
      # However, I'm not running as root and they are really very useful.
      # Maybe a future improvement would be to only enable the modeline if
      # the file is under a certain directory. All my code lives in
      # `~/code`, but any random stuff I get from the internet goes into
      # `~/tmp`, which now I come to tell someone is not a great name for
      # that directory. I should stick it in `~/src`. Anyway, the point is
      # I trust files in `~/code` and I don't trust files in `~/src`
      # (maybe _anywhere_ else).
      modeline = true;

      # Because I use tools like Salt and Ansible, the first few lines of
      # a file are often a disclaimer saying 'any edits here will be sent
      # to oblivion' or similar. So the modeline sometimes get pushed a
      # bit further down the file.
      modelines = 6;

      # Code should be wrapped at 80. There are lots of arguments on the
      # internet about why this is a good or a bad thing, and none of them
      # matter. It's a good thing because short lines are easier to read.
      # Long lines are hard to read, and anyone who says otherwise is
      # gaslighting you. Besides, I use a tiled window manager, and my
      # terminals take up half the screen or less sometimes. But that's a
      # selfish reason, which doesn't negate the fact that all code should
      # be wrapped at 80 characters.
      textwidth = 80;
      # However, if code isn't wrapped, don't make navigation annoying by
      # wrapping it visually.
      wrap = false;

      # clipboard = "unnamedplus";
      # Make sure you can always see a few lines above and below the current one
      scrolloff = 8;
      # Same for horizontal; also ensures that window snaps back to left after a
      # long line
      sidescrolloff = 8;

      # ==============
      # Search options
      # ==============
      # When I search for something, highlight all the matches, not just
      # the next one.
      hlsearch = true;
      # Highlight searches even before I've finished typing in the query.
      # Just match as I type.
      incsearch = true;
      # Usually I want my searches to be case-insensitive, so providing
      # the whole query is lower case, then match any case.
      ignorecase = true;
      # But if I have any capitals in my search string, then it's a signal
      # that I care about case after all.
      smartcase = true;
      # Make replacements default to global ones (everything on the line).
      # If you want to just change the first instance, add a g to the end
      # of the search
      gdefault = true;
      # Show the effects of `:substitute`, `:smagic`, `:snomagic` and user
      # commands with the `:command-preview` flag as you type in a
      # split.
      inccommand = "split";
      virtualedit = "block";
      termguicolors = true;
      foldmethod = "expr";
      # foldexpr = "nvim_treesitter#foldexpr()";
      foldenable = true;
      # The ruler option shows the current line and column number at the
      # bottom-right of the buffer. It's sometimes useful, especially when
      # I am counting characters or lining things up.
      # It's on by default, but explicit is better than implicit, yeah?
      ruler = true;
      # Even though the current line number is shown on the ruler, I find
      # myself looking for it so often that it's handy to have it in the
      # classical place on the left hand side of the screen. I also use
      # relativenumber (see the next setting below), but when you combine
      # the two options then the currently highlighted line shows the
      # absolute line number for the current line, while the lines above
      # and below have a relative value.
      number = true;
      # I like a colorcolumn to show me where column 80 is, makes resizing
      # terminals a bit easier - I can make sure they keep a useful size.
      colorcolumn = "80";
      # Relativenumber is wonderful for performing actions on nearby lines
      # and lets you give a relative range at a glance, rather than having
      # to type out an absolute range (usually more keypresses) or have to
      # do complex mental arithmetic to subtract three from the current
      # line number.
      relativenumber = true;
      # ====================
      # Indentation Settings
      # ====================
      # If a filetype doesn't have a specific indent ruleset, I like to
      # use spaces as a default. Tabs are not my friend.
      # These can be overridden by ft indentation rules in
      # $VIMCONFIG/after/indent
      shiftwidth = 4;
      tabstop = 4;
      softtabstop = 0;
      expandtab = true;
      # The default symbols are a bit dull though, and we have a modern
      # terminal and a nerd-font, so make the symbols a bit cooler looking
      listchars = "tab:▸ ,eol:¬";
      # And, here's a neat trick that's behind a ******* medium paywall
      # but I got access via a free trial. Different colours for the
      # active and inactive windows, something similar to my Tmux setup.
      # Active windows will appear darker than the inactive ones.
      winhighlight = "Normal:ActiveWindow,NormalNC:InactiveWindow";
      # Splits are opened above and to the left by default, which doesn't
      # really jive with my expectations. Open new splits below and to the
      # right, just like i3.
      splitbelow = true;
      splitright = true;
      #  Disable the swapfile because our undo history is going to replace
      # that.
      swapfile = false;
      # Don't backup my files if I overwrite an existing file. I didn't
      # know this was a thing before, and I shudder to think how many
      # backup files I've left sitting in a directory somewhere.
      backup = false;
      # Setting my undo dir to the new default
      # undodir = ''os.getenv("HOME") .. "/.local/state/nvim/undo"'';
      undofile = true;
    };

    plugins = {
      # Commenting and uncommenting things. Makes that very easy.
      # See https://github.com/numToStr/Comment.nvim
      comment.enable = true;

      # Vim Fugitive
      # See https://github.com/tpope/vim-fugitive
      # Fugitive is the premier Vim plugin for Git. Or maybe it's the
      # premier Git plugin for Vim? Either way, it's "so awesome, it
      # should be illegal". That's why it's called Fugitive.
      # Everything I do is in git somewhere.
      fugitive.enable = true;

      # Git integration for buffers
      # https://github.com/lewis6991/gitsigns.nvim
      # Super fast git decorations implemented purely in lua.
      gitsigns.enable = true;

      # Oil
      # stevearc/oil.nvim: Neovim file explorer: edit your filesystem like
      # a buffer
      # https://github.com/stevearc/oil.nvim
      # A vim-vinegar like file explorer that lets you edit your
      # filesystem like a normal Neovim buffer.
      oil.enable = true;

      # I use vim inside of tmux a lot. By default, it's actually a bit of
      # a pain because you need to context-switch between tmux windows and
      # vim windows. You need to use <Ctrl>+w for moving around vim, then
      # <Ctrl>+b for moving around tmux. Worse still, by default you need
      # to use the arrow keys for tmux.

      # This plugin adds some keybindings for navigating in vim so you
      # don't need the <Ctrl>+w key before a movement key, just
      # `<Ctrl>h/j/k/l`. But (with a bit of extra tmux config) those same
      # keys work for tmux, so you can use the same bindings for either.

      # If that's not enough, the plugin lets you _seamlessly_ move
      # between vim and tmux; the plugin is smart enough to know if you
      # are moving to another vim window or moving to a tmux window.

      # This is the most essential plugin I use, and I promise it's a
      # game-changer for anyone who uses both vim and tmux.
      tmux-navigator.enable = true;

      # Undotree
      #
      # See https://github.com/mbbill/undotree
      #
      # Undotree visualizes the undo history and makes it easy to browse
      # and switch between different undo branches. You may be wondering,
      # what are undo "branches" anyway? They're a feature of Vim that
      # allow you to go back to a prior state even after it has been
      # overwritten by later edits. For example: In most editors, if you
      # make some change A, followed by change B, then go back to A and
      # make another change C, normally you wouldn't be able to go back to
      # change B because the undo history is linear. That's not the case
      # with Vim, however. Vim internally stores the entire edit history
      # for each file as a single, monolithic tree structure; this plug-in
      # exposes that tree to you so that you can not only switch back and
      # forth between older and more recent edits linearly but can also
      # switch between diverging branches.
      #
      # The recommended mapping for UndoTree is <leader><F5>, which really
      # doesn't sit well with me. I don't like to use function keys at the
      # best of times, they often do something weird on modern machines
      # like turn the fan on or the screen off, so I'd rather try
      # something closer to home and that fits in my brain better.
      undotree.enable = true;
    };
    extraPackages = with pkgs; [
      # Formatters
      alejandra
      asmfmt
      astyle
      black
      cmake-format
      gofumpt
      golines
      gotools
      isort
      nodePackages.prettier
      prettierd
      rustfmt
      shfmt
      stylua
      # Linters
      commitlint
      eslint_d
      golangci-lint
      hadolint
      html-tidy
      luajitPackages.luacheck
      markdownlint-cli
      nodePackages.jsonlint
      pylint
      ruff
      shellcheck
      tflint
      vale
      yamllint
      # Debuggers / misc deps
      asm-lsp
      bashdb
      cargo
      clang-tools
      delve
      dotnet-sdk_8
      fd
      gcc
      gdb
      go
      lldb_17
      llvmPackages_17.bintools-unwrapped
      marksman
      nodejs
      python3
      ripgrep
      rr
      tree-sitter
      zig
    ];
    extraConfigLua = ''
      -- There are people in the world that don't like syntax highlighting
      -- because it's distracting. There are also people in the world that
      -- don't like tea, so it takes all sorts, eh?
      -- I like syntax highlighting and lots of pretty colours. Turn on
      -- the colors.
      vim.cmd('syntax enable')

      vim.wo.foldlevel = 99
      vim.wo.conceallevel = 2
      -- ============
      -- AutoCommands
      -- ============

      -- System options
      -- ==============

      -- This autocommand remembers the position of the curser when a file
      -- is closed, and navigate back to that position when the file is
      -- opened again.
      vim.api.nvim_create_autocmd("BufRead", {
              pattern = '*',
              command = [[
              if &ft !~# 'commit\|rebase' && line("'\"") > 1 && line("'\"") <= line("$") | exe 'normal! g`"' | endif
              ]],
              })

      -- Windows and panes
      -- =================

      -- Because I'm using a tiling window manager (i3), the size of my
      -- nvim UI changes quite frequently as I open and close other
      -- windows below or to the right of nvim. This is fine if I only
      -- have one buffer/pane/not quite sure of the difference on the
      -- screen, but if I have more than one then they tend to get all
      -- squashed up and a bit unusable. This autocommand resizes and
      -- redistributes windows fairly when nvim changes dimensions.
      vim.api.nvim_create_autocmd(
              "VimResized", {
              pattern = '*',
              command = ':wincmd ='
              })

      -- Undo defence
      -- ============
      -- au BufWritePre /tmp/* setlocal noundofile
      vim.api.nvim_create_autocmd("BufWritePre", {
              pattern = {'/tmp/*', '/dev/shm/pass*'},
              command = "setlocal noundofile"
              })


      -- =============
      -- Typo defences
      -- =============

      -- Because ex commands start with a `:`, you end up holding the
      -- shift button down. Sometimes you hold it down for a little bit
      -- too long and accidentally capitalize the next character as well.
      -- These are a few rebinds for when that happens and you are trying
      -- to write, quit, or both.
      --
      -- NOTE: `vim.keymap.set` does not support abbreviations in the
      -- current release of neovim, so I've got this workaround in the
      -- mean time. It's coming though, see
      -- https://github.com/neovim/neovim/pull/23803
      --
      -- Once v0.10 comes out, this should work:
      -- vim.keymap.set('ca', 'W!', 'w!')
      --
      -- In the mean time, set up these abbreviations with vim commands:
      vim.cmd('cnoreabbrev W! w!')
      vim.cmd('cnoreabbrev Q! q!')
      vim.cmd('cnoreabbrev Wq wq')
      vim.cmd('cnoreabbrev Wa wa')
      vim.cmd('cnoreabbrev wQ wq')
      vim.cmd('cnoreabbrev WQ wq')
      vim.cmd('cnoreabbrev W w')
      vim.cmd('cnoreabbrev Q q')
      -- Delete the currently selected text into the void register to
      -- preserve the current paste register.
      -- TODO: Find out how to do this in keymaps
      vim.keymap.set("x", "<leader>p", [["_dP]])
      vim.keymap.set({ "n", "v" }, "<leader>d", [["_d]])
      -- Yank things into the system clipboard register
      vim.keymap.set({ "n", "v" }, "<leader>y", [["+y]])
      vim.keymap.set("n", "<leader>Y", [["+Y]])

      vim.cmd.highlight({"link", "netrwMarkFile", "Search"})
    '';
    extraPlugins = with pkgs.vimPlugins; [
      # Fix incrementing and decrementing dates, tpope style
      # https://github.com/tpope/vim-speeddating
      vim-speeddating

      # Vim Rhubarb
      # https://github.com/tpope/vim-rhubarb
      # - Enables :GBrowse from fugitive.vim to open GitHub URLs.
      # - In commit messages, GitHub issues, issue URLs, and collaborators can
      #   be omni-completed (<C-X><C-O>, see :help compl-omni). This makes
      #   inserting those Closes #123 remarks slightly easier than copying and
      #   pasting from the browser.
      vim-rhubarb

      # Fugitive Gitlab
      # https://github.com/shumphrey/fugitive-gitlab.vim
      # This plugin allows you to use it with https://gitlab.com or your own
      # private GitLab instance.
      # - Enables :GBrowse from fugitive.vim to open GitLab URLs
      # - In commit messages, GitLab issues and users can be omni-completed
      #   (<C-X><C-O>, see :help compl-omni).
      fugitive-gitlab-vim

      # This plugin is used for handling column separated data with Vim. Usually
      # those files are called csv files and use the ',' as delimiter, though
      # sometimes they use e.g. the '|' or ';' as delimiter and there also
      # exists fixedwidth columns. The aim of this plugin is to ease handling
      # these kinds of files.
      csv-vim
    ];
  };
  home.file = {
    ".config/nvim/after/ftplugin/lua.lua" = {
      source = ./files/after/ftplugin/lua.lua;
    };
    ".config/nvim/after/ftplugin/norg.lua" = {
      source = ./files/after/ftplugin/norg.lua;
    };
    ".config/nvim/after/plugin/devicons.lua" = {
      source = ./files/after/plugin/devicons.lua;
    };
    ".config/nvim/luasnip/all.lua" = {
      source = ./files/luasnip/all.lua;
    };
    ".config/nvim/luasnip/markdown.lua" = {
      source = ./files/luasnip/markdown.lua;
    };
    ".config/nvim/luasnip/python.lua" = {
      source = ./files/luasnip/python.lua;
    };
    ".config/nvim/luasnip/vimwiki.lua" = {
      source = ./files/luasnip/vimwiki.lua;
    };
  };
}
