{config, ...}: let
  helpers = config.lib.nixvim;
in {
  programs.nixvim = {
    plugins = {
      cmp = {
        enable = true;
        # Setting this means we don't need to explicitly enable
        # each completion source, so long as the plugin is listed
        # in https://github.com/nix-community/nixvim/blob/cd32dcd50fa98cd03e2916b6fd47e31deffbca24/plugins/completion/cmp/cmp-helpers.nix#L23
        autoEnableSources = true;
        settings = {
          snippet = {
            expand = "function(args) require('luasnip').lsp_expand(args.body) end";
          };
          sources = [
            {name = "nvim_lsp";}
            {name = "luasnip";}
            {name = "path";}
            {name = "buffer";}
            {name = "nvim_lua";}
            {name = "emoji";}
            {name = "digraphs";}
          ];
          mapping = {
            "<C-d>" = "cmp.mapping.scroll_docs(-4)";
            "<C-f>" = "cmp.mapping.scroll_docs(4)";
            "<C-e>" = "cmp.mapping.close()";
            "<C-p>" = "cmp.mapping.select_prev_item(cmp_select)";
            "<C-n>" = "cmp.mapping.select_next_item(cmp_select)";
            "<C-space>" = "cmp.mapping.complete()";
            "<C-y>" = "cmp.mapping.confirm({ select = true, behavior = cmp.ConfirmBehavior.Insert })";
          };
          window.documentation.border = [
            "╭"
            "─"
            "╮"
            "│"
            "╯"
            "─"
            "╰"
            "│"
          ];
        };
        cmdline = {
          "/" = {
            mapping = {
              __raw = "cmp.mapping.preset.cmdline()";
            };
            sources = [
              {
                name = "buffer";
              }
            ];
          };
          ":" = {
            mapping = {
              __raw = "cmp.mapping.preset.cmdline()";
            };
            sources = [
              {
                name = "path";
              }
              {
                name = "cmdline";
                option = {
                  ignore_cmds = [
                    "Man"
                    "!"
                  ];
                };
              }
            ];
          };
        };
      };
      cmp-buffer.enable = true;
      cmp-cmdline-history.enable = true;
      cmp-cmdline.enable = true;
      cmp-conventionalcommits.enable = true;
      cmp-digraphs.enable = true;
      cmp-emoji.enable = true;
      cmp-nvim-lsp.enable = true;
      cmp-nvim-lua.enable = true;
      cmp-path.enable = true;
      luasnip = {
        enable = true;
        settings = {
          enable_autosnippets = true;
          # TODO: Change this key
          # store_selection_keys = "<Tab>";
          update_events = (
            helpers.listToUnkeyedAttrs ["TextChanged" "TextChangedI"]
          );
        };
        fromLua = [
          {}
          {
            paths = "~/.config/nvim/luasnip";
          }
        ];
      };
      cmp_luasnip.enable = true; # TODO: Which of these to use?
    };
  };
}
