{pkgs, ...}: {
  programs.nixvim = {
    # Gruvbox everywhere please. Everywhere.
    # See https://github.com/morhetz/gruvbox
    #  I love the gruvbox colourscheme, and I try to use it anywhere and
    #  everywhere I can. Especially in vim.
    # It's muted and clear, looks damn good to my eyes and is easy on them,
    # literally.
    colorschemes.gruvbox = {
      enable = true;
      settings = {
        bold = true;
        # color_column = "bg3";
        # highlight_search_cursor = "red";
        #  Italics are disabled by default because so many terminals can't
        # handle the slant, but my terminal can.
        italics = true;
        # numberColumn = "bg2";
        # signColumn = "bg1";
        contrast_dark = "soft";
        contrast_light = "hard";
        improved_warnings = true;
        improved_strings = false;
        invert_tabline = false;
        invert_indent_guides = false;
        invert_signs = false;
        transparent_bg = false;
        italicize_comments = false;
        italicize_strings = false;
        # By default, gruvbox only uses 256 colours, but it is able to make
        # use of so many more colours than that. Switching on
        # `termguicolors` makes nvim use a 24bit palette instead because we
        # live in the future.
        true_color = true;
        undercurl = true;
        underline = true;
      };
      # vertSplitColor = "purple";
    };
    opts = {
      # Timeout settings for which-key
      timeout = true;
      timeoutlen = 300;
    };
    plugins = {
      # Lean & mean status/tabline for vim that's light as air. It matches
      # my tmux statusline as well.
      # See https://github.com/vim-airline/vim-airline
      airline = {
        enable = true;
        settings = {
          # TODO: Try with 0 and see what changes
          powerline_fonts = 1;
          theme = "base16_gruvbox_dark_medium";
        };
      };
      web-devicons.enable = true;
      headlines.enable = false;
      noice = {
        enable = false;
        settings = {
          cmdline.format = {
            cmdline = {icon = ">";};
            search_down = {icon = "🔍⌄";};
            search_up = {icon = "🔍⌃";};
            filter = {icon = "$";};
            lua = {icon = "☾";};
            help = {icon = "?";};
          };
          format = {
            level = {
              icons = {
                error = "✖";
                warn = "▼";
                info = "●";
              };
            };
          };
          inc_rename.cmdline.format.IncRename = {icon = "⟳";};
          presets = {
            bottom_search = true;
          };
          popupmenu = {
            kind_icons = false;
          };
        };
      };
      # nvim-notify
      # https://github.com/rcarriga/nvim-notify
      # A fancy, configurable, notification manager for NeoVim
      notify.enable = false;

      # HiPhish/rainbow-delimiters.nvim: Rainbow delimiters for Neovim
      # with Tree-sitter
      # https://github.com/hiphish/rainbow-delimiters.nvim
      # This Neovim plugin provides alternating syntax highlighting
      # ("rainbow parentheses") for Neovim, powered by Tree-sitter. The
      # goal is to have a hackable plugin which allows for different
      # configuration of queries and strategies, both globally and per
      # file type. Users can override and extend the built-in defaults
      # through their own configuration.
      rainbow-delimiters.enable = true;
      # folke/which-key.nvim: 💥 Create key bindings that stick.
      # WhichKey is a lua plugin for Neovim 0.5 that displays a popup with
      # possible keybindings of the command you started typing.
      # https://github.com/folke/which-key.nvim
      which-key.enable = true;
    };
    extraPlugins = with pkgs.vimPlugins; [
      vim-airline-themes
      lsp-colors-nvim
      # Pretty icons for pretty ui
      nvim-web-devicons
    ];
  };
}
