{...}: {
  programs.nixvim = {
    plugins.nvim-ufo = {
      enable = true;
      settings = {
        provider_selector = ''
            function(bufnr, filetype, buftype)
            return { 'treesitter', 'indent' }
          end
        '';
      };
    };
    opts = {
      foldlevel = 99; # Using ufo provider needs a large value.
      foldcolumn = "1";
      foldlevelstart = 99;
    };
    extraConfigLua = ''
      vim.keymap.set('n', 'zK', function()
          local winid = require('ufo').peekFoldedLinesUnderCursor()
          if not winid then
          vim.lsp.buf.hover()
          end
          end, { desc = "Pee[k] fold" })
    '';
  };
}
