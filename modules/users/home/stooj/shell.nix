{...}: {
  home.shellAliases = {
    # Coloured grep
    grep = "grep --color=auto";
    fgrep = "fgrep --color=auto";
    egrep = "egrep --color=auto";
    # There was this thing called more that I used to type sometimes and now
    # it's more less than more.
    more = "less";
    # Human numbers for df
    df = "df --human-readable";
    # Human numbers and a grand total for du
    du = "du --total --human-readable";
    # Make dmesg output colorful and with nice numbers
    dmesg = "dmesg --human --color";
  };
}
