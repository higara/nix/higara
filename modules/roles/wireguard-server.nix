[
  {
    home-manager = {
      extraSpecialArgs = {
        isWorkstation = false;
        isPersonalWorkstation = false;
        isWorkWorkstation = false;
      };
    };
  }
  ../formulas/wireguard-server.nix
]
