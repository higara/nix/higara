[
  {
    home-manager = {
      extraSpecialArgs = {
        isWorkstation = false;
        isPersonalWorkstation = false;
        isWorkWorkstation = false;
      };
    };
  }
  ../formulas/rclone.nix
  ../formulas/storagebox-audiobooks-mount.nix
  ../formulas/audiobookshelf.nix
]
