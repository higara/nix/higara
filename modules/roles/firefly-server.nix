[
  {
    home-manager = {
      extraSpecialArgs = {
        isWorkstation = false;
        isPersonalWorkstation = false;
        isWorkWorkstation = false;
      };
    };
  }
  ../formulas/firefly.nix
]
