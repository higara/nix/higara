[
  {
    home-manager = {
      extraSpecialArgs = {
        isWorkstation = false;
        isPersonalWorkstation = false;
        isWorkWorkstation = false;
      };
    };
  }
  ../profiles/media-mounts.nix
  ../formulas/jellyfin.nix
]
