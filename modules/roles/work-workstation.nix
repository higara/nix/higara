[
  {
    home-manager = {
      extraSpecialArgs = {
        isWorkstation = true;
        isPersonalWorkstation = false;
        isWorkWorkstation = true;
      };
    };
    virtualisation.vmVariant = {
      virtualisation = {
        memorySize = 2048;
        cores = 3;
      };
    };
  }
  ../formulas/workstation-users.nix
  ../profiles/desktop-environment.nix
  ../profiles/work-workstation-environment.nix
]
