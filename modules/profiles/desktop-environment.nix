{
  imports = [
    ../formulas/dev-tools.nix
    ../formulas/firefly-data-importer.nix
    ../formulas/firefox.nix
    ../formulas/fonts.nix
    ../formulas/gdm.nix
    ../formulas/gnome.nix
    ../formulas/gpg.nix
    ../formulas/graphic-design.nix
    ../formulas/i3wm.nix
    # ../formulas/kodi.nix
    ../formulas/keyboardio.nix
    ../formulas/libvirt.nix
    ../formulas/mpv.nix
    ../formulas/office.nix
    ../formulas/plasma.nix
    ../formulas/python.nix
    ../formulas/qutebrowser.nix
    ../formulas/rclone.nix
    ../formulas/sound.nix
    # Disabled, does not build atm
    # ../formulas/virtualbox.nix
    ../formulas/yubikey.nix
    ../formulas/workstation-packages.nix
    # Disable hyprland for now. hy3 doesn't support the nixpkgs version of
    # hyprland, but I don't want to run the development version and have to
    # compile it myself.
    # ../formulas/hyprland.nix
  ];
}
