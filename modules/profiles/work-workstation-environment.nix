{
  imports = [
    ../formulas/docker.nix
    ../formulas/work-workstation/chromium.nix
    ../formulas/work-workstation/firefox.nix
    ../formulas/work-workstation/packages.nix
    ../formulas/work-workstation/gitwatch.nix
    ../formulas/work-workstation/homepage.nix
  ];
}
