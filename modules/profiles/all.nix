{
  imports = [
    ../users/stooj.nix
    ../formulas/base-packages.nix
    ../formulas/git.nix
    ../formulas/home-manager.nix
    ../formulas/locale.nix
    ../formulas/nixpkgs.nix
    ../formulas/openssh.nix
    ../formulas/timesyncd.nix
    ../formulas/tmux.nix
    ../formulas/users.nix
    ../formulas/zsh.nix
  ];
}
