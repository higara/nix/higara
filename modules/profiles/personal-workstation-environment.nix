{
  imports = [
    ../formulas/podman.nix
    ../formulas/personal-workstation/games.nix
    # ../formulas/personal-workstation/mozillavpn.nix
    ../formulas/personal-workstation/packages.nix
    ../formulas/personal-workstation/printers.nix
    ../formulas/personal-workstation/nextcloud-client.nix
  ];
}
